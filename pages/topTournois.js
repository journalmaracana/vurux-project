
import { useContext, useState, useEffect } from 'react'
import { Typography, Divider, Space, notification, Button, Row, Col, Upload,  } from 'antd'
import { UploadOutlined, InboxOutlined } from '@ant-design/icons';
const { Dragger } = Upload;
const { Title, Paragraph } = Typography
import HeaderDark from 'Templates/HeaderDark';
import Sidebar from 'Templates/HeaderSidebar';
import {
    VuroxLayout,
    HeaderLayout,
    VuroxSidebar,
    ContentLayout,
    VuroxComponentsContainer
} from 'Components/layout'
import { teal, grey } from "@material-ui/core/colors";
import { CardMedia, MenuItem, ButtonGroup, withStyles } from '@material-ui/core';
import { vuroxContext } from '../context'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import Paper from '@mui/material/Paper'
import axios from "axios"
import {API_LINK} from '../utils/constantes'
import Swal from 'sweetalert2';
import Spinner from '../Components/spinner';
import { I18n } from 'react-redux-i18n';
import {connect} from 'react-redux'
import {verifyPermission} from '../utils/functions'
import { useRouter } from 'next/router';

const styles = theme => ({
    root: {
        flexGrow: 5,
        fontSize: "1.5rem"
    },
    primaryColor: {
        color: "rgb(0, 193, 216)",
        fontSize: "27px"
    },
    secondaryColor: {
        color: grey[700]
    },

    padding: {
        padding: 0,

    },
    textFont: {
        fontSize: "1.5rem"
    },
    mainHeader: {
        backgroundColor: grey[100],
        padding: 20,
        alignItems: "center"
    },
    mainContent: {
        padding: 40,

    },
    secondaryContainer: {
        padding: "20px 25px",
        backgroundColor: grey[200]
    }
});


const TopTournoi = (props) => {
    const langPage = props.i18n.locale
    const { classes, open, onClose } = props;
    const { menuState } = useContext(vuroxContext)
    const toggleClass = menuState ? 'menu-closed' : 'menu-open'
    const [isLoading, setIsLaoding] = useState(true)
    const [topTournois, setTopTournois] = useState([])
    const [tournois, setTournois] = useState([])

    useEffect(() => {
        getTournois()
        getTopTournois()
    },[])
    const getTournois = async () => {
        await axios.get(API_LINK+ 'v1/tournois')
            .then(res => {
                console.log('res tournois', res);

                setIsLaoding(false)
                setTournois(res.data.map(tournoi => {
                    return {
                        value: tournoi._id,
                        label: langPage === 'ar' ? tournoi.entitledAR : tournoi.entitled
                    };
                }),
                );
            })
            .catch(err => {
                setIsLaoding(false)
            });
    };
    const getTopTournois = async () => {
        await axios.get(API_LINK + 'v1/topTournois/')
            .then(res => {
                console.log('res selected', res);
                setTopTournois(res.data.map(tournoi => {
                    return {
                        value: tournoi.value._id,
                        label: langPage === 'ar' ? tournoi.value.entitledAR : tournoi.value.entitled
                    };
                }),
                );
            })
            .catch(err => {
                console.log(err);
            });
    };

    const addTopTournois = topTournoi => {
        axios.put(API_LINK + 'v1/topTournois', topTournoi)
            .then(res => {
                console.log('res', res);
                getTopTournois();
                setIsLaoding(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('topTournoi.addsuccess'),
                        showConfirmButton: true,
                    }))
            })
            .catch(err => {
                console.error('err', err.response);
                setIsLaoding(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('topTournoi.adderror'),
                        showConfirmButton: true,
                    }))

            });
    };

    
    const handleChange = (event, values) => {
        console.log("love", event, values);
        if (event) {
            setTopTournois(values);
        } else {
            setTopTournois([]);
        }
    };

    const handleDelete = (event, item) => {

        if (event) {
            const newSelectedItem = [...topTournois];
            console.log("handleDeletecat", newSelectedItem.indexOf(item))
            newSelectedItem.splice(newSelectedItem.indexOf(item), 1);
            setTopTournois(newSelectedItem);
        }
    };
  
    if (
        !verifyPermission(
             props.user.UserID,
            useRouter().pathname,
            props.user.UserType ? props.user.UserType : "simple",
            props.user.UserIsValid? props.user.UserIsValid : ""
        )
    ) {
        return (
            <>
                <React.Fragment>
                    <VuroxLayout>
                      
                        <ContentLayout width="100%" className="p-3 vurox-scroll-y">
                            <Row align={props.i18n.locale === 'ar' ? "right" : "left"}>
                                <Col md="4"></Col>
                                <Col md="4">
                                    <b
                                        className="text-center text-danger"
                                        style={{ marginTop: "210px", fontSize: "55px" }}
                                    >
                                        {" "}
                                       {props.i18n.locale=="ar"?"ACCESS DENIED":"ACCESS DENIED"} 
                                    </b>
                                </Col>
                                <Col md="4"></Col>
                            </Row>
                        </ContentLayout>
                    </VuroxLayout>
                </React.Fragment>
            </>
        );
    }
    return (
        
        <React.Fragment>
            <div dir={langPage === 'ar' ? 'rtl' : 'ltr'}>  
             {!isLoading ? 
          <>
            <HeaderLayout className="sticky-top">
                <HeaderDark />
            </HeaderLayout>
            <VuroxLayout>
                <VuroxSidebar width={240} className={`sidebar-container  ${toggleClass}`} >
                    <Sidebar className={toggleClass} />
                </VuroxSidebar>
                <ContentLayout width='100%' className='p-1 vurox-scroll-y'>
                    <Row align={langPage === 'ar' ? "right" : "left"}>
                        <Col md={24} align={langPage === 'ar' ? "right" : "left"}>
                            <VuroxComponentsContainer className='p-4 ml-2 mt-2' >
                                <Title level={2} align={langPage === 'ar' ? "right" : "left"}>{I18n.t('topTournoi.top_tournoi')}</Title>

                                <Divider dashed />

                                <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} >
                                    <Autocomplete
                                        multiple
                                        freeSolo
                                        id="tags-outlined"
                                        options={tournois}
                                        getOptionLabel={(option) => option.label}
                                        getOptionValue={(option) => option.value}
                                        value={topTournois.length > 0 ? topTournois : []}
                                        PaperComponent={({ children }) => (
                                            <Paper style= {langPage === 'ar' ?{ direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                        )}
                                        onChange={(event, newValue) =>
                                            handleChange(event, newValue)
                                        }
                                        getOptionSelected={(option, value) => {
                                            return option.value === value.value
                                        }}
                                        filterSelectedOptions
                                        renderInput={(params) => {

                                            return (
                                                <TextField
                                                    inputProps={{ style: langPage === 'ar' ? { textAlign: 'right' } : { textAlign: 'left' } }}
                                                    {...params}
                                                    InputProps={{
                                                        ...params.InputProps,
                                                       
                                                        startAdornment:
                                                        //langPage === 'ar' ? null :
                                                            (

                                                                topTournois.length > 0 ? topTournois.map((item, index) => (
                                                                    <Chip
                                                                        tabIndex={-1}
                                                                        label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.label}</Typography>}
                                                                        inputLabelProps={{style:{ textAlign: 'left' }}}
                                                                        //style={{height:"100%"}}
                                                                        className={classes.chip}
                                                                        onDelete={(e, v) => { console.log("valuuees", v, e), handleDelete(e, item) }}
                                                                    />
                                                                ))

                                                                    : null
                                                                    
                                                            ),
                //                                              endAdornment: langPage === 'ar' ?
                //      topTournois.length> 0? topTournois.map((item,index) => (
                //        <>

                //     <Chip
                //       label={<Typography style={{whiteSpace: 'normal' , float:"right"}}>{ item.label}</Typography>}
                //       inputLabelProps={{ style: { textAlign: 'right' }}}
                //       className={classes.chip}
                //       onDelete={(e)=> handleDelete(e,item)}
                //     />
                //     </>
                //   )):null:null, 

                                                    }}
                                                    variant="outlined"
                                                    label={""}
                                                    margin="normal"
                                                    fullWidth
                                                    name="selectValue"
                                                    id="selectValue"
                                                // onBlur={() => validator.showMessageFor("category")}

                                                />
                                            );
                                        }}
                                    />
                                </Grid>

                                <Button type="primary" onClick={()=>addTopTournois(topTournois)} >
                                    {I18n.t('action.enregistrer')}
                                </Button>
                                <br />
                                <br />
                                <Divider dashed />

                            </VuroxComponentsContainer>
                        </Col>

                    </Row>
                </ContentLayout>
            </VuroxLayout>
         </> :

        <Spinner  loading={isLoading}/>
}</div>  </React.Fragment>
     
    )
}
export default connect(state => state)(withStyles(styles)(TopTournoi))
