import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import { API_LINK ,langueOptions,typeAnnonceOptions,pagesOptions ,ADsTypeOptions} from "../utils/constantes";
import axios from "axios";
import Spinner from "../Components/spinner";
import { Pagination } from "../Components/pagination";
import {verifyPermission} from "../utils/functions";
import { Markup } from "interweave";
import moment from 'moment'
import { connect } from 'react-redux'
import Chip from "@material-ui/core/Chip";
import Paper from '@mui/material/Paper'
import Swal from 'sweetalert2';
import Box from '@material-ui/core/Box';
import { I18n } from 'react-redux-i18n'
import { PlusSquareOutlined, EditOutlined } from '@ant-design/icons';
import Dialog from "@material-ui/core/Dialog";
import { CardMedia } from '@material-ui/core';
import DialogContent from "@material-ui/core/DialogContent";
import { SearchOutlined } from '@ant-design/icons';
import Grid from "@material-ui/core/Grid";
import { Cancel, Tag } from "@material-ui/icons";
import { Stack } from "@material-ui/core";
//import { Radio, RadioGroup, Checkbox } from '@material-ui/core';
// import Button from "@material-ui/core/Button";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { teal, grey } from "@material-ui/core/colors";
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Button from '@material-ui/core/Button';
import IconButton from "@material-ui/core/IconButton";
// import { getCookie, setCookie, deleteCookie, verifyPermission } from "../utils/functions"
import { UploadOutlined, InboxOutlined, LinkedinOutlined, MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import {
  MuiPickersUtilsProvider, KeyboardDatePicker,
  DatePicker as DateP,
  DateTimePicker as DateTP
} from "material-ui-pickers";
import { fr } from "date-fns/locale";
import ar from "date-fns/locale/ar-DZ";
import List from "@material-ui/core/List"
import InputLabel from "@material-ui/core/InputLabel";
import ListItem from "@material-ui/core/ListItem"
import DateFnsUtils from '@date-io/date-fns';
import ListItemText from "@material-ui/core/ListItemText"
import Autocomplete from "@material-ui/lab/Autocomplete";
import {
  Space,
  Divider,
  message,
  Radio, Table,
  Timeline,
  TimeRelatedForm,
  Tooltip, Upload, Icon, ConfigProvider
} from 'antd';
import { FaEye, FaTrash, FaPencilAlt, FaUserTimes } from 'react-icons/fa';
import { vuroxContext } from '../context/index'
import HeaderDark from '../Templates/HeaderDark';
import Sidebar from '../Templates/HeaderSidebar';
import {
  Row,
  Col,
  Form,
  Input,
  FormGroup,

} from "reactstrap";


import {
  VuroxLayout,
  HeaderLayout,
  VuroxSidebar,
  ContentLayout,
} from "../Components/layout";

import SimpleReactValidator from "simple-react-validator";

import Item from "antd/lib/list/Item";
//import SunEditor from "suneditor-react";
//import "suneditor/dist/css/suneditor.min.css";
import dynamic from "next/dynamic";
import "suneditor/dist/css/suneditor.min.css"; // Import Sun Editor's CSS File to the _app.js
import { VuroxBreadcrumbs } from "../Components/breadcrumbs";
import TagsInput from "../Components/Tagsinput"
import { withStyles, MenuItem, ButtonGroup } from "@material-ui/core";
import { useRouter } from "next/router";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import VuroxFormSearch from "../Components/search"
import { StylesProvider, ThemeProvider, jssPreset } from "@material-ui/styles";
import { create } from "jss";
import rtl from "jss-rtl";
import { createMuiTheme } from "@material-ui/core/styles";

const KeyCodes = {
  comma: 188,
  enter: [10, 13],
};

let majid = []
const SunEditor = dynamic(() => import("suneditor-react"), { //besure to import dynamically
  ssr: false,
});

const styles = theme => ({
  root: {
    flexGrow: 5,
    fontSize: "1.5rem"
  },
  primaryColor: {
    color: "rgb(0, 193, 216)",
    fontSize: "27px"
  },
  secondaryColor: {
    color: grey[700]
  },

  padding: {
    padding: 0,

  },
  textFont: {
    fontSize: "1.5rem"
  },
  mainHeader: {
    backgroundColor: grey[100],
    padding: 20,
    alignItems: "center"
  },
  mainContent: {
    padding: 40,

  },
  secondaryContainer: {
    padding: "20px 25px",
    backgroundColor: grey[200]
  }
});
const ads_manager = (props) => {
  const [columns, setColumns] = useState([
    {
      title: "#",
      width: 50,
      dataIndex: 'num',
      key: 'num',
      //fixed: 'left'
    },
    {
      title: I18n.t("ads.compagne"),
      width: 100,
      dataIndex: 'compagne',
      key: 'compagne',
      //fixed: 'left'
    },
    { title: I18n.t("ads.type_annonce"), dataIndex: 'type', key: 'type', width: 100 },
    { title: I18n.t("ads.date_debut"), dataIndex: 'debut', key: 'debut', width: 100 },
    { title: I18n.t("ads.date_fin"), dataIndex: 'fin', key: 'fin', width: 100 },
    {
      title: I18n.t("action.action"),
      key: 'operation',
      dataIndex: 'action',
      //fixed: 'right',
      width: 150,
      //render: () => <OptionButtons />
    }]);
  


  const { classes, open, onClose } = props;

  const [editImage, SetEditImage] = useState(false);
  const [editPdf, SetEditPdf] = useState(false);
  const [journalID, setJournalID] = useState("")
  const { menuState } = useContext(vuroxContext)
  const [lng, setLang] = useState('FR');
  const [currentPage, setCurrentPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const [pageSize, setPageSize] = useState(5);
  const [publishedHour, setPublishedHour] = useState(moment.now());
  const [image, setImage] = useState("");
  const [pdf, setPdf] = useState("");
  const toggleClass = menuState ? 'menu-closed' : 'menu-open'
  const validator = new SimpleReactValidator({ autoForceUpdate: this });
  const [OpenModal, setOpenModal] = useState(false)
  const [OpenModalEdit, setOpenModalEdit] = useState(false)
  const [OpenModalDelete, setOpenModalDelete] = useState(false)
  const [OpenModalView, setOpenModalView] = useState(false)
  const router = useRouter()
  const [numero, setNumero] = useState("")
  const [publier, setPublier] = useState("")
  const [data, setData] = useState([])
  const [stateR, setStateR] = useState('Nosearch')
  const [search, setSearch] = useState('')
  const [loadingPublicity, setloadingPublicity] = useState(true)
  const [tournoisOptions, setTournoisOptions] = useState([])
  const [annonceId, setAnnonceId] = useState("")
  const [param, setParam] = useState("add")
  const [typeAnnonce, setTypeAnnonce] = useState("")
  const [langues, setLangues] = useState([])
  const [nameCompPub, setNameCompPub] = useState("")
  const [script, setScript] = useState("")
  const [pages, setPages] = useState([])
  const [tournoisSelected, setTournoisSelected] = useState([])
  const [file, setFile] = useState("")
  const [imageLeft, setImageLeft] = useState("")
  const [imageRight, setImageRight] = useState("")
  const [fileBefore, setFileBefore] = useState("")
  const [imageLeftBefore, setImageLeftBefore] = useState("")
  const [imageRightBefore, setImageRightBefore] = useState("")
  const [link, setlink] = useState("")
  const [dateStart, setdateStart] = useState(moment())
  const [duration, setduration] = useState("")
  const [typeADS, settypeADS] = useState("")
  const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
  const theme = createMuiTheme({
    direction: props.i18n.locale === 'ar' ? "rtl" : "ltr"
  });
  useEffect(() => {
    const pageQuery = router.query.page ? router.query.page : 1
    console.log("routerrr", pageQuery)
    setCurrentPage(pageQuery)
    getTournois()
    getPublicities(pageQuery, pageSize)
  }, [router.isReady, router.query, search]);
  useEffect(() => {
    { console.log("daaatestart", dateStart, moment(dateStart, moment.date).toDate()) }
  }, [dateStart])
  async function getPublicities(index, limit) {
    setData([])
    setloadingPublicity(true)
    let url
    if (search) {
      url = API_LINK + 'v1/searchPublicitiesPagination/' + search + '/' + index + '/' + limit
    }
    else {
      url = API_LINK + 'v1/publicityPagination/' + index + '/' + limit
    }
    await axios.get(url)
      .then(res => {
        console.log("publicities", res.data.publicities)
        setData(res.data.publicities.map((item, id) => {
          return {
            num: id,
            compagne: item.compagne ? item.compagne : "/",
            type: props.i18n.locale=="ar"?item.type=="Libre"?'غوغل':'حرّة':item.type,
            debut: item.datedebut ? moment(item.datedebut).format('DD-MM-YYYY[T]HH:mm') : "/",
            fin: item.datedebut ? moment(item.datedebut).add(item.Duration, 'days').format('DD-MM-YYYY[T]HH:mm') : "/",
            action: (
              <OptionButtons
                onEdit={() => editModalPublicity(item)}
                onView={() => viewModalPublicity(item)}
                onDelete={() => deleteModalPublicity(item)}
              />
            ),
          };
        }))

        setTotalCount(res.data.count)
        setloadingPublicity(false)

      })
      .catch(err => {
        setloadingPublicity(false)
        console.log(err);
      })

  }
  const OptionButtons = (props) => {
    return (
      <Fragment>
        <Button color="success" outline className="btn-sm mx-1" onClick={() => props.onView()}> <FaEye /> </Button>
        <Button color="success" outline className="btn-sm mx-1" onClick={() => props.onEdit()}> <FaPencilAlt /> </Button>
        <Button color="danger" outline className="btn-sm mx-1" onClick={() => props.onDelete()}> <FaTrash /> </Button>

      </Fragment>
    );
  }
  const getTournois = async () => {
    await axios.get(API_LINK + 'v1/tournois')
      .then(res => {
        setTournoisOptions(res.data.map(tournoi => {
          return { value: tournoi._id, label:tournoi.entitled+"-"+tournoi.entitledAR }
        }))
      })
      .catch(err => {
        console.log(err);
      })
  }

  const searchPublicity = (e) => {
    console.log("seaarch", e.target.value)
    if (e) {
      setloadingPublicity(true)
      setData([])
      if (e.target.value !== '') {
        setSearch(e.target.value)
      }
      else {
        setStateR('Nosearch')
        setSearch("")
      }
      getPublicities(1, pageSize)
      router.replace({
        pathname: '/ads_manager',
        query: { page: 1 }
      },
        undefined, { shallow: true }
      )
    }

  }
  /****************fin affichage */
  const ChangePageSize = (e) => {
    if (e) {
      if (e.target.value === '') {
        setPageSize(50)
        getPublicities(1, 50)
        router.replace({
          pathname: '/ads_manager',
          query: { page: 1 }
        },
          undefined, { shallow: true }
        )
      }
      else {
        setPageSize(e.target.value)
        getPublicities(1, e.target.value)
        router.replace({
          pathname: '/ads_manager',
          query: { page: 1 }
        },
          undefined, { shallow: true }
        )

      }
    }

  }


  const editModalPublicity = (item) => {
    console.log("updaaate", item)
    setParam("update")
    setOpenModalEdit(true)
    setAnnonceId(item._id)
    settypeADS(item.position)
    setTournoisSelected(item.tournoi)
    setLangues(item.lng)
    setPages(item.page)
    setFile(item.image)
    setlink(item.lien)
    setdateStart(item.datedebut ? item.datedebut : "")
    setduration(item.Duration ? item.Duration : "")
    setNameCompPub(item.compagne)
    setTypeAnnonce(item.type)
    setScript(item.googleAdsense)
    setFile(item.image)
    setImageLeft(item.imageleft)
    setImageRight(item.imageright)
    setFileBefore(item.image)
    setImageLeftBefore(item.imageleft)
    setImageRightBefore(item.imageright)

  }
  const viewModalPublicity = (item) => {
    console.log("updaaate", item)
    setOpenModalView(true)
    setAnnonceId(item._id)
    settypeADS(item.position)
    setTournoisSelected(item.tournoi)
    setLangues(item.lng)
    setPages(item.page)
    setFile(item.image)
    setlink(item.lien)
    setdateStart(item.datedebut ? item.datedebut : "")
    setduration(item.Duration ? item.Duration : "")
    setNameCompPub(item.compagne)
    setTypeAnnonce(item.type)
    setScript(item.googleAdsense)
    setFile(item.image)
    setImageLeft(item.imageleft)
    setImageRight(item.imageright)

  }
  const deleteModalPublicity = (item) => {
    setAnnonceId(item._id)
    setOpenModalDelete(true)
  }


  const resetState = () => {
    setAnnonceId("")
    settypeADS([])
    setTournoisSelected([])
    setLangues([])
    setPages([])
    setFile("")
    setImageLeft('')
    setImageRight()
    setlink('')
    setdateStart(moment())
    setduration("")
    setTypeAnnonce("")
    setScript('')
    setNameCompPub("")

  }

  const handleSelecetedPages = (event, values) => {
    if (event) {
      if (pages.length == 0) {
        console.log('')
        values.shift()
      }
      const uniquevalues = Array.from(new Set(values.map(a => a.value)))
        .map(id => {
          return values.find(a => a.value === id)
        })

      console.log('catselectevent', uniquevalues)
      setPages(uniquevalues)
    } else {
      setPages([])
    }
  };
  function handleSelecetedAds(items) {
    if (event) {
      if (typeADS.length == 0) {
        console.log('')
        values.shift()
      }
      const uniquevalues = Array.from(new Set(values.map(a => a.value)))
        .map(id => {
          return values.find(a => a.value === id)
        })

      console.log('catselectevent', uniquevalues)
      settypeADS(uniquevalues)
    } else {
      settypeADS([])
    }
  }
  const addAnnonce = () => {

    if (validator.allValid()) {
      const action = {
        user: "622e0eb16aed3a15d81a0e61",
        date: moment().format()
      }

      let newAnnonce = new FormData()
      newAnnonce.append('type', typeAnnonce);
      newAnnonce.append('googleAdsense', script);
      newAnnonce.append('lng', JSON.stringify(langues));
      newAnnonce.append('compagne', nameCompPub);
      newAnnonce.append('page', JSON.stringify(pages));
      newAnnonce.append('tournoi', JSON.stringify(tournoisSelected));
      newAnnonce.append('image', file);
      newAnnonce.append('imageleft', imageLeft);
      newAnnonce.append('imageright', imageRight);
      newAnnonce.append('lien', link);
      newAnnonce.append('datedebut', dateStart);
      newAnnonce.append('Duration', duration);
      newAnnonce.append('position', JSON.stringify(typeADS));
      newAnnonce.append('actions', JSON.stringify(action));

      if (param == "add") {
        axios.post(API_LINK + 'v1/publicity', newAnnonce, {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        })
          .then(res => {
            Swal.fire({
              position: "center",
              icon: "success",
              title: "",
              text: I18n.t("ads.addsuccess"),
              confirmButtonText: I18n.t("action.oui"),
              showConfirmButton: true,
            })
            getPublicities(currentPage, pageSize)
            resetState()
            setOpenModal(false)
          })
          .catch(err => {
            Swal.fire({
              icon: "error",
              title: "",
              confirmButtonColor: "#a6c76c",
              cancelButtonColor: "#a6c76c",
              confirmButtonText: I18n.t("action.oui"),
              text: I18n.t("ads.adderror")
            });
            resetState()
            setOpenModal(false)
          })
      } else if (param == "update") {

        axios.put(API_LINK + 'v1/publicity/' + annonceId, newAnnonce, {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        })
          .then(res => {
            Swal.fire({
              position: "center",
              icon: "success",
              title: "",
              confirmButtonText: I18n.t("action.oui"),
              text: I18n.t("ads.setsuccess"),
              showConfirmButton: true,
            })
            getPublicities(currentPage, pageSize)
            resetState()
            setOpenModalEdit(false)
          })
          .catch(err => {
            Swal.fire({
              icon: "error",
              title: "",
              confirmButtonColor: "#a6c76c",
              cancelButtonColor: "#a6c76c",
              confirmButtonText: I18n.t("action.oui"),
              text: I18n.t("ads.seterror")
            });
            resetState()
            setOpenModalEdit(false)
          })
      }

    } else {

      validator.showMessages()

    }

  }

  const renderAddModalnew = () => {
    const userType = props.user.UserType
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="sm"
            open={OpenModal}
            onClose={() => setOpenModal(!OpenModal)}
          >
            <Grid container >
              <Grid item xs={12}>
                <Grid container>
                <Grid item xs={12} className={classes.padding}  align={props.i18n.locale === 'ar' ? "left" : "right"}>
                    <IconButton
                      edge="end"
                      color="inherit"
           
                      aria-label="Close"
                      style={{ padding: 15 }}
                      onClick={() => setOpenModal(!OpenModal)}
                      className={classes.padding}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Grid>
                </Grid>

                <Grid container direction="row" className={classes.mainHeader}>
                  <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <Typography className={classes.primaryColor} variant="h5">
                      {I18n.t("ads.add")}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  container
                  className={classes.mainContent}
                  spacing={4}
                  dir={props.i18n.locale === 'ar' ? 'rtl' : 'ltr'}
                >
                  <Grid>
                  </Grid>
                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <TextField
                      inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      label={I18n.t("ads.type_annonce")}
                      fullWidth
                      select
                      variant="outlined"
                      id="type"
                      name="type"
                      value={typeAnnonce}
                      required
                      margin="dense"
                      onChange={(e) => setTypeAnnonce(e.target.value)}
                      onBlur={() => validator.showMessageFor('typeAnnonce')}
                    >
                      <span style={{ 'color': 'red' }}>{validator.message('typeAnnonce', typeAnnonce, 'required',
                        {
                          messages: {
                            'required': I18n.t("journal.required"),
                          }
                        })}
                      </span>
                      {typeAnnonceOptions.map(item=>{
                      return (
                        <MenuItem style={props.i18n.locale  == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }}  value={item.value}>{props.i18n.locale=="ar"?item.label.split('-')[1]:item.label.split('-')[0]}</MenuItem>
                      )})}
                     
                    </TextField>
                  </Grid>
                  {typeAnnonce == "Libre" ?
                    <>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}

                          multiple
                          name="langues"
                          id="langues"
                          options={langueOptions}
                          value={langues.length > 0 ? langues : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setLangues([
                              ...newValue
                            ]);
                          }}
                         
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                
                                {...getTagProps({ index })}

                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.langues")} placeholder={I18n.t("ads.langue")} />
                          )}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('langues', langues, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                        <TextField
                          fullWidth
                          margin="dense"
                          variant="outlined"
                          label={I18n.t("ads.compagne")}
                          id="compagne"
                          name="compagne"
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          className={classes.textFont}
                          onChange={(e) => setNameCompPub(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('nameCompPub')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('nameCompPub', nameCompPub, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="Ads type"
                          id="ads"
                          options={ADsTypeOptions}
                          value={typeADS.length > 0 ? typeADS : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            settypeADS([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }

                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.ads_type")} placeholder={I18n.t("ads.ads_type")} />
                          )}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('typeADS', typeADS, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="pages"
                          id="pages"
                          options={pagesOptions}
                          value={pages.length > 0 ? pages : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setPages([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.page")} placeholder={I18n.t("ads.page")} />
                          )}

                        />

                        <span style={{ 'color': 'red' }}>{validator.message('pages', pages, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>
                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="Tournois (multiple)"
                          id="tournois"
                          options={tournoisOptions}
                          value={tournoisSelected.length > 0 ? tournoisSelected : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setTournoisSelected([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.tournoi")} placeholder={I18n.t("ads.tournoi")} />
                          )}

                        />

                        <span style={{ 'color': 'red' }}>{validator.message('tournoisSelected', tournoisSelected, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>
                      </Grid>
                      <Grid
                        item xs={12}
                        className={classes.padding}
                        align={props.i18n.locale === 'ar' ? "right" : "left"}
                      >

                        <input
                          className="text-center"
                          accept="image/*"
                          type="file"
                          id="file"
                          name="file"
                          onChange={(e) => {
                            setFile(e.target.files[0])
                          }}
                          style={{ display: "none" }}
                          onBlur={() => validator.showMessageFor("file")}
                        />

                        <span
                          style={{ color: "red" }}>
                          {validator.message("file", file, "required", {
                            messages: {
                              required: I18n.t("journal.required"),
                            },
                          })}
                        </span>
                        <label htmlFor="file" className="text-center">
                          <Button variant="contained" component="span">
                            {I18n.t("ads.importer")} {I18n.t("ads.banner")}
                          </Button>
                        </label>
                        <br />
                        {file ?

                          <span
                            style={{ color: "grey", fontSize: "14px" }}>
                            {file.name}
                          </span> : null
                        }
                      </Grid>
                      {typeADS.filter(({ value }) => value == "Habillage du site").length > 0 ?
                        <>
                          <Grid
                            align={props.i18n.locale === 'ar' ? "right" : "left"}
                            item xs={12}
                            className={classes.padding}
                          >

                            <input
                              className="text-center"
                              accept="image/*"
                              type="file"
                              id="imageLeft"
                              name="imageLeft"
                              onChange={(e) => {
                                setImageLeft(e.target.files[0])
                              }}
                              style={{ display: "none" }}
                              onBlur={() => validator.showMessageFor("imageLeft")}
                            />

                            <span
                              style={{ color: "red" }}>
                              {validator.message("imageLeft", imageLeft, "required", {
                                messages: {
                                  required: I18n.t("journal.required"),
                                },
                              })}
                            </span>
                            <label htmlFor="imageLeft" className="text-center">
                              <Button variant="contained" component="span">
                                {I18n.t("ads.importer")} {I18n.t("ads.banner_g")}
                              </Button>
                            </label>
                            <br />
                            {imageLeft ?

                              <span
                                style={{ color: "grey", fontSize: "14px" }}>
                                {imageLeft.name}
                              </span> : null
                            }
                          </Grid>
                          <Grid
                            align={props.i18n.locale === 'ar' ? "right" : "left"}
                            item xs={12}
                            className={classes.padding}
                          >

                            <input
                              className="text-center"
                              accept="image/*"
                              type="file"
                              id="imageRight"
                              name="imageRight"
                              onChange={(e) => {
                                setImageRight(e.target.files[0])
                              }}
                              style={{ display: "none" }}
                              onBlur={() => validator.showMessageFor("imageRight")}
                            />

                            <span
                              style={{ color: "red" }}>
                              {validator.message("imageRight", imageRight, "required", {
                                messages: {
                                  required: I18n.t("journal.required"),
                                },
                              })}
                            </span>
                            <label htmlFor="imageRight" className="text-center">
                              <Button variant="contained" component="span">
                                {I18n.t("ads.importer")} {I18n.t("ads.banner_d")}
                              </Button>
                            </label>
                            <br />
                            {imageRight ?

                              <span
                                style={{ color: "grey", fontSize: "14px" }}>
                                {imageRight.name}
                              </span> : null
                            }
                          </Grid>
                        </> : null}
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <TextField
                          fullWidth
                          margin="dense"
                          variant="outlined"
                          label={I18n.t("ads.link")}
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          id="lien"
                          name="lien"
                          className={classes.textFont}
                          onChange={(e) => setlink(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('link')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('link', link, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} md={6} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={props.i18n.locale ==="ar"?ar:fr}>

                          <DateTP

                            // inputProps={getCookie("lang") == "ar" ? { style: { textAlign: "right" } } : { style: { textAlign: "left" } }}
                            okLabel={I18n.t("journal.ok")}
                            clearLabel={I18n.t("journal.clear")}
                            cancelLabel={I18n.t("journal.cancel")}
                            inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                            value={dateStart ? moment(dateStart, moment.date).toDate() : ""}
                            label={I18n.t("ads.date_debut") + " " + I18n.t("action.et") + " " + I18n.t("ads.date_fin")}
                            placeholder= {props.i18n.locale ==="ar"?'--:-- aaaa/mm/jj ':'jj/mm/aaaa --:--'}
                            fullWidth
                            id="date"
                            name="date"
                            format={props.i18n.locale ==="ar"?"yyyy-MM-dd mm:HH  ":"dd-MM-yyyy HH:mm "}
                            ampm={false}
                            onChange={(e) => { console.log("dateselected", moment(moment(e)).format("YYYY-MM-DDTHH:mm")), setdateStart(e) }}
                            required
                          />

                        </MuiPickersUtilsProvider>


                      </Grid>
                      <Grid item xs={12} md={6} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <TextField
                          type='number'
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          fullWidth
                          margin="dense"
                          variant="outlined"
                          label=""
                          placeholder={I18n.t("ads.duree")}
                          id="durée"
                          name="durée"
                          className={classes.textFont}
                          onChange={(e) => setduration(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('duration')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('duration', duration, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>

                    </> : <>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <TextField
                          fullWidth
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          margin="dense"
                          variant="outlined"
                          label={I18n.t("ads.ads_script")}
                          id="script"
                          name="script"
                          multiline
                          rows={5}
                          className={classes.textFont}
                          onChange={(e) => setScript(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('script')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('script', script, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="pages (multiple)"
                          id="pages"
                          options={pagesOptions}
                          value={pages.length > 0 ? pages : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setPages([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.page")} />
                          )}

                        />

                        <span style={{ 'color': 'red' }}>{validator.message('pages', pages, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>
                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="Ads type"
                          id="ads"
                          options={ADsTypeOptions}
                          value={typeADS.length > 0 ? typeADS : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            settypeADS([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.ads_type")} />
                          )}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('typeADS', typeADS, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>

                      </Grid></>
                  }


                  <Divider><small></small></Divider>
                  <Grid item container alignContent="flex-end" align={props.i18n.locale === 'ar' ? "right" : "left"} >

                    <Grid item md={4} sm={4} xs={4} >
                      <Button disabled={!validator.allValid()} variant='outlined' onClick={() => addAnnonce()}>{I18n.t("action.enregistrer")}</Button>
                    </Grid>
                    <Grid item md={4} sm={4} xs={4} >
                    </Grid>
                    <Grid item md={4} sm={4} xs={4} alignContent="flex-end">
                      <Button variant='outlined' onClick={() => setOpenModal(!OpenModal)}>{I18n.t("action.fermer")}</Button>
                    </Grid>

                  </Grid>

                </Grid>

              </Grid>

            </Grid>
          </Dialog>
        </ThemeProvider>
      </StylesProvider>
    )

  }

  const renderEditModalnew = () => {
    console.log("updaaate 2", OpenModalEdit)
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="sm"
            open={OpenModalEdit}
            onClose={() => setOpenModalEdit(!OpenModalEdit)}
          >
            <Grid container >
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} className={classes.padding}  align={props.i18n.locale === 'ar' ? "left" : "right"}>
                    <IconButton
                      edge="end"
                      color="inherit"
           
                      aria-label="Close"
                      style={{ padding: 15 }}
                      onClick={() => setOpenModalEdit(!OpenModalEdit)}
                      className={classes.padding}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Grid>
                </Grid>

                <Grid container direction="row" className={classes.mainHeader}>
                  <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <Typography className={classes.primaryColor} variant="h5">
                      {I18n.t("ads.set")}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  container
                  className={classes.mainContent}
                  spacing={4}
                  dir={props.i18n.locale === 'ar' ? 'rtl' : 'ltr'}
                >
                  <Grid>
                  </Grid>
                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <TextField
                      inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      label={I18n.t("ads.type_annonce")}
                      fullWidth
                      select
                      variant="outlined"
                      id="type"
                      name="type"
                      value={typeAnnonce}
                      required
                      margin="dense"
                      onChange={(e) => setTypeAnnonce(e.target.value)}
                      onBlur={() => validator.showMessageFor('typeAnnonce')}
                    >
                      <span style={{ 'color': 'red' }}>{validator.message('typeAnnonce', typeAnnonce, 'required',
                        {
                          messages: {
                            'required': I18n.t("journal.required"),
                          }
                        })}
                      </span>
                      {typeAnnonceOptions.map(item=>{
                      return (
                        <MenuItem style={props.i18n.locale  == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }}  value={item.value}>{props.i18n.locale=="ar"?item.label.split('-')[1]:item.label.split('-')[0]}</MenuItem>
                      )})}
                     
                    </TextField>
                  </Grid>
                  {typeAnnonce == "Libre" ?
                    <>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="langues"
                          id="langues"
                          options={langueOptions}
                          value={langues.length > 0 ? langues : []}
                          getOptionLabel={({ label }) =>props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setLangues([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.langues")} placeholder={I18n.t("ads.langue")} />
                          )}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('langues', langues, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                        <TextField
                         inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          fullWidth
                          margin="dense"
                          variant="outlined"
                          label={I18n.t("ads.compagne")}
                          id="compagne"
                          name="compagne"
                          value={nameCompPub}
                          className={classes.textFont}
                          onChange={(e) => setNameCompPub(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('nameCompPub')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('nameCompPub', nameCompPub, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="Ads type"
                          id="ads"
                          options={ADsTypeOptions}
                          value={typeADS.length > 0 ? typeADS : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            settypeADS([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.ads_type")} />
                          )}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('typeADS', typeADS, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="pages (multiple)"
                          id="pages"
                          options={pagesOptions}
                          value={pages.length > 0 ? pages : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setPages([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.page")} />
                          )}

                        />

                        <span style={{ 'color': 'red' }}>{validator.message('pages', pages, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>
                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="Tournois (multiple)"
                          id="tournois"
                          options={tournoisOptions}
                          value={tournoisSelected.length > 0 ? tournoisSelected : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setTournoisSelected([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.tournoi")} />
                          )}

                        />

                        <span style={{ 'color': 'red' }}>{validator.message('tournoisSelected', tournoisSelected, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>
                      </Grid>
                      <Grid
                        item xs={12}
                        className={classes.padding}
                        align={props.i18n.locale === 'ar' ? "right" : "left"}
                      >

                        <input
                          className="text-center"
                          accept="image/*"
                          type="file"
                          id="file"
                          name="file"
                          onChange={(e) => {
                            setFile(e.target.files[0])
                          }}
                          style={{ display: "none" }}
                          onBlur={() => validator.showMessageFor("file")}
                        />

                        <span
                          style={{ color: "red" }}>
                          {validator.message("file", file, "required", {
                            messages: {
                              required: I18n.t("journal.required"),
                            },
                          })}
                        </span>
                        <label htmlFor="file" className="text-center">
                          <Button variant="contained" component="span">
                            {I18n.t("action.modifier")} {I18n.t("ads.banner")}
                          </Button>
                        </label>
                        <br />
                        {file ?

                          <span
                            style={{ color: "grey", fontSize: "14px" }}>
                            {file.name}
                          </span> : null
                        }
                        {fileBefore && (

                          <Box mt={2} xs={{ flexDirection: 'row' }}>
                            <img src={API_LINK + fileBefore} height="100px" />
                          </Box>

                        )}
                      </Grid>
                      {typeADS.filter(({ value }) => value == "Habillage du site").length > 0 ?
                        <>
                          <Grid
                            item xs={12}
                            className={classes.padding}
                            align={props.i18n.locale === 'ar' ? "right" : "left"}
                          >

                            <input
                              className="text-center"
                              accept="image/*"
                              type="file"
                              id="imageLeft"
                              name="imageLeft"
                              onChange={(e) => {
                                setImageLeft(e.target.files[0])
                              }}
                              style={{ display: "none" }}
                              onBlur={() => validator.showMessageFor("imageLeft")}
                            />

                            <span
                              style={{ color: "red" }}>
                              {validator.message("imageLeft", imageLeft, "required", {
                                messages: {
                                  required: I18n.t("journal.required"),
                                },
                              })}
                            </span>
                            <label htmlFor="imageLeft" className="text-center">
                              <Button variant="contained" component="span">
                                {I18n.t("action.modifier")} {I18n.t("ads.banner_g")}
                              </Button>
                            </label>
                            <br />
                            {imageLeft ?

                              <span
                                style={{ color: "grey", fontSize: "14px" }}>
                                {imageLeft.name}
                              </span> : null
                            }
                            {imageLeftBefore && (

                              <Box mt={2} xs={{ flexDirection: 'row' }}>
                                <img src={API_LINK + imageLeftBefore} height="100px" />
                              </Box>

                            )}
                          </Grid>
                          <Grid
                            item xs={12}
                            className={classes.padding}
                            align={props.i18n.locale === 'ar' ? "right" : "left"}
                          >

                            <input
                              className="text-center"
                              accept="image/*"
                              type="file"
                              id="imageRight"
                              name="imageRight"
                              onChange={(e) => {
                                setImageRight(e.target.files[0])
                              }}
                              style={{ display: "none" }}
                              onBlur={() => validator.showMessageFor("imageRight")}
                            />

                            <span
                              style={{ color: "red" }}>
                              {validator.message("imageRight", imageRight, "required", {
                                messages: {
                                  required: I18n.t("journal.required"),
                                },
                              })}
                            </span>
                            <label htmlFor="imageRight" className="text-center">
                              <Button variant="contained" component="span">
                                {I18n.t("action.modifier")} {I18n.t("ads.banner_d")}
                              </Button>
                            </label>
                            <br />
                            {imageRight ?

                              <span
                                style={{ color: "grey", fontSize: "14px" }}>
                                {imageRight.name}
                              </span> : null
                            }
                            {imageRightBefore && (

                              <Box mt={2} xs={{ flexDirection: 'row' }}>
                                <img src={API_LINK + imageRightBefore} height="100px" />
                              </Box>

                            )}
                          </Grid>
                        </> : null}
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <TextField
                          fullWidth
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          margin="dense"
                          variant="outlined"
                          label={I18n.t("ads.link")}
                          id="lien"
                          value={link}
                          name="lien"
                          className={classes.textFont}
                          onChange={(e) => setlink(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('link')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('link', link, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} md={6}  align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={props.i18n.locale ==="ar"?ar:fr}>
                          {/* <div style={{ display: "block" }}> */}
                          <DateTP
 i                            nputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                            // inputProps={getCookie("lang") == "ar" ? { style: { textAlign: "right" } } : { style: { textAlign: "left" } }}
                            okLabel={I18n.t("journal.ok")}
                            clearLabel={I18n.t("journal.clear")}
                            cancelLabel={I18n.t("journal.cancel")}
                            value={dateStart ? moment(dateStart, moment.date).toDate() : ""}
                            label={I18n.t("ads.date_debut") + " " + I18n.t("action.et") + " " + I18n.t("ads.date_fin")}
                              placeholder= {props.i18n.locale ==="ar"?'--:-- aaaa/mm/jj ':'jj/mm/aaaa --:--'}
                            fullWidth
                            id="date"
                            name="date"
                            format={props.i18n.locale ==="ar"?" yyyy-MM-dd mm:HH":"dd-MM-yyyy HH:mm "}
                            ampm={false}
                            onChange={(e) => { console.log("dateselected", moment(moment(e)).format("YYYY-MM-DDTHH:mm")), setdateStart(e) }}
                            required
                          />

                        </MuiPickersUtilsProvider>


                      </Grid>
                      <Grid item xs={12} md={6}  align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <TextField
                          type='number'
                          fullWidth
                          margin="dense"
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          variant="outlined"
                          label=""
                          placeholder={I18n.t("ads.duree")}
                          id="durée"
                          value={duration}
                          name="durée"
                          className={classes.textFont}
                          onChange={(e) => setduration(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('duration')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('duration', duration, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>

                    </> : <>
                      <Grid item xs={12}  align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <TextField
                          fullWidth
                          margin="dense"
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          variant="outlined"
                          label={I18n.t("ads.ads_script")}
                          id="script"
                          name="script"
                          value={script}
                          multiline
                          rows={5}
                          className={classes.textFont}
                          onChange={(e) => setScript(e.target.value)}
                          required
                          onBlur={() => validator.showMessageFor('script')}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('script', script, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required")
                            }
                          })}
                        </span>

                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="pages (multiple)"
                          id="pages"
                          inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          options={pagesOptions}
                          value={pages.length > 0 ? pages : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            setPages([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.page")} />
                          )}

                        />

                        <span style={{ 'color': 'red' }}>{validator.message('pages', pages, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>
                      </Grid>
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <Autocomplete PaperComponent={({ children }) => (
                          <Paper style={props.i18n.locale == 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                        )}
                          multiple
                          name="Ads type"
                          id="ads"
                          options={ADsTypeOptions}
                          value={typeADS.length > 0 ? typeADS : []}
                          getOptionLabel={({ label }) => props.i18n.locale=="ar"?label.split('-')[1]:label.split('-')[0]}
                          getOptionValue={({ value }) => value}
                          onChange={(event, newValue) => {
                            settypeADS([
                              ...newValue
                            ]);
                          }}
                          getOptionSelected={(option, value) => {
                            return option.value === value.value;
                          }}
                          filterSelectedOptions
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip inputLabelProps={props.i18n.locale == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                 label={props.i18n.locale=="ar"?option.label.split('-')[1]:option.label.split('-')[0]}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          style={{ width: 500 }}
                          renderInput={(params) => (
                            <TextField {...params} label={I18n.t("ads.ads_type")} />
                          )}
                        />
                        <span style={{ 'color': 'red' }}>{validator.message('typeADS', typeADS, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>

                      </Grid></>
                  }


                  <Divider><small></small></Divider>
                  <Grid item container alignContent="flex-end" align={props.i18n.locale === 'ar' ? "right" : "left"} >

                    <Grid item md={4} sm={4} xs={4} >
                      <Button disabled={!validator.allValid()} variant='outlined' onClick={() => addAnnonce()}>{I18n.t("action.modifier")}</Button>
                    </Grid>
                    <Grid item md={4} sm={4} xs={4} >
                    </Grid>
                    <Grid item md={4} sm={4} xs={4} alignContent="flex-end">
                      <Button variant='outlined' onClick={() => setOpenModalEdit(!OpenModalEdit)}>{I18n.t("action.fermer")}</Button>
                    </Grid>

                  </Grid>

                </Grid>

              </Grid>

            </Grid>
          </Dialog>
        </ThemeProvider >
      </StylesProvider>
    )


  }

  const renderViewModal = () => {
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="sm"
            open={OpenModalView}
            onClose={() => setOpenModalView(!OpenModalView)}

          >
            <Grid container>
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} className={classes.padding}  align={props.i18n.locale === 'ar' ? "left" : "right"}>
                    <IconButton
                      edge="end"
                      color="inherit"
                      aria-label="Close"
                      style={{ padding: 15 }}
                      onClick={() => setOpenModalView(!OpenModalView)}
                      className={classes.padding}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Grid>
                </Grid>

                <Grid container direction="row" className={classes.mainHeader}>
                  <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <Typography className={classes.primaryColor} variant="h5">
                      {I18n.t("ads.info")}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  container
                  className={classes.mainContent}
                  spacing={4}
                  dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}
                >


                  <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}   >
                    <TextField
 inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="standard"
                      label={I18n.t("ads.type_annonce")}
                      id="type"
                      name="type"
                      value={typeAnnonce ? props.i18n.locale=="ar"?typeAnnonce=="Libre"?'غوغل':'حرّة':typeAnnonce: '/'}
                      className={classes.textFont}
                      ariaReadonly="true"
                    />

                  </Grid>
                  {script ?
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <TextField
                       inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        fullWidth
                        margin="dense"
                        variant="standard"
                        label={I18n.t("ads.ads_script")}
                        id="script"
                        name="script"
                        value={script}
                        className={classes.textFont}
                        ariaReadonly="true"
                      />

                    </Grid> : null}
                  {nameCompPub ?
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <TextField
                       inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        fullWidth
                        margin="dense"
                        variant="standard"
                        label={I18n.t("ads.compagne")}
                        id="Compagne"
                        name="Compagne"
                        value={nameCompPub}
                        className={classes.textFont}
                        ariaReadonly="true"
                      />

                    </Grid> : null}
                  {langues && langues.length ?
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <Divider style={{ borderTopColor: "#606060" }} variant="h6" component="div">
                        <small>{I18n.t("ads.langues")}</small>
                      </Divider>
                      <List >
                        {langues.length ? langues.map(item => {
                          return <>
                            <ListItem>
                              <ListItemText className='text-center'
                                primary={props.i18n.locale=="ar"?item.label.split('-')[1]:item.label.split('-')[0]}
                              //secondary={secondary ? 'Secondary text' : null}
                              />
                            </ListItem>
                          </>
                        }) : null}
                      </List>
                    </Grid> : null}
                  {typeADS && typeADS.length ?
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <Divider style={{ borderTopColor: "#606060" }} variant="h3" component="div">
                        <small>{I18n.t("ads.ads_type")}</small>
                      </Divider>
                      <List >
                        {typeADS.length ? typeADS.map(item => {
                          return <>
                            <ListItem>
                              <ListItemText className='text-center'
                                primary={props.i18n.locale=="ar"?item.label.split('-')[1]:item.label.split('-')[0]}
                              //secondary={secondary ? 'Secondary text' : null}
                              />
                            </ListItem>
                          </>
                        }) : null}
                      </List>
                    </Grid> : null}
                  {pages && pages.length ?
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <Divider style={{ borderTopColor: "#606060" }} variant="h6" component="div">
                        <small>{I18n.t("ads.page")}</small>
                      </Divider>
                      <List >
                        {pages.length ? pages.map(item => {
                          return <>
                            <ListItem>
                              <ListItemText className='text-center'
                                primary={props.i18n.locale=="ar"?item.label.split('-')[1]:item.label.split('-')[0]}
                              //secondary={secondary ? 'Secondary text' : null}
                              />
                            </ListItem>
                          </>
                        }) : null}
                      </List>
                    </Grid> : null}
                  {tournoisSelected && tournoisSelected.length ?
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <Divider style={{ borderTopColor: "#606060" }} variant="h6" component="div">
                        <small>{I18n.t("ads.tournoi")}</small>
                      </Divider>
                      <List >
                        {tournoisSelected.length ? tournoisSelected.map(item => {
                          return <>
                            <ListItem>
                              <ListItemText className='text-center'
                                primary={props.i18n.locale=="ar"?item.label.split('-')[1]:item.label.split('-')[0]}
                              //secondary={secondary ? 'Secondary text' : null}
                              />
                            </ListItem>
                          </>
                        }) : null}
                      </List>
                    </Grid> : null}
                  {link ? <Grid item md={12} sm={12} xs={12}  >
                    <TextField
                     inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="standard"
                      label={I18n.t("ads.link")}
                      id="description"
                      name="description"
                      value={link}
                      className={classes.textFont}
                      aria-readonly="true"
                    />

                  </Grid>
                    : null}
                  {dateStart ? <>
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <TextField
                       inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        fullWidth
                        margin="dense"
                        variant="standard"
                        label={I18n.t("ads.date_debut")}
                        id="date"
                        name="date"
                        format={props.i18n.locale ==="ar"?" yyyy-MM-dd mm:HH ":"dd-MM-yyyy HH:mm "}
                        value={props.i18n.locale ==="ar"?moment(dateStart).format('mm:HH YYYY-MM-DD m') :moment(dateStart).format('DD-MM-YYYY HH:mm')}
                        className={classes.textFont}
                        aria-readonly="true"
                      />

                    </Grid>
                    <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                      <TextField
                       inputProps={props.i18n.locale === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        fullWidth
                        margin="dense"
                        variant="standard"
                        label={I18n.t("ads.date_fin")}
                        id="datef"
                        name="datef"
                        value={moment(dateStart).add(duration, 'days').format('DD-MM-YYYY HH:mm')}
                        className={classes.textFont}
                        aria-readonly="true"
                      />

                    </Grid></>
                    : null}

                  {typeADS && typeADS.length ? typeADS.filter(({ value }) => value == "Habillage du site").length > 0 ?
                    <>
                      <Divider></Divider>
                      <Grid item xs={4} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <h5>{I18n.t("ads.banner_g")}</h5>
                        <Box mt={2} textAlign="center">
                          <img src={imageLeft ? API_LINK + imageLeft : null} height="100px" />
                        </Box>
                      </Grid>
                      <Grid item xs={4} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                        <h5>{I18n.t("ads.banner")}</h5>
                        <Box mt={2} textAlign="center">
                          <img src={file ? API_LINK + file : null} height="100px" />
                        </Box></Grid>
                      <Grid item xs={4} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                        <h5>{I18n.t("ads.banner_d")}</h5>
                        <Box mt={2} textAlign="center">
                          <img src={imageRight ? API_LINK + imageRight : null} height="100px" />
                        </Box>
                      </Grid>
                    </> : file ?
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}  >
                        <Divider></Divider>
                        <h5>{I18n.t("ads.banner")}</h5>
                        <Box mt={2} textAlign="center">
                          <img src={file ? API_LINK + file : null} height="100px" />
                        </Box></Grid> : null : null
                  }

                  <Divider></Divider>
                </Grid>
              </Grid>
            </Grid>
          </Dialog>

        </ThemeProvider >
      </StylesProvider>


    )
  }
  const deletePublicity = async () => {
    await axios.delete(API_LINK + 'v1/publicity/' + annonceId).then(res => {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "",
        text: I18n.t("ads.deletesuccess"),
        confirmButtonText: I18n.t("action.oui"),
        showConfirmButton: true,
      })
      getPublicities(currentPage, pageSize)
      resetState()
      setOpenModalDelete(false)

    })
      .catch(err => {
        Swal.fire({
          icon: "error",
          title: "Oops...! ",
          confirmButtonColor: "#a6c76c",
          cancelButtonColor: "#a6c76c",
          confirmButtonText: I18n.t("action.oui"),
          text: I18n.t("ads.deleteerror")
        });
        resetState()
        setOpenModalDelete(false)
      })
  }



  const renderDeleteModel = () => {
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="sm"
            open={OpenModalDelete}
            onClose={() => setOpenModalDelete(!OpenModalDelete)}

          >
            <DialogContent className={classes.padding}>
              <Grid container>
                <Grid item xs={12}>
                  <Grid container>
              
                    <Grid item xs={12} className={classes.padding}  align={props.i18n.locale === 'ar' ? "left" : "right"}>
                    <IconButton
                      edge="end"
                      color="inherit"
                      aria-label="Close"
                      style={{ padding: 15 }}
                      onClick={() => setOpenModalDelete(!OpenModalDelete)}
                      className={classes.padding}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Grid>
                  </Grid>

                  <Grid container direction="row" className={classes.mainHeader}>
                    <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      <Typography style={{ 'color': 'red' }} variant="h5">
                        {I18n.t("action.supelement")}
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid

                    container

                    className={classes.mainContent}
                    spacing={4}
                  >

                    <Grid item container alignContent="flex-end" >
                      <Grid item md={3} sm={6} xs={6} alignContent="flex-end" className='text-center'>
                        <Button variant='outlined' onClick={() => setOpenModalDelete(!OpenModalDelete)} > {I18n.t("action.annuler")}</Button>
                      </Grid>
                      <Grid item md={3} sm={6} xs={6} alignContent="flex-end" className='text-center'>
                        <Button variant='outlined' onClick={() => deletePublicity()} > {I18n.t("action.supprimer")}</Button>
                      </Grid>


                    </Grid>

                  </Grid>

                </Grid>
              </Grid>
            </DialogContent>
          </Dialog>
        </ThemeProvider >
      </StylesProvider>


    )
  }






  const Component = () => (
    <>
      <p>{I18n.t("action.total")}: ({totalCount} {I18n.t("ads.annonce")})</p>
      <ConfigProvider direction={props.i18n.local == 'ar' ? 'rtl' : 'ltr'}>
      <Table columns={columns} dataSource={data} scroll={{ x: 1200, y: 3000 }} pagination={false} locale={{ emptyText: I18n.t("ads.liste") }} />
     
      <div className={props.i18n.locale === 'ar' ?"alignLeft float-left mt-2":"alignRight float-right mt-2"}>
        <Pagination  perPage={pageSize} total={totalCount} langue={props.i18n.locale} />
      </div>
      </ConfigProvider>

    </>
  );





  //-------------------------------------------//
  if (
    !verifyPermission(
         props.user.UserID,
        useRouter().pathname,
        props.user.UserType ? props.user.UserType : "simple",
        props.user.UserIsValid? props.user.UserIsValid : ""
    )
) {
    return (
        <>
               <React.Fragment>
                <VuroxLayout>
                  
                    <ContentLayout width="100%" className="p-3 vurox-scroll-y">
                        <Row align={props.i18n.locale === 'ar' ? "right" : "left"}>
                            <Col md="4"></Col>
                            <Col md="4">
                                <b
                                    className="text-center text-danger"
                                    style={{ marginTop: "210px", fontSize: "55px" }}
                                >
                                    {" "}
                                   {props.i18n.locale=="ar"?"ACCESS DENIED":"ACCESS DENIED"} 
                                </b>
                            </Col>
                            <Col md="4"></Col>
                        </Row>
                    </ContentLayout>
                </VuroxLayout>
            </React.Fragment>
        </>
    );
}
  return (
    <React.Fragment>
<div dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}>
      <HeaderLayout className="sticky-top">
        <HeaderDark />
      </HeaderLayout>
      <VuroxLayout>
        <VuroxSidebar width={240} className={`sidebar-container  ${toggleClass}`} >
          <Sidebar className={toggleClass} />
        </VuroxSidebar>
        <ContentLayout width='100%' className='p-3 vurox-scroll-y'>
          <Col md={36}>
            <Row className="vurox-admin-content-top-section"  align={props.i18n.locale === 'ar' ? "right" : "left"} >
              <Col md="12">
                <VuroxBreadcrumbs pagename={I18n.t("ads.annonce")} links={[]} />
              </Col>
            </Row>
            <Row className="vurox-admin-content-top-section" align={props.i18n.locale=== 'ar' ? "right" : "left"}>
            <Col md={2} xs={4}></Col>
              <Col md="5">
                <div className='vurox-search-from inline-flex mb-1'>
                  <label>{I18n.t("action.show_entries")}:</label>&nbsp;
                  <input value={pageSize} style={{ width: "60px", border: "1px solid #ccc", borderRadius: "10px", padding: "0 0px 0 4px" }} type="text" onChange={ChangePageSize} />


                </div>

              </Col>
              <Col md={2} xs={4}></Col>
              <Col md="5">
                <div className='vurox-search-from mb-1'>
                  {/* {props.icon} */}
                  <input type="text" placeholder={I18n.t("action.recherche") + " ..."} onChange={searchPublicity} />
                  <SearchOutlined />

                </div>
              </Col>
              <Col md={2} xs={4}></Col>
              <Col md="2" >
                <div className={props.i18n.locale === 'ar' ?"float-md-left mb-2":"float-md-right mb-2"}>
                  <button
                    onClick={() => {
                      { setOpenModal(true), setParam("add"), resetState(), validator.showMessages() }
                    }}
                    type="button"
                    className="dash_button float-none float-right mr-2 btn white bg-magenta-5 btn-sm rounded hover-color my-sm-2  d-block"
                    style={{
                      background: "#00C1D8",
                      color: "white",
                      fontSize: "16px",
                    }}
                  >
                    {I18n.t("action.ajouter")}
                  </button>

                </div>

              </Col>
            </Row>
            <br />
            <Row align={props.i18n.locale=== 'ar' ? "right" : "left"} >

              <Col md="12">
                {
                  loadingPublicity ?
                    <Spinner />
                    :
                    Component()


                }
              </Col>

            </Row>
          </Col>
          {OpenModal ? renderAddModalnew() : null}
          {OpenModalView ? renderViewModal() : null}
          {OpenModalEdit ? renderEditModalnew() : null}
          {OpenModalDelete ? renderDeleteModel() : null}
          {/* 
          <br /> <br /> <br /> <br /> <br /> <br />
          {Component()} */}
        </ContentLayout>

      </VuroxLayout>
</div>
    </React.Fragment>

  )
}
export default connect(state => state)(withStyles(styles)(ads_manager))
