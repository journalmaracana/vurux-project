import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import { API_LINK } from "../utils/constantes";
import {verifyPermission} from "../utils/functions";
import axios from "axios";
import Spinner from "../Components/spinner";
import { Pagination } from "../Components/pagination";
import { Markup } from "interweave";
import moment from 'moment'
import { connect } from 'react-redux'
import Chip from "@material-ui/core/Chip";
import Paper from '@mui/material/Paper'
import Swal from 'sweetalert2';
import Box from '@material-ui/core/Box';
import { I18n, Translate } from 'react-redux-i18n'
import { PlusSquareOutlined, EditOutlined } from '@ant-design/icons';
import Dialog from "@material-ui/core/Dialog";
import { CardMedia } from '@material-ui/core';
import DialogContent from "@material-ui/core/DialogContent";
import { SearchOutlined } from '@ant-design/icons';
import Grid from "@material-ui/core/Grid";
import { Cancel, Tag } from "@material-ui/icons";
import { Stack } from "@material-ui/core";

//import { Radio, RadioGroup, Checkbox } from '@material-ui/core';
// import Button from "@material-ui/core/Button";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { teal, grey } from "@material-ui/core/colors";
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Button from '@material-ui/core/Button';
import IconButton from "@material-ui/core/IconButton";
// import { getCookie, setCookie, deleteCookie, verifyPermission } from "../utils/functions"
import { UploadOutlined, InboxOutlined, LinkedinOutlined, MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import Autocomplete from "@material-ui/lab/Autocomplete";
import {
  MuiPickersUtilsProvider, KeyboardDatePicker,
  DatePicker as DateP,
  DateTimePicker as DateTP
} from "material-ui-pickers";
import { fr } from "date-fns/locale";
import ar from "date-fns/locale/ar-DZ";
import List from "@material-ui/core/List"
import InputLabel from "@material-ui/core/InputLabel";
import ListItem from "@material-ui/core/ListItem"
import DateFnsUtils from '@date-io/date-fns';
import ListItemText from "@material-ui/core/ListItemText"
import {
  Space,
  Divider,
  message,
  Radio, Table,
  Timeline,
  TimeRelatedForm,
  Tooltip, Upload, Icon, ConfigProvider
} from 'antd';
import { FaEye, FaTrash, FaPencilAlt, FaUserTimes } from 'react-icons/fa';
import { vuroxContext } from '../context/index'
import HeaderDark from '../Templates/HeaderDark';
import Sidebar from '../Templates/HeaderSidebar';
import TagsInput from "./TagsInput"
import {
  VuroxLayout,
  HeaderLayout,
  VuroxSidebar,
  ContentLayout,
} from "../Components/layout";

import SimpleReactValidator from "simple-react-validator";

import Item from "antd/lib/list/Item";
//import SunEditor from "suneditor-react";
//import "suneditor/dist/css/suneditor.min.css";
import dynamic from "next/dynamic";
import "suneditor/dist/css/suneditor.min.css"; // Import Sun Editor's CSS File to the _app.js
import { VuroxBreadcrumbs } from "../Components/breadcrumbs";
import { withStyles, MenuItem, ButtonGroup } from "@material-ui/core";
import { useRouter } from "next/router";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import VuroxFormSearch from "../Components/search"
import { StylesProvider, ThemeProvider, jssPreset } from "@material-ui/styles";
import { create } from "jss";
import rtl from "jss-rtl";
import { createMuiTheme } from "@material-ui/core/styles";
const KeyCodes = {
  comma: 188,
  enter: [10, 13],
};

let majid = []
const SunEditor = dynamic(() => import("suneditor-react"), { //besure to import dynamically
  ssr: false,
});

const styles = theme => ({
  root: {
    flexGrow: 5,
    fontSize: "1.5rem"
  },
  primaryColor: {
    color: "rgb(0, 193, 216)",
    fontSize: "27px"
  },
  secondaryColor: {
    color: grey[700]
  },

  padding: {
    padding: 0,

  },
  textFont: {
    fontSize: "1.5rem"
  },
  mainHeader: {
    backgroundColor: grey[100],
    padding: 20,
    alignItems: "center"
  },
  mainContent: {
    padding: 40,

  },
  secondaryContainer: {
    padding: "20px 25px",
    backgroundColor: grey[200]
  }
});
const Journal = (props) => {
  const [columns, setColumns] = useState([
    {
      title: I18n.t("journal.numero"),
      width: 100,
      dataIndex: 'num',
      key: 'num',
      //fixed: 'left'
    },
    { title: I18n.t("journal.date"), dataIndex: 'date', key: 'date', width: 100 },
    { title: I18n.t("journal.date_de_publication"), dataIndex: 'datepub', key: 'datepub', width: 100 },
    {
      title: I18n.t("action.action"),
      key: 'operation',
      dataIndex: 'action',
      //fixed: 'right',
      width: 150,
      //render: () => <OptionButtons />
    }]);




  const { classes, open, onClose } = props;

  const [editImage, SetEditImage] = useState(false);
  const [editPdf, SetEditPdf] = useState(false);
  const [journalID, setJournalID] = useState("")
  const { menuState } = useContext(vuroxContext)
  const [lng, setLang] = useState('FR');
  const [currentPage, setCurrentPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const [pageSize, setPageSize] = useState(50);
  const [publishedHour, setPublishedHour] = useState(moment.now());
  const [image, setImage] = useState("");
  const [pdf, setPdf] = useState("");
  const toggleClass = menuState ? 'menu-closed' : 'menu-open'
  const validator = new SimpleReactValidator({ autoForceUpdate: this });
  const [date, setDate] = useState(moment.now())
  const [OpenModal, setOpenModal] = useState(false)
  const [OpenModalEdit, setOpenModalEdit] = useState(false)
  const [OpenModalDelete, setOpenModalDelete] = useState(false)
  const [OpenModalView, setOpenModalView] = useState(false)
  const router = useRouter()
  const [journaux, setJournaux] = useState([])
  const [journalsSearch, setJournalsSearch] = useState([])
  const [numero, setNumero] = useState("")
  const [publier, setPublier] = useState("")
  const [data, setData] = useState([])
  const [stateR, setStateR] = useState('Nosearch')
  const [search, setSearch] = useState('')
  const [loadingJournal, setloadingJournal] = useState(true)
  const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
  const theme = createMuiTheme({
    direction: props.i18n.locale === 'fr' ? "ltr" : "rtl"
  });

  useEffect(() => {
    const pageQuery = router.query.page ? router.query.page : 1
    console.log("routerrr", pageQuery)
    console.log("prooops", I18n.t('journal.seterror'),)
    setCurrentPage(pageQuery)
    getJournals(pageQuery, pageSize)
  }, [router.isReady, router.query, search]);
  useEffect(() => {
    console.log('useEffect numero', numero)
  }, [numero])
  async function getJournals(index, limit) {
    console.log("before", search)
    setloadingJournal(true)
    let url = ""
    if (search) {
      url = API_LINK + 'v1/searchJournalsPagination/' + search + '/' + index + '/' + limit

    }
    else {
      url = API_LINK + 'v1/Journalpagination/' + index + '/' + limit

    }
    await axios.get(url)
      .then(res => {
        console.log("before res", res.data.journals)
        // setJournaux(res.data.journals)
        setData(res.data.journals.map((item) => {
          return {
            num: item.numero ? item.numero : null,
            date: item.date ? item.date.split("T")[0] : null,
            datepub: item.publishedHour ? item.publishedHour.split("T")[0] + ' ' + item.publishedHour.split("T")[1].split('.')[0] : null,
            action: (
              <OptionButtons
                onEdit={() => editModalJournal(item)}
                onView={() => viewModalJournal(item)}
                onDelete={() => deleteModalJournal(item)}
              />
            ),
          };
        }))
        setStateR('Nosearch')
        setTotalCount(res.data.count)
        setloadingJournal(false)
      })
      .catch(err => {
        console.log(err);
        setStateR('Nosearch')
        setloadingJournal(false)
      })
  }
  const searchJournalsPress = (e) => {
    console.log("seaarch", e.target.value)
    if (e) {
      setloadingJournal(true)
      setData([])
      if (e.target.value !== '') {

        setSearch(e.target.value)
      }
      else {
        setStateR('Nosearch')
        setSearch("")
      }
      getJournals(1, pageSize)
      router.replace({
        pathname: '/journal',
        query: { page: 1 }
      },
        undefined, { shallow: true }
      )
    }

  }
  /****************fin affichage */
  const ChangePageSize = (e) => {
    if (e) {
      if (e.target.value === '') {
        setPageSize(50)
        getJournals(1, 50)
        router.replace({
          pathname: '/journal',
          query: { page: 1 }
        },
          undefined, { shallow: true }
        )
      }
      else {
        setPageSize(e.target.value)
        getJournals(1, e.target.value)
        router.replace({
          pathname: '/journal',
          query: { page: 1 }
        },
          undefined, { shallow: true }
        )

      }
    }

  }


  const editModalJournal = (item) => {
    console.log("iteeemEdit", item);
    setJournalID(item._id)
    setLang(item.lng)
    setNumero(item.numero)
    setPublier(item.state)
    setImage(item.image)
    setPdf(item.pdf)
    setDate(item.date)
    setPublishedHour(item.publishedHour)
    setOpenModalEdit(true)

  }
  const deleteModalJournal = (item) => {
    setJournalID(item._id)
    setOpenModalDelete(true)
  }


  const OptionButtons = (props) => {
    return (
      <Fragment>
        <Button color="success" outline className="btn-sm mx-1" onClick={() => props.onView()}> <FaEye /> </Button>
        <Button color="success" outline className="btn-sm mx-1" onClick={() => props.onEdit()}> <FaPencilAlt /> </Button>
        <Button color="danger" outline className="btn-sm mx-1" onClick={() => props.onDelete()}> <FaTrash /> </Button>

      </Fragment>
    );
  }
  const tagRef = useRef();




  const handleChangeImage = (e) => {
    console.log("useEffect handle", e.target)
    if (e) {
      if (e.target.name === 'image') {

        setImage(e.target.files[0]),
          SetEditImage(true)
      }
      else if (e.target.name === 'pdf') {
        setPdf(e.target.files[0]),
          SetEditPdf(true)
      }
    }
  }

  const resetState = () => {
    setJournalID("")
    setNumero("")
    setDate(moment.now())
    setPublishedHour(moment.now())
    setPublier("")
    setImage("")
    setPdf("")
    setLang("FR")

  }

  const addJournal = async () => {
    let form_data = new FormData();

    form_data.append('lng', lng);
    form_data.append('numero', numero);
    const action = {
      user: props.user.UserID,
      date: moment()
    }
    form_data.append('date', date);
    if (publier === 'Maintenant') {
      form_data.append('publishedHour', date);
    } else {
      form_data.append('publishedHour', publishedHour);
    }
    form_data.append('journal', pdf);
    form_data.append('image', image);
    form_data.append('state', publier);
    form_data.append('action', JSON.stringify(action));

    await axios.post(API_LINK + 'v1/Journal', form_data, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
      .then(res => {
        Swal.fire({
          position: "center",
          icon: "success",
          title: "",
          text: I18n.t("journal.addsuccess"),
          confirmButtonText: I18n.t("action.oui"),
          showConfirmButton: true,
        })
        getJournals(currentPage, pageSize)
        resetState()
        setOpenModal(false)

      })
      .catch(err => {
        Swal.fire({
          icon: "error",
          title: "",
          confirmButtonColor: "#a6c76c",
          cancelButtonColor: "#a6c76c",
          confirmButtonText: I18n.t("action.oui"),
          text: I18n.t("journal.adderror"),
        });
        resetState()
        setOpenModal(false)
      })
  }
  const editJournal = async () => {
    let form_data = new FormData();

    form_data.append('lng', lng);
    form_data.append('numero', numero);
    const action = {
      user: props.user.UserID,
      date: moment()
    }
    form_data.append('date', date);
    if (publier === 'Maintenant') {
      form_data.append('publishedHour', date);
    } else {
      form_data.append('publishedHour', publishedHour);
    }
    form_data.append('journal', pdf);
    form_data.append('image', image);
    form_data.append('state', publier);
    form_data.append('action', JSON.stringify(action));

    await axios.put(API_LINK + 'v1/Journal/' + journalID, form_data, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
      .then(res => {
        console.log("useeeer", res)
        Swal.fire({
          position: "center",
          icon: "success",
          title: "",
          text: I18n.t("journal.setsuccess"),
          confirmButtonText: I18n.t("action.oui"),
          showConfirmButton: true,
        })
        getJournals(currentPage, pageSize)
        resetState()
        setOpenModalEdit(false)

      })
      .catch(err => {
        Swal.fire({
          icon: "error",
          title: "",
          confirmButtonColor: "#a6c76c",
          cancelButtonColor: "#a6c76c",
          confirmButtonText: I18n.t("action.oui"),
          text: I18n.t("journal.seterror"),
        });
        resetState()
        setOpenModalEdit(false)


      })
  }

  const renderAddModalnew = () => {

    const userType = props.user.UserType
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>

          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="xs"
            open={OpenModal}
            onClose={() => setOpenModal(!OpenModal)}
          >
            <Grid container >
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "left" : "right"}>
                    <IconButton
                      edge="end"
                      color="inherit"
                      aria-label="Close"
                      style={{ padding: 15 }}
                      onClick={() => setOpenModal(!OpenModal)}
                      className={classes.padding}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Grid>
                </Grid>

                <Grid container direction="row" className={classes.mainHeader} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                  <Grid item xs={12} className={classes.padding}>
                    <Typography className={classes.primaryColor} variant="h5">
                      {I18n.t("journal.add")}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  container
                  className={classes.mainContent}
                  spacing={4}
                  dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}
                >
                  <Grid>
                  </Grid>
                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      label={I18n.t("journal.langue")}
                      fullWidth
                      select
                      variant="outlined"
                      id="lng"
                      name="lng"
                      value={lng}
                      required
                      margin="dense"
                      onChange={(e) => setLang(e.target.value)}
                      onBlur={() => validator.showMessageFor('lng')}
                    >
                      <span style={{ 'color': 'red' }}>{validator.message('lng', lng, 'required',
                        {
                          messages: {
                            'required': I18n.t("journal.required"),
                          }
                        })}
                      </span>
                      <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'AR'} value="AR">{I18n.t("action.ar")}</MenuItem>
                      <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'FR'} value={'FR'}>{I18n.t("action.fr")}</MenuItem>
                    </TextField>
                  </Grid>

                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="outlined"
                      label={I18n.t("journal.numero")}
                      id="numéro"
                      name="numéro"
                      className={classes.textFont}
                      onChange={(e) => setNumero(e.target.value)}
                      required
                      onBlur={() => validator.showMessageFor('numéro')}
                    />
                    <span style={{ 'color': 'red' }}>{validator.message('numéro', numero, 'required',
                      {
                        messages: {
                          'required': I18n.t("journal.required")
                        }
                      })}
                    </span>

                  </Grid>
                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={props.i18n.locale === "ar" ? ar : fr} >
                      {/* <div style={{ display: "block" }}> */}
                      <DateP
                       
                        inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        okLabel={I18n.t("journal.ok")}
                        clearLabel={I18n.t("journal.clear")}
                        cancelLabel={I18n.t("journal.cancel")}
                        value={date ?   moment(moment(date, moment.date).toDate(), "DD-MM-YYYY").add(1, 'days') : ""}
                      
                        label={I18n.t("journal.date_parution")}

                        fullWidth
                        id="date"
                        name="date"
                        placeholder={props.i18n.locale === "ar" ? 'aaaa/mm/jj ' : 'jj/mm/aaaa'}
                        format={props.i18n.locale === "ar" ? "yyyy-MM-dd " : "dd-MM-yyyy "}
                        ampm={false}
                        onChange={(e) => { console.log("dateselected", moment(moment(e)).format("YYYY-MM-DD")), setDate(e) }}
                        required
                      />

                    </MuiPickersUtilsProvider>


                  </Grid>
                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      label={I18n.t("journal.publier")}
                      fullWidth
                      select
                      variant="outlined"
                      id="state" name='state'
                      value={publier}
                      required
                      margin="dense"
                      onChange={(e) => setPublier(e.target.value)}
                      onBlur={() => validator.showMessageFor('publier')}
                    >
                      <span style={{ 'color': 'red' }}>
                        {validator.message('publier', publier, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                      </span>
                      <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'u'} value='u'>--</MenuItem>
                      <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'Maintenant'} value='Maintenant'>{I18n.t("action.maintenant")}</MenuItem>
                      <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'Programmer'} value='Programmer'>{I18n.t("action.programmer")}</MenuItem>
                    </TextField>
                  </Grid>
                  {publier == "Programmer" ?
                    <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={props.i18n.locale === "ar" ? ar : fr}>
                        {/* <div style={{ display: "block" }}> */}
                        <DateTP

                          inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          okLabel={I18n.t("journal.ok")}
                          clearLabel={I18n.t("journal.clear")}
                          cancelLabel={I18n.t("journal.cancel")}
                          value={publishedHour ? moment(publishedHour, moment.date).toDate() : ""}
                          label={I18n.t("journal.date_parution")}
                          placeholder={props.i18n.locale === "ar" ? '--:-- aaaa/mm/jj ' : 'jj/mm/aaaa --:--'}
                          fullWidth
                          id="publishedHour"
                          name="publishedHour"
                          format={props.i18n.locale === "ar" ? "yyyy-MM-dd mm:HH " : "dd-MM-yyyy HH:mm "}
                          ampm={false}
                          onChange={(e) => { console.log("dateselected", moment(moment(e)).format("YYYY-MM-DDTHH:mm")), setPublishedHour(e) }}
                          required

                        />

                      </MuiPickersUtilsProvider>


                    </Grid>
                    : null

                  }


                  <Grid
                    item xs={12}
                    className={classes.padding}
                    align={props.i18n.locale === 'ar' ? "right" : "left"}
                  >
                    <input
                      className="text-center"

                      type="file"
                      id="select-image"
                      name="pdf"
                      onChange={(e) => {
                        setPdf(e.target.files[0]),
                          SetEditPdf(true)
                      }}
                      style={{ display: "none" }}
                      onBlur={() => validator.showMessageFor("pdf")}
                    />

                    <span
                      style={{ color: "red" }}>
                      {validator.message("pdf", pdf, "required", {
                        messages: {
                          required: I18n.t("journal.required"),
                        },
                      })}
                    </span>
                    <label htmlFor="select-image" className="text-center">
                      <Button variant="contained" component="span">
                        {I18n.t("journal.importPDF")}
                      </Button>
                    </label>
                    <br />

                    {pdf ?
                      <span
                        style={{ color: "grey", fontSize: "14px" }}>
                        {pdf.name}
                      </span> : null
                    }
                  </Grid>
                  <Grid
                    item xs={12}
                    className={classes.padding}
                    align={props.i18n.locale === 'ar' ? "right" : "left"}
                  >

                    <input
                      className="text-center"
                      accept="image/*"
                      type="file"
                      id="select-pdf"
                      name="image"
                      onChange={(e) => {
                        console.log("imaaage", e), setImage(e.target.files[0]),
                          SetEditImage(true)
                      }}
                      style={{ display: "none" }}
                      onBlur={() => validator.showMessageFor("image")}
                    />

                    <span
                      style={{ color: "red" }}>
                      {validator.message("image", image, "required", {
                        messages: {
                          required: I18n.t("journal.required"),
                        },
                      })}
                    </span>
                    <label htmlFor="select-pdf" className="text-center">
                      <Button variant="contained" component="span">
                        {I18n.t("journal.importCouv")}
                      </Button>
                    </label>
                    <br />
                    {image ?

                      <span
                        style={{ color: "grey", fontSize: "14px" }}>
                        {image.name}
                      </span> : null
                    }
                  </Grid>
                  <Grid
                    item xs={12}
                    className={classes.padding}
                    align={props.i18n.locale === 'ar' ? "right" : "left"}
                  >

                    <span style={{ color: 'red', fontSize: "18px" }}>
                      {I18n.t("regex.champs")}
                    </span>
                  </Grid>

                  <Divider><small></small></Divider>
                  <Grid item container alignContent="flex-end" >

                    <Grid item md={4} sm={4} xs={4} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                      {publier === "Programmer" ? <Button disabled={!validator.allValid()} variant='outlined' onClick={() => addJournal()}>  {I18n.t("action.programmer")}</Button> :
                        <Button variant='outlined' disabled={!validator.allValid()} onClick={() => addJournal()}>  {I18n.t("action.publier")}</Button>}
                    </Grid>
                    <Grid item md={4} sm={4} xs={4} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    </Grid>
                    <Grid item md={4} sm={4} xs={4} alignContent="flex-end" align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      <Button variant='outlined' onClick={() => setOpenModal(!OpenModal)}>  {I18n.t("action.fermer")}</Button>
                    </Grid>

                  </Grid>

                </Grid>

              </Grid>

            </Grid>
          </Dialog>
        </ThemeProvider >
      </StylesProvider>

    )

  }
  const renderEditModalnew = () => {
    const userType = props.user.UserType
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="xs"
            open={OpenModalEdit}
            onClose={() => setOpenModalEdit(!OpenModalEdit)}

          >
            <Grid container>
              <Grid item xs={12}>
                <Grid container>
                  <Grid container>
                    <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "left" : "right"}>
                      <IconButton
                        edge="end"
                        color="inherit"

                        aria-label="Close"
                        style={{ padding: 15 }}
                        onClick={() => setOpenModalEdit(!OpenModalEdit)}
                        className={classes.padding}
                      >
                        <CloseIcon />
                      </IconButton>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid container direction="row" className={classes.mainHeader} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                  <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <Typography className={classes.primaryColor} variant="h5">
                      {I18n.t("journal.set")}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid

                  container
                  // dir={getCookie('lang') == 'ar' ? 'rtl' : 'ltr'}
                  className={classes.mainContent}
                  spacing={4}
                  dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}
                >
                  <Grid>
                  </Grid>
                  <>
                    <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                      <TextField
                        inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        label={I18n.t("journal.langue")}
                        fullWidth
                        select
                        variant="outlined"
                        id="lng"
                        name="lng"
                        value={lng}
                        required
                        margin="dense"
                        onChange={(e) => setLang(e.target.value)}
                        onBlur={() => validator.showMessageFor('lng')}
                      >
                        <span style={{ 'color': 'red' }}>{validator.message('lng', lng, 'required',
                          {
                            messages: {
                              'required': I18n.t("journal.required"),
                            }
                          })}
                        </span>
                        <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'AR'} value="AR">{I18n.t("action.ar")}</MenuItem>
                        <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'FR'} value={'FR'}>{I18n.t("action.fr")}</MenuItem>
                      </TextField>
                    </Grid>

                    <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                      <TextField
                        fullWidth
                        inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        margin="dense"
                        variant="outlined"
                        label={I18n.t("journal.numero")}
                        id="numero"
                        name="numero"
                        value={numero}
                        className={classes.textFont}
                        onChange={(e) => setNumero(e.target.value)}
                        required
                        onBlur={() => validator.showMessageFor('numéro')}
                      />
                      <span style={{ 'color': 'red' }}>{validator.message('numéro', numero, 'required',
                        {
                          messages: {
                            'required': I18n.t("journal.required")
                          }
                        })}
                      </span>

                    </Grid>
                    <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={props.i18n.locale === "ar" ? ar : fr}>
                        {/* <div style={{ display: "block" }}> */}
                        <DateP

                          inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                          okLabel={I18n.t("journal.ok")}
                          clearLabel={I18n.t("journal.clear")}
                          cancelLabel={I18n.t("journal.cancel")}
                          value={date ? moment(date, moment.date).toDate() : ""}
                          label={I18n.t("journal.date_parution")}
                          fullWidth
                          id="date"
                          name="date"
                          placeholder={props.i18n.locale === "ar" ? 'aaaa/mm/jj ' : 'jj/mm/aaaa'}
                          format={props.i18n.locale === "ar" ? "yyyy-MM-dd " : "dd-MM-yyyy "}
                          ampm={false}
                          onChange={(e) => { console.log("dateselected", moment(moment(e)).format("YYYY-MM-DD")), setDate(e) }}
                          required
                        />

                      </MuiPickersUtilsProvider>


                    </Grid>
                    <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      <TextField
                        inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                        label={I18n.t("journal.publier")}
                        fullWidth
                        select
                        variant="outlined"
                        id="state" name='state'
                        value={publier}
                        required
                        margin="dense"
                        onChange={(e) => setPublier(e.target.value)}
                        onBlur={() => validator.showMessageFor('publier')}
                      >
                        <span style={{ 'color': 'red' }}>
                          {validator.message('publier', publier, 'required',
                            {
                              messages: {
                                'required': I18n.t("journal.required"),
                              }
                            })}
                        </span>
                        <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'u'} value='u'>--</MenuItem>
                        <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'Maintenant'} value='Maintenant'>{I18n.t("action.maintenant")}</MenuItem>
                        <MenuItem style={props.i18n.locale == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }} key={'Programmer'} value='Programmer'>{I18n.t("action.programmer")}</MenuItem>
                      </TextField>
                    </Grid>
                    {publier == "Programmer" ?
                      <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={props.i18n.locale === "ar" ? ar : fr}>
                          {/* <div style={{ display: "block" }}> */}
                          <DateTP

                            inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                            okLabel={I18n.t("journal.ok")}
                            clearLabel={I18n.t("journal.clear")}
                            cancelLabel={I18n.t("journal.cancel")}
                            value={publishedHour ? moment(publishedHour, moment.date).toDate() : moment.now()}
                            label={I18n.t("journal.date_de_publication")}
                            placeholder={props.i18n.locale === "ar" ? '--:-- aaaa/mm/jj ' : 'jj/mm/aaaa --:--'}
                            fullWidth
                            id="publishedHour"
                            name="publishedHour"
                            format={props.i18n.locale === "ar" ? " yyy-MM-dd mm:HH" : "dd-MM-yyyy HH:mm "}
                            ampm={false}

                            onChange={(e) => { console.log("dateselected", moment(moment(e)).format("YYYY-MM-DDTHH:mm")), setPublishedHour(e) }}
                            required

                          />

                        </MuiPickersUtilsProvider>


                      </Grid>
                      : null

                    }
                    <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      <input
                        className='text-center'
                        variant='outlined'
                        type="file"
                        id="select-pdf"
                        name={'pdf'}
                        label={I18n.t('dash.articles.image')}
                        style={{ display: 'none' }}
                        onChange={handleChangeImage}

                      />

                      <label htmlFor="select-pdf" className='text-center'>
                        <Button variant="contained" component="span">
                          {I18n.t("journal.modifPDF")}
                        </Button>
                      </label>
                      <span
                        style={{ color: "red" }}>
                        {validator.message("pdf", pdf, "required", {
                          messages: {
                            required: I18n.t("journal.required"),
                          },
                        })}
                      </span>
                      {pdf && (

                        <Box xs={{ flexDirection: 'row' }}>
                          <a target="_blank" style={{ color: "rgb(0, 193, 216)", fontSize: "15px" }} href={API_LINK + pdf} >{I18n.t("journal.lien")}</a>
                        </Box>

                      )}


                    </Grid>
                    <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      <input
                        className='text-center'
                        variant='outlined'
                        accept="image/*"
                        type="file"
                        id="select-image"
                        name={'image'}
                        label=""
                        style={{ display: 'none' }}
                        onChange={handleChangeImage}
                      />

                      <label htmlFor="select-image" className='text-center'>
                        <Button variant="contained" component="span">
                          {I18n.t("journal.modifCouv")}
                        </Button>
                      </label>
                      <span
                        style={{ color: "red" }}>
                        {validator.message("image", image, "required", {
                          messages: {
                            required: I18n.t("journal.required"),
                          },
                        })}
                      </span>
                      {image && (

                        <Box mt={2} xs={{ flexDirection: 'row' }}>
                          <img src={image ? API_LINK + image : null} height="100px" />
                        </Box>

                      )}


                    </Grid>


                    <Divider><small></small></Divider>
                    <Grid item container alignContent="flex-end" >

                      <Grid item md={4} sm={4} xs={4} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <Button variant='outlined' disabled={!validator.allValid()} onClick={() => editJournal()}>{I18n.t("action.modifier")}</Button>
                      </Grid>
                      <Grid item md={4} sm={4} xs={4} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                      </Grid>
                      <Grid item md={4} sm={4} xs={4} alignContent="flex-end" align={props.i18n.locale === 'ar' ? "right" : "left"}>
                        <Button variant='outlined' onClick={() => setOpenModalEdit(!OpenModalEdit)}>{I18n.t("action.fermer")}</Button>
                      </Grid>

                    </Grid>


                  </>


                </Grid>

              </Grid>

            </Grid>
          </Dialog>
        </ThemeProvider >
      </StylesProvider>

    )


  }
  const deleteJournal = async () => {
    await axios.delete(API_LINK + 'v1/Journal/' + journalID).then(res => {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "",
        text: I18n.t("journal.deletesuccess"),
        confirmButtonText: I18n.t("action.oui"),
        showConfirmButton: true,
      })
      getJournals(currentPage, pageSize)
      resetState()
      setOpenModalDelete(false)

    })
      .catch(err => {
        Swal.fire({
          icon: "error",
          title: "",
          confirmButtonColor: "#a6c76c",
          cancelButtonColor: "#a6c76c",
          confirmButtonText: I18n.t("action.oui"),
          text: I18n.t("journal.deleteerror")
        });
        resetState()
        setOpenModalDelete(false)
      })
  }



  const renderDeleteModel = () => {
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="sm"
            open={OpenModalDelete}
            onClose={() => setOpenModalDelete(!OpenModalDelete)}

          >
            <DialogContent className={classes.padding}>
              <Grid container>
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "left" : "right"}>
                      <IconButton
                        edge="end"
                        color="inherit"

                        aria-label="Close"
                        style={{ padding: 15 }}
                        onClick={() => setOpenModalDelete(!OpenModalDelete)}
                        className={classes.padding}
                      >
                        <CloseIcon />
                      </IconButton>
                    </Grid>
                  </Grid>

                  <Grid container direction="row" className={classes.mainHeader} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <Grid item xs={12} className={classes.padding} >
                      <Typography style={{ 'color': 'red' }} variant="h5">
                        {I18n.t("action.supelement")}
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid

                    container

                    className={classes.mainContent}
                    spacing={4}
                    dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}
                  >

                    <Grid item container alignContent="flex-end" align={props.i18n.locale === 'ar' ? "right" : "left"} >
                      <Grid item md={3} sm={6} xs={6} alignContent="flex-end" className='text-center'>
                        <Button variant='outlined' onClick={() => setOpenModalDelete(!OpenModalDelete)} >   {I18n.t("action.annuler")}</Button>
                      </Grid>
                      <Grid item md={3} sm={6} xs={6} alignContent="flex-end" className='text-center'>
                        <Button variant='outlined' onClick={() => deleteJournal()} >    {I18n.t("action.supprimer")}</Button>
                      </Grid>


                    </Grid>

                  </Grid>

                </Grid>
              </Grid>
            </DialogContent>
          </Dialog>
        </ThemeProvider >
      </StylesProvider>

    )
  }

  const viewModalJournal = (item) => {
    console.log("updaaate", item)
    setOpenModalView(true)
    setJournalID(item._id)
    setLang(item.lng)
    setNumero(item.numero)
    setPublier(item.state)
    setImage(item.image)
    setPdf(item.pdf)
    setDate(item.date)
    setPublishedHour(item.publishedHour)

  }
  const renderViewModal = () => {
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Dialog
            className={classes.root}
            fullWidth
            maxWidth="sm"
            open={OpenModalView}
            onClose={() => setOpenModalView(!OpenModalView)}

          >
            <Grid container>
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} className={classes.padding} align={props.i18n.locale === 'ar' ? "left" : "right"}>
                    <IconButton
                      edge="end"
                      color="inherit"

                      aria-label="Close"
                      style={{ padding: 15 }}
                      onClick={() => setOpenModalView(!OpenModalView)}
                      className={classes.padding}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Grid>
                </Grid>

                <Grid container direction="row" className={classes.mainHeader} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                  <Grid item xs={12} className={classes.padding}>
                    <Typography className={classes.primaryColor} variant="h5">
                      {I18n.t("journal.infos")}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  container
                  className={classes.mainContent}
                  spacing={4}
                  dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}
                >


                  <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="standard"
                      label={I18n.t("journal.langue")}
                      id="lng"
                      name="lng"
                      value={lng == "FR" ? "Français" : 'Arabe'}
                      className={classes.textFont}
                      ariaReadonly="true"
                    />

                  </Grid>
                  <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="standard"
                      label={I18n.t("journal.numero")}
                      id="num"
                      name="num"
                      value={numero ? numero : '/'}
                      className={classes.textFont}
                      ariaReadonly="true"
                    />

                  </Grid>
                  <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="standard"
                      label={I18n.t("journal.etat")}
                      id="etat"
                      name="etat"
                      value={publier ? publier=="Programmer" ?I18n.t("action.programmer"):I18n.t("action.maintenant"): '/'}
                      className={classes.textFont}
                      ariaReadonly="true"
                    />

                  </Grid>

                  <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="standard"
                      label={I18n.t("journal.date_parution")}
                      id="date"
                      name="date"
                      value={moment(date).format('DD-MM-YYYY')}
                      className={classes.textFont}
                      aria-readonly="true"
                    />

                  </Grid>
                  <Grid item md={6} sm={6} xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <TextField
                      inputProps={props.i18n.locale === "ar" ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                      fullWidth
                      margin="dense"
                      variant="standard"
                      label={I18n.t("journal.date_de_publication")}
                      id="datef"
                      name="datef"
                      value={moment(publishedHour).format('DD-MM-YYYY HH:mm')}
                      className={classes.textFont}
                      aria-readonly="true"
                    />

                  </Grid>
                  <Divider></Divider>
                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"}>
                    <h5>{I18n.t("journal.couverture")} : </h5>
                    <Box mt={2} textAlign="center">
                      <img src={image ? API_LINK + image : null} height="100px" />
                    </Box>
                  </Grid>
                  <Grid item xs={12} align={props.i18n.locale === 'ar' ? "right" : "left"} >
                    <h5>{I18n.t("journal.pdf")}  : </h5>
                    <Box  >
                      <a target="_blank" style={{ color: "rgb(0, 193, 216)", fontSize: "15px" }} href={API_LINK + pdf} >{I18n.t("journal.lien")} </a>

                    </Box></Grid>
                  <Divider></Divider>
                </Grid>
              </Grid>
            </Grid>
          </Dialog>
        </ThemeProvider >
      </StylesProvider>


    )
  }




  const Component = () => (
    <>
      <p>{I18n.t("action.total")}: ({totalCount} {I18n.t("journal.journal")})</p>
      <ConfigProvider direction={props.i18n.local == 'ar' ? 'rtl' : 'ltr'}>
        <Table columns={columns} className='text-right' dataSource={data} scroll={{ x: 1200, y: 3000 }} pagination={false} locale={{ emptyText: I18n.t("journal.liste") }} />

        <div className={props.i18n.locale === 'ar' ? "alignLeft float-left mt-2" : "alignRight float-right mt-2"}>
          <Pagination perPage={pageSize} total={totalCount} langue={props.i18n.locale} />
        </div>
      </ConfigProvider>

    </>
  );





  //-------------------------------------------//
  if (
    !verifyPermission(
         props.user.UserID,
        useRouter().pathname,
        props.user.UserType ? props.user.UserType : "simple",
        props.user.UserIsValid? props.user.UserIsValid : ""
    )
) {
    return (
        <>
            <React.Fragment>
                <VuroxLayout>
                  
                    <ContentLayout width="100%" className="p-3 vurox-scroll-y">
                        <div align={props.i18n.locale === 'ar' ? "right" : "left"}>
                            <div className='md-4'></div>
                            <div className='md-4'>
                                <b
                                    className="text-center text-danger"
                                    style={{ marginTop: "210px", fontSize: "55px" }}
                                >
                                    {" "}
                                   {props.i18n.locale=="ar"?"ACCESS DENIED":"ACCESS DENIED"} 
                                </b>
                            </div>
                            <div className='md-4'></div>
                        </div>
                    </ContentLayout>
                </VuroxLayout>
            </React.Fragment>
        </>
    );
}
  return (
    <React.Fragment>
      <div dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}>
        <HeaderLayout className="sticky-top">
          <HeaderDark />
        </HeaderLayout>
        <VuroxLayout>
          <VuroxSidebar width={240} className={`sidebar-container  ${toggleClass}`} >
            <Sidebar className={toggleClass} />
          </VuroxSidebar>
          <ContentLayout width='100%' className='p-3 vurox-scroll-y'>
            <div className ='md-36'>
              <div className="vurox-admin-content-top-section" align={props.i18n.locale === 'ar' ? "right" : "left"} >
                <div className ='md-12'>
                  <VuroxBreadcrumbs pagename={I18n.t("journal.journal")} links={[]} />
                </div>
              </div >
              
             
              <div className="vurox-admin-content-top-section" align={props.i18n.locale === 'ar' ? "right" : "left"}>

                <div md="5">
                  <div className='vurox-search-from inline-flex mb-1'>
                    <label>{I18n.t("action.show_entries")}:</label>&nbsp;
                    <input style={{ width: "60px", border: "1px solid #ccc", borderRadius: "10px", padding: "0 0px 0 4px" }} type="text" onChange={ChangePageSize} />


                  </div>

                </div>
               
                <div className='md-5'>
                  <div className='vurox-search-from mb-1'>
                    {/* {props.icon} */}
                    <input type="text" placeholder={I18n.t("action.recherche") + " ..."} onChange={searchJournalsPress} />
                    <SearchOutlined />

                  </div>
                </div>
                <div className ='md-2 xs-6'></div>
                <div className='md-2' >

                  <div className={props.i18n.locale === 'ar' ? "float-md-left mb-2" : "float-md-right mb-2"}>
                    <button
                      onClick={() => {
                        { setOpenModal(true), resetState(), validator.showMessages() }
                      }}
                      type="button"
                      className="dash_button float-none float-right mr-2 btn white bg-magenta-5 btn-sm rounded hover-color my-sm-2  d-block"
                      style={{
                        background: "#00C1D8",
                        color: "white",
                        fontSize: "16px",
                      }}
                    >
                      {I18n.t("action.ajouter")}
                    </button>

                  </div>

                </div>
              </div>
              <br />
              <div align={props.i18n.locale === 'ar' ? "right" : "left"}>

                <div className='md-12' >
                  {
                    loadingJournal ?
                      <Spinner />
                      :
                      Component()


                  }
                </div>

              </div>
            </div>
            {OpenModal ? renderAddModalnew() : null}
            {OpenModalView ? renderViewModal() : null}
            {OpenModalEdit ? renderEditModalnew() : null}
            {OpenModalDelete ? renderDeleteModel() : null}
            {/* 
          <br /> <br /> <br /> <br /> <br /> <br />
          {Component()} */}
          </ContentLayout>

        </VuroxLayout>
      </div>
    </React.Fragment>

  )
}
export default connect(state => state)(withStyles(styles)(Journal))
