import React, { useState, useContext, Fragment, useEffect, useRef } from 'react'
import { Divider, Typography, Space, Comment, Tooltip, Avatar, Input, Form, List, Row, Col, Radio, Select } from 'antd'
const { Title, Paragraph } = Typography
const { TextArea } = Input
import { Table, ConfigProvider } from 'antd'
import moment from 'moment'
import HeaderDark from 'Templates/HeaderDark';
import Sidebar from 'Templates/HeaderSidebar';
import {
    VuroxLayout,
    HeaderLayout,
    VuroxSidebar,
    ContentLayout,
    VuroxComponentsContainer
} from 'Components/layout'
import { Pagination } from '../Components/pagination'
import { vuroxContext } from '../context'
import Box from '@material-ui/core/Box';
import axios from 'axios'
import { API_LINK, CODE_PHONE, Domaine } from '../utils/constantes'
import Button from '@material-ui/core/Button';
import { teal, grey } from "@material-ui/core/colors";
import { FaEye, FaTrash, FaPencilAlt, FaUserTimes, FaFirstAid, FaPlus } from 'react-icons/fa';
import Dialog from "@material-ui/core/Dialog";
import { CardMedia, MenuItem, ButtonGroup, withStyles } from '@material-ui/core';
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import TextField from "@material-ui/core/TextField";
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import Swal from 'sweetalert2';
import SimpleReactValidator from "simple-react-validator";
import { useRouter } from 'next/router'
import Spinner from '../Components/spinner'
import { SearchOutlined } from '@ant-design/icons';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider, KeyboardDatePicker,
    DatePicker as DateP,
    DateTimePicker as DateTP
} from "material-ui-pickers";
import { fr } from "date-fns/locale";
import ar from "date-fns/locale/ar-DZ";
import { I18n } from 'react-redux-i18n'
import { connect } from 'react-redux'
import { createMuiTheme } from "@material-ui/core/styles";
import { StylesProvider, ThemeProvider, jssPreset } from "@material-ui/styles";
import { create } from "jss";
import rtl from "jss-rtl";
import { verifyPermission } from '../utils/functions'


const styles = theme => ({
    root: {
        flexGrow: 5,
        fontSize: "1.5rem"
    },
    primaryColor: {
        color: "rgb(0, 193, 216)",
        fontSize: "27px"
    },
    secondaryColor: {
        color: grey[700]
    },

    padding: {
        padding: 0,

    },
    textFont: {
        fontSize: "1.5rem"
    },
    mainHeader: {
        backgroundColor: grey[100],
        padding: 20,
        alignItems: "center"
    },
    mainContent: {
        padding: 40,

    },
    secondaryContainer: {
        padding: "20px 25px",
        backgroundColor: grey[200]
    }
});
const Users = (props) => {
    console.log("user", props)
    const langPage = props.i18n.locale
    const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
    const theme = createMuiTheme({
        direction: langPage === 'fr' ? "ltr" : "rtl"
    });

    const OptionButtons = (props) => {
        return (
            <Fragment>
                <Button color="success" outline className="btn-sm mx-1" onClick={() => props.onView()}> <FaEye /> </Button>
                <Button color="success" outline className="btn-sm mx-1" onClick={() => props.onEdit()}> <FaPencilAlt /> </Button>
                <Button color="danger" outline className="btn-sm mx-1" onClick={() => props.onDelete()}> <FaTrash /> </Button>

            </Fragment>
        );
    }

    const [columns, setColumns] = useState([
        {
            title: I18n.t('admin.nom'),
            width: 100,
            dataIndex: 'name',
            key: 'name',
            //fixed: 'left'
        },
        { title: I18n.t('admin.prenom'), dataIndex: 'lastname', key: 'lastname', width: 100 },
        { title: I18n.t('admin.mail'), dataIndex: 'email', key: 'email', width: 100 },
        { title: I18n.t('admin.pseudo'), dataIndex: 'pseudo', key: 'pseudo', width: 100 },
        { title: I18n.t('admin.sexe'), dataIndex: 'sexe', key: 'sexe', width: 100 },
        { title: I18n.t('admin.state'), dataIndex: 'isvalid', key: 'isvalid', width: 50 },
        {
            title: I18n.t('action.action'),
            key: 'operation',
            dataIndex: 'action',
            //fixed: 'right',
            width: 100,
            //render: () => <OptionButtons />
        }]);
    const [columnsPer, setColumnsPer] = useState([
        {
            title: I18n.t('admin.auth'),
            width: 100,
            dataIndex: 'name',
            key: 'name',
            //fixed: 'left'
        },
        { title: I18n.t('perm.permission'), dataIndex: 'per', key: 'per', width: 100 },
        {
            title: I18n.t('action.action'),
            key: 'operation',
            dataIndex: 'action',
            //fixed: 'right',
            width: 150,
            //render: () => <OptionButtons />
        }]);

    const { classes, open, onClose } = props;
    const { menuState } = useContext(vuroxContext)
    const toggleClass = menuState ? 'menu-closed' : 'menu-open'
    const validator = new SimpleReactValidator({ autoForceUpdate: this });
    const [typeIndex, setTypeIndex] = useState(1)
    const [search, setSearch] = useState("")
    const [users, setUsers] = useState([])
    const [clientUsers, setClientUsers] = useState([])
    const [userPer, setUserPer] = useState([])
    const [permissions, setPermissions] = useState([])
    const [loadingUsers, setLoadingUsers] = useState(true)
    const [loadingClients, setLoadingClients] = useState(true)
    const [stateR, setStateR] = useState("")
    const [currentPage, setCurrentPage] = useState(1);
    const [totalCount, setTotalCount] = useState(0);
    const [totalCountClient, setTotalCountClient] = useState(0);
    const [pageSize, setPageSize] = useState(20);
    const [name, setName] = useState("")
    const [nameAR, setNameAR] = useState("")
    const [lastname, setLastname] = useState("")
    const [lastnameAR, setLastnameAR] = useState("")
    const [mail, setMail] = useState("")
    const [password, setPassword] = useState("")
    const [sexe, setSexe] = useState("")
    const [adress, setAdress] = useState("")
    const [adressAR, setAdressAR] = useState("")
    const [codePhone, setPhoneCode] = useState("")
    const [numberPhone, setPhoneNumber] = useState("")
    const [pseudo, setPseudo] = useState("")
    const [pseudoAR, setPseudoAR] = useState("")
    const [userID, setUserID] = useState("")
    const [OpenModalAdd, setOpenModalAdd] = useState(false);
    const [OpenModalDelete, setOpenModalDelete] = useState(false);
    const [OpenModalEdit, setOpenModalEdit] = useState(false);
    const [OpenModalAddClient, setOpenModalAddClient] = useState(false);
    const [isActive, setIsActive] = useState("");
    const [OpenModalEditClient, setOpenModalEditClient] = useState(false);
    const [phone, setPhone] = useState("");
    const [date, setDate] = useState("");
    const [picture, setPicture] = useState("");
    const [type, setType] = useState("");
    const [type2, setType2] = useState("");
    const [OpenModalViewClient, setOpenModalViewClient] = useState(false)
    const [OpenModalView, setOpenModalView] = useState(false)
    const [permissionsAll, setPermissionsAll] = useState([]);
    const router = useRouter()

    useEffect(() => {
        const pageQuery = router.query.page ? router.query.page : 1
        console.log("routerrr", pageQuery)
        setCurrentPage(pageQuery)
        getAdmins(pageQuery, pageSize)
        getClientUsers(pageQuery, pageSize)
        countUsers()
        countClients()
    }, [router.isReady, router.query, search]);
    useEffect(() => {
        console.log('useEffect numero', totalCount)
    }, [totalCount])
    useEffect(() => {
        console.log('langPage', langPage)
        setColumns([

            {
                title: I18n.t('admin.nom'),
                width: 100,
                dataIndex: 'name',
                key: 'name',
                //fixed: 'left'
            },
            { title: I18n.t('admin.prenom'), dataIndex: 'lastname', key: 'lastname', width: 100 },
            { title: I18n.t('admin.mail'), dataIndex: 'email', key: 'email', width: 100 },
            { title: I18n.t('admin.pseudo'), dataIndex: 'pseudo', key: 'pseudo', width: 100 },
            { title: I18n.t('admin.sexe'), dataIndex: 'sexe', key: 'sexe', width: 100 },
            { title: I18n.t('admin.state'), dataIndex: 'isvalid', key: 'isvalid', width: 50 },
            {
                title: I18n.t('action.action'),
                key: 'operation',
                dataIndex: 'action',
                //fixed: 'right',
                width: 100,
                //render: () => <OptionButtons />
            }
        ]),


            setColumnsPer([
                {
                    title: I18n.t('admin.auth'),
                    width: 100,
                    dataIndex: 'name',
                    key: 'name',
                    //fixed: 'left'
                },
                { title: I18n.t('perm.permission'), dataIndex: 'per', key: 'per', width: 100 },
                {
                    title: I18n.t('action.action'),
                    key: 'operation',
                    dataIndex: 'action',
                    //fixed: 'right',
                    width: 150,
                    //render: () => <OptionButtons />
                }
            ])


    }, [langPage])

    const getAdmins = async (index, limit) => {
        let url = ""
        if (search) {
            url = API_LINK + 'v1/users-pagination-search/' + search + '/' + 'simple' + '/' + index + '/' + limit

        }
        else {
            url = API_LINK + 'v1/users-pagination/type/' + 'simple' + '/' + index + '/' + limit
        }
        await axios.get(url)
            .then(res => {
                console.log('admiiiin', res.data)
                setUsers(res.data.users.map((item) => {
                    return {
                        name: item.name ? item.name : '/',
                        lastname: item.lastname ? item.lastname : '/',
                        email: item.mail ? item.mail : '/',
                        pseudo: item.pseudo ? item.pseudo : '/',
                        sexe: item.sexe ? item.sexe : '/',
                        isvalid: item.isvalid === true ? "Compte Valide" : "Compte Non Valide",
                        action: (
                            <OptionButtons
                                onEdit={() => editModal(item)}
                                onView={() => showModal(item)}
                                onDelete={() => deleteModal(item)}
                            />
                        ),
                    };
                }))
                if (res.data.users.length) {
                    setPermissionsAll(res.data.users.map(item => {
                        if (item.roles.permissions.length) {
                            return { ID: item._id, Permission: item.roles.permissions }
                        }
                        else return []
                    }))
                }
                setUserPer(res.data.users)
                setStateR('Nosearch')
                setLoadingUsers(false)//count
            })
            .catch(err => {
                console.log(err)
                setLoadingUsers(false)
                setStateR('Nosearch')
            })

    }
    const countUsers = () => {
        axios.get(API_LINK + 'v1/user/type/' + "simple").then(res => {
            setTotalCount(res.data.length)
        }).catch(err => {
            console.log('ere', err)
        })
    }
    const countClients = () => {
        axios.get(API_LINK + 'v1/user/type/' + "Client").then(res => {
            setTotalCountClient(res.data.length)
        }).catch(err => {
            console.log('ere', err)
        })
    }
    const getClientUsers = async (index, limit) => {
        let url = ""
        if (search) {
            url = API_LINK + 'v1/users-pagination-search/' + search + '/' + 'Client' + '/' + index + '/' + limit

        }
        else {
            url = API_LINK + 'v1/users-pagination/type/' + 'Client' + '/' + index + '/' + limit
        }
        await axios.get(url)
            .then(res => {
                setClientUsers(res.data.users.map((item) => {
                    return {
                        name: item.name ? item.name : '/',
                        lastname: item.lastname ? item.lastname : '/',
                        email: item.mail ? item.mail : '/',
                        pseudo: item.pseudo ? item.pseudo : '/',
                        sexe: item.sexe ? item.sexe : '/',
                        isvalid: item.isvalid === true ? "Compte Valide" : "Compte Non Valide",
                        action: (
                            <OptionButtons
                                onEdit={() => editModal(item)}
                                onView={() => showModal(item)}
                                onDelete={() => deleteModal(item)}
                            />
                        ),
                    };
                }))
                setStateR('Nosearch')
                setLoadingClients(false)//count
            })
            .catch(err => {
                console.log(err)
                setLoadingClients(false)
                setStateR('Nosearch')
            })

    }

    const handleChangetypeIndex = (type) => {
        if (type == 1) {
            getAdmins()
            countUsers()
            setTypeIndex(1)
            setColumns([

                {
                    title: I18n.t('admin.nom'),
                    width: 100,
                    dataIndex: 'name',
                    key: 'name',
                    //fixed: 'left'
                },
                { title: I18n.t('admin.prenom'), dataIndex: 'lastname', key: 'lastname', width: 100 },
                { title: I18n.t('admin.mail'), dataIndex: 'email', key: 'email', width: 100 },
                { title: I18n.t('admin.pseudo'), dataIndex: 'pseudo', key: 'pseudo', width: 100 },
                { title: I18n.t('admin.sexe'), dataIndex: 'sexe', key: 'sexe', width: 100 },
                { title: I18n.t('admin.state'), dataIndex: 'isvalid', key: 'isvalid', width: 50 },
                {
                    title: I18n.t('action.action'),
                    key: 'operation',
                    dataIndex: 'action',
                    //fixed: 'right',
                    width: 100,
                    //render: () => <OptionButtons />
                }
            ])

        }
        else if (type == 2) {
            getClientUsers()
            countClients()
            setTypeIndex(2)
            setColumns([

                {
                    title: I18n.t('admin.nom'),
                    width: 100,
                    dataIndex: 'name',
                    key: 'name',
                    //fixed: 'left'
                },
                { title: I18n.t('admin.prenom'), dataIndex: 'lastname', key: 'lastname', width: 100 },
                { title: I18n.t('admin.mail'), dataIndex: 'email', key: 'email', width: 100 },
                { title: I18n.t('admin.pseudo'), dataIndex: 'pseudo', key: 'pseudo', width: 100 },
                { title: I18n.t('admin.sexe'), dataIndex: 'sexe', key: 'sexe', width: 100 },
                { title: I18n.t('admin.state'), dataIndex: 'isvalid', key: 'isvalid', width: 50 },
                {
                    title: I18n.t('action.action'),
                    key: 'operation',
                    dataIndex: 'action',
                    //fixed: 'right',
                    width: 100,
                    //render: () => <OptionButtons />
                }
            ])

        }
        else if (type == 3) {
            getAdmins()
            countUsers()
            setTypeIndex(3)
            setColumnsPer([
                {
                    title: I18n.t('admin.auth'),
                    width: 100,
                    dataIndex: 'name',
                    key: 'name',
                    //fixed: 'left'
                },
                { title: I18n.t('perm.permission'), dataIndex: 'per', key: 'per', width: 100 },
                {
                    title: I18n.t('action.action'),
                    key: 'operation',
                    dataIndex: 'action',
                    //fixed: 'right',
                    width: 150,
                    //render: () => <OptionButtons />
                }
            ])

        }
    };
    const optionsPermission = [
        { value: 'users', label: I18n.t('perm.auteur') },
        { value: 'article', label: I18n.t('perm.article') },
        { value: 'journal', label: I18n.t('perm.journal') },
        { value: 'topClub', label: I18n.t('perm.top_club') },
        { value: 'topPersonality', label: I18n.t('perm.top_perso') },
        { value: 'topSelection', label: I18n.t('perm.top_selection') },
        { value: 'topTournoi', label: I18n.t('perm.top_tournoi') },
        { value: 'profil', label: I18n.t('perm.profil') },
        { value: 'ads_manager', label: I18n.t('perm.ads') }
    ]

    const dataPermission = []
    userPer.length ? userPer.map((item, i) => {
        dataPermission.push({
            key: i,
            name: item.name + " " + item.lastname,
            per: <Form.Item name={'permissions'} >
                <Select
                    dropdownStyle={{ textAlign: 'left' }}
                    mode='multiple'
                    id={"permissions" + item._id}
                    name={"permissions" + item._id}
                    onChange={handleChangePer}
                    textAlign={langPage == 'ar' ? 'right' : 'left'}
                    defaultValue={permissionsAll.length ? permissionsAll.filter(x => x.ID == item._id)[0] ? permissionsAll.filter(x => x.ID == item._id)[0].Permission : [] : []}
                >
                    {optionsPermission.length ? optionsPermission.map(item => {
                        return <Option value={item.value}>{item.label}</Option>
                    }) : null}
                </Select>
            </Form.Item>,
            action: <Button className="btn-sm" color={permissionsAll.length > 0 ? "success" : "primary"} onClick={() => addPermission(item._id)}  >
                {permissionsAll.filter(x => x.ID == item._id)[0] ? permissionsAll.filter(x => x.ID == item._id)[0].Permission ? <FaPencilAlt /> : <FaPlus /> : <FaPlus />} </Button>
        })
    }) : null

    const addPermission = (userID) => {

        let roles = {
            roles: {
                user: userID,
                permissions: permissions
            }
        }
        axios.put(API_LINK + 'v1/roles', roles, {
            headers: {
                'content-type': 'application/json'
            }
        })
            .then(response => {
                getPermissionsByUser(userID)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('perm.successper'),
                        showConfirmButton: true,
                    })
                )
            })
            .catch(err => {
                console.log(err)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('perm.errorsetper'),
                        showConfirmButton: true,
                    })
                )
            })
    }

    const getPermissionsByUser = (userID) => {
        axios.get(API_LINK + 'v1/roles/' + userID)
            .then(response => {
                setPermissions(response.data)
            })
            .catch(err => console.log(err))
    }



    function handleChangePer(value) {
        if (value) {
            setPermissions(value)
        } else {
            setPermissions([])
        }
    }
    const handleChangeImage = (e) => {
        if (e) {
            if (e.target.name === 'image') {
                setPicture(e.target.files[0])
            }
        }
    }

    const searchUsersPress = (e) => {
        if (e) {
            setLoadingUsers(true)
            setUsers([])
            if (e.target.value !== '') {

                setSearch(e.target.value)
            }
            else {
                setStateR('Nosearch')
                setSearch("")
            }
            getAdmins(1, pageSize)
            router.replace({
                pathname: '/users',
                query: { page: 1 }
            },
                undefined, { shallow: true }
            )
        }

    }
    const searchClientPress = (e) => {
        if (e) {
            setLoadingClients(true)
            setClientUsers([])
            if (e.target.value !== '') {

                setSearch(e.target.value)
            }
            else {
                setStateR('Nosearch')
                setSearch("")
            }
            getClientUsers(1, pageSize)
            router.replace({
                pathname: '/users',
                query: { page: 1 }
            },
                undefined, { shallow: true }
            )
        }

    }
    /****************fin affichage */
    const ChangePageSize = (e) => {
        if (e) {
            if (e.target.value === '') {
                setPageSize(20)
                getAdmins(1, 20)
                router.replace({
                    pathname: '/users',
                    query: { page: 1 }
                },
                    undefined, { shallow: true }
                )
            }
            else {
                setPageSize(e.target.value)
                getAdmins(1, e.target.value)
                router.replace({
                    pathname: '/users',
                    query: { page: 1 }
                },
                    undefined, { shallow: true }
                )

            }
        }

    }
    const ChangePageSizeClient = (e) => {
        if (e) {
            if (e.target.value === '') {
                setPageSize(20)
                getClientUsers(1, 20)
                router.replace({
                    pathname: '/users',
                    query: { page: 1 }
                },
                    undefined, { shallow: true }
                )
            }
            else {
                setPageSize(e.target.value)
                getClientUsers(1, e.target.value)
                router.replace({
                    pathname: '/users',
                    query: { page: 1 }
                },
                    undefined, { shallow: true }
                )

            }
        }

    }
    /*************** render models   *********/
    function showModal(item) {
        setOpenModalView(true)
        setUserID(item._id)
        setName(item.name)
        setType(item.type)
        setLastname(item.lastname)
        setMail(item.mail)
        setAdress(item.adress)
        setNameAR(item.nameAR)
        setLastnameAR(item.lastnameAR)
        setAdressAR(item.adressAR)
        setPseudoAR(item.pseudoAR)
        setDate(item.date)
        setPseudo(item.pseudo)
        setSexe(item.sexe)
        setPhone(item.phone)
        setPicture(item.picture)
    };
    function editModal(item) {
        setOpenModalEdit(true)
        setUserID(item._id)
        setType(item.type)
        setName(item.name)
        setNameAR(item.nameAR)
        setLastname(item.lastname)
        setLastnameAR(item.lastnameAR)
        setMail(item.mail)
        setAdressAR(item.adressAR)
        setDate(item.date)
        setPseudoAR(item.pseudoAR)
        setSexe(item.sexe)
        setPhone(item.phone)
        setPhoneCode(item.phone ? item.phone.split('-')[0] : null)
        setPhoneNumber(item.phone ? item.phone.split('-')[1] : null)
        setPicture(item.picture)
    };
    function deleteModal(item) {
        setIsActive(item.isvalid)
        setOpenModalDelete(true)
        setUserID(item._id)
    };
    function resetState() {
        setUserID("")
        setName("")
        setLastname("")
        setMail("")
        setAdress("")
        setDate("")
        setPseudo("")
        setSexe("")
        setPhone("")
        setPicture("")
        setType("")
        setType2("")
    };

    const renderAddModal = () => {
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="md"
                        open={OpenModalAdd}
                        onClose={() => setOpenModalAdd(!OpenModalAdd)}

                    >

                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={12} align={langPage === 'ar' ? "left" : "right"} className={classes.padding}>
                                        <IconButton
                                            edge="start"
                                            align={langPage === 'ar' ? "left" : "right"}
                                            color="inherit"
                                            aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalAdd(!OpenModalAdd)}
                                        //  className={classes.padding}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={classes.mainHeader}>
                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding}>
                                        <Typography className={classes.primaryColor} variant="h5">
                                            {I18n.t('admin.add')}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid

                                    container
                                    direction={langPage === 'ar' ? 'rtl' : 'ltr'}
                                    className={classes.mainContent}
                                    spacing={4}
                                >

                                    <>

                                        <Grid item md={12} sm={12} xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                label={I18n.t('admin.type')}
                                                fullWidth select required variant="outlined"
                                                id="lng"
                                                name="lng"
                                                value={type2}
                                                margin="dense"
                                                onChange={
                                                    (e) => setType2(e.target.value)}
                                                onBlur={
                                                    () => validator.showMessageFor('type2')} >
                                                <span style={
                                                    { 'color': 'red' }} > {
                                                        validator.message('type2', type2, 'required', {
                                                            messages: {
                                                                'required': I18n.t('admin.required'),
                                                            }
                                                        })
                                                    } </span>
                                                <MenuItem key="admin"
                                                    value="admin"
                                                    style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('admin.Admin')} </MenuItem>
                                                <MenuItem key="auteur"
                                                    value="auteur"
                                                    style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('admin.Auteur')} </MenuItem>


                                            </TextField> </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.nom')}
                                                id="name"
                                                name="name"
                                                className={classes.textFont}
                                                onChange={(e) => setName(e.target.value)}
                                                required
                                                onBlur={() => validator.showMessageFor('name')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('name', name, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('admin.required'),
                                                    }
                                                })}
                                            </span>
                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.prenom')}
                                                id="lastname"
                                                name="lastname"
                                                className={classes.textFont}
                                                onChange={(e) => setLastname(e.target.value)}
                                                required
                                                onBlur={() => validator.showMessageFor('lastname')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('lastname', lastname, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('admin.required'),
                                                    }
                                                })}
                                            </span>
                                        </Grid>
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.mail')}
                                                id="mail"
                                                name="mail"
                                                className={classes.textFont}
                                                onChange={(e) => setMail(e.target.value)}
                                                required
                                                onBlur={() => validator.showMessageFor('mail')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('mail', mail, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('admin.required'),
                                                    }
                                                })}
                                            </span>

                                        </Grid>




                                        <Grid item container alignContent="flex-end" >
                                            <Grid item md={3} sm={6} align={langPage === 'ar' ? "right" : "left"} alignContent="flex-end">
                                                <Button onClick={() => setOpenModalAdd(!OpenModalAdd)}>{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item md={3} sm={6} alignContent="flex-end">
                                                <Button
                                                    disabled={!(validator.fieldValid('mail'))}
                                                    onClick={addAuthor}>{I18n.t('action.ajouter')}</Button>
                                            </Grid>
                                        </Grid>

                                    </>


                                </Grid>

                            </Grid>

                        </Grid>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )

    }
    const renderAddModalClient = () => {
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="md"
                        open={OpenModalAddClient}
                        onClose={() => setOpenModalAddClient(!OpenModalAddClient)}

                    >
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={12} align={langPage == 'ar' ? 'left' : "right"} className={classes.padding}>
                                        <IconButton
                                            edge="start"
                                            align={langPage == 'ar' ? 'left' : "right"}
                                            color="inherit"
                                            aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalAddClient(!OpenModalAddClient)}
                                        //  className={classes.padding}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={classes.mainHeader}>
                                    <Grid item xs={12} align={langPage == 'ar' ? 'right' : "left"} className={classes.padding}>
                                        <Typography className={classes.primaryColor} variant="h5">
                                            {I18n.t('admin.addClient')}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid

                                    container
                                    direction={langPage == 'ar' ? 'rtl' : 'ltr'}
                                    className={classes.mainContent}
                                    spacing={4}
                                >
                                    <Grid>
                                    </Grid>
                                    <>
                                        <Grid item xs={12} align={langPage == 'ar' ? 'right' : "left"}>
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.mail')}
                                                id="mail"
                                                name="mail"
                                                className={classes.textFont}
                                                onChange={(e) => setMail(e.target.value)}
                                                required
                                                onBlur={() => validator.showMessageFor('mail')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('mail', mail, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('admin.required'),
                                                    }
                                                })}
                                            </span>

                                        </Grid>




                                        <Grid item container alignContent="flex-end" >
                                            <Grid item md={3} sm={6} align={langPage == 'ar' ? 'right' : "left"} alignContent="flex-end">
                                                <Button onClick={() => setOpenModalAddClient(!OpenModalAddClient)}>{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item md={3} sm={6} alignContent="flex-end">
                                                <Button
                                                    disabled={!(validator.fieldValid('mail'))}
                                                    onClick={addClient}>{I18n.t('action.ajouter')}</Button>
                                            </Grid>
                                        </Grid>

                                    </>


                                </Grid>

                            </Grid>

                        </Grid>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )

    }
    const addAuthor = () => {
console.log("lastname author", lastname)
        const action = {
            user: props.user.UserID,
            date: moment()
        }
        let userObject = {
            mail: mail,
            password: 'maracanaStandard',
            type: "simple",
            lien: Domaine +"Complete-Compte",
            type2:type2,
            name: name,
            lastname:lastname,
            action: JSON.stringify(action)
        }

        axios.post(API_LINK + 'v1/user', userObject).then(res => {
            setOpenModalAdd(false)
            getAdmins()
            return (Swal.fire({
                position: 'center',
                icon: 'success',
                title: I18n.t('admin.valid'),
                showConfirmButton: true,

            })
            )
        })
            .catch((err) => {
                setOpenModalAdd(false)
                console.log("err", err.response)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('admin.erroradd'),
                        showConfirmButton: true,
                        confirmButtonText: "OK"
                    })
                )

            });
    }
    const addClient = () => {

        const action = {
            user: props.user.UserID,
            date: moment()
        }
        let userObject = {
            mail: mail,
            password: 'maracanaStandard',
            type: "Client",
            lien:Domaine + "/Complete-Compte",
            action: JSON.stringify(action)
        }

        axios.post(API_LINK + 'v1/user', userObject).then(res => {
            setOpenModalAddClient(false)
            getClientUsers()
            return (Swal.fire({
                position: 'center',
                icon: 'success',
                title: I18n.t('admin.valid'),
                showConfirmButton: true,

            })
            )
        })
            .catch((err) => {
                setOpenModalAdd(false)
                console.log("err", err.response)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('admin.erroradd'),
                        showConfirmButton: true,
                    })
                )

            });
    }
    const updateUser = (usertype) => {
        const action = {
            user: props.user.UserID,
            date: moment()
        }

        let user_form = new FormData()
        user_form.append('name', name)
        user_form.append('lastname', lastname)
        user_form.append('mail', mail)
        user_form.append('nameAR', nameAR)
        user_form.append('lastnameAR', lastnameAR)
        user_form.append('password', "maracanaStandard")
        user_form.append('image', picture)
        user_form.append('sexe', sexe)
        user_form.append('pseudo', pseudo)
        user_form.append('pseudoAR', pseudoAR)
        user_form.append('date', date)
        user_form.append('adress', adress)
        user_form.append('adressAR', adressAR)
        user_form.append('type', usertype)
        user_form.append('phone', codePhone + "-" + numberPhone)
        user_form.append('action', JSON.stringify(action))

        axios.put(API_LINK + 'v1/editeuser/' + userID + '/', user_form,
            {
                headers: { 'Content-Type': 'multipart/form-data' }
            }).then(res => {
                setOpenModalEdit(false)
                if (usertype === 'simple') { getAdmins() }
                else if (usertype === "Client") {
                    getClientUsers()
                }
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('admin.successset'),
                        showConfirmButton: true,

                    })
                )

            })
            .catch((err) => {
                setOpenModalEdit(false)
                console.log("err", err)
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: I18n.t('admin.errorset'),
                    showConfirmButton: true

                })
            });
    }
    async function deleteAuthor(id) {
        await axios
            .put(API_LINK + "v1/deleteAccount", { id: id })
            .then((res) => {
                setOpenModalDelete(false)
                getAdmins()
                return (
                    Swal.fire({

                        title: I18n.t('admin.successdes'),
                        position: 'center',
                        icon: 'success',
                        showConfirmButton: true,
                        confirmButtonText: "OK"
                    }))

            })
            .catch((err) => {
                setOpenModalDelete(false);
                console.log('"auteur', err)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('admin.errordes'),
                        showConfirmButton: true,
                        confirmButtonText: "OK"

                    })
                )

            }
            )

    };

    //*****add modal */

    const changeCivilite = (e) => {
        setSexe(e.target.value)
    };


    const renderEditModal = () => {

        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>

                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="md"
                        open={OpenModalEdit}
                        onClose={() => setOpenModalEdit(!OpenModalEdit)}

                    >
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={12} align={langPage === 'ar' ? "left" : "right"} className={classes.padding}>
                                        <IconButton
                                            edge="start"
                                            align={langPage === 'ar' ? "left" : "right"}
                                            color="inherit"
                                            aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalEdit(!OpenModalEdit)}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={classes.mainHeader}>
                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding}>
                                        <Typography className={classes.primaryColor} variant="h5 text-center">
                                            {I18n.t('admin.set')}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid

                                    container
                                    dir={langPage === 'ar' ? "rtl" : 'ltr'}
                                    className={classes.mainContent}
                                    spacing={4}
                                >
                                    <Grid>
                                    </Grid>

                                    <>

                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                label={I18n.t('admin.sexe')}
                                                fullWidth
                                                select
                                                variant="outlined"
                                                id="sexe"
                                                name="sexe"
                                                value={sexe}
                                                margin="dense"
                                                onChange={changeCivilite}
                                            >

                                                <MenuItem key={'Madame'} value={'Madame'} style={{ direction: langPage === 'ar' ? "rtl" : "ltr" }}>
                                                    {I18n.t('admin.male')}
                                                </MenuItem>
                                                <MenuItem key={'Monsieur'} value={'Monsieur'} style={{ direction: langPage === 'ar' ? "rtl" : "ltr" }}>
                                                    {I18n.t('admin.female')}
                                                </MenuItem>

                                            </TextField>
                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                fullWidth
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.nom')}
                                                id="name"
                                                name="name"
                                                value={name}
                                                className={classes.textFont}
                                                onChange={(e) => setName(e.target.value)}
                                                required
                                                onBlur={() => validator.showMessageFor('name')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('name', name, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('admin.required'),
                                                    }
                                                })}
                                            </span>

                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                fullWidth
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.nomar')}
                                                id="nameAR"
                                                name="nameAR"
                                                value={nameAR}
                                                className={classes.textFont}
                                                onChange={(e) => setNameAR(e.target.value)}
                                                required
                                            // onBlur={() => validator.showMessageFor('name')}
                                            />


                                        </Grid>

                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                fullWidth
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.prenom')}
                                                id="lastname"
                                                name="lastname"
                                                value={lastname}
                                                onChange={(e) => setLastname(e.target.value)}
                                                className={classes.textFont}
                                                required
                                                onBlur={() => validator.showMessageFor('lastname')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('lastname', lastname, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('admin.required'),
                                                    }
                                                })}
                                            </span>
                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                fullWidth
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.prenomar')}
                                                id="lastnameAR"
                                                name="lastnameAR"
                                                value={lastnameAR}
                                                onChange={(e) => setLastnameAR(e.target.value)}
                                                className={classes.textFont}
                                                required
                                            //onBlur={() => validator.showMessageFor('lastname')}
                                            />

                                        </Grid>

                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                fullWidth
                                                type="email"
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.mail')}
                                                id="mail"
                                                name="mail"
                                                value={mail}
                                                onChange={(e) => setMail(e.target.value)}
                                                className={classes.textFont}
                                                required
                                                onBlur={() => validator.showMessageFor('mail')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('mail', mail, 'required|email',
                                                {
                                                    messages: {
                                                        'required': I18n.t('admin.required'),
                                                        'email': "Émail doit étre valide"
                                                    }
                                                })}
                                            </span>
                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.pseudo')}
                                                id="pseudo"
                                                name="pseudo"
                                                value={pseudo}
                                                onChange={(e) => setPseudo(e.target.value)}
                                                className={classes.textFont}
                                            />

                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.pseudoar')}
                                                id="pseudoAR"
                                                name="pseudoAR"
                                                value={pseudoAR}
                                                onChange={(e) => setPseudoAR(e.target.value)}
                                                className={classes.textFont}
                                            />

                                        </Grid>

                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <MuiPickersUtilsProvider className={"text-left"} utils={DateFnsUtils} locale={langPage === "ar" ? ar : fr}  >

                                                <DateP
                                                    inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                    placeholder={props.i18n.locale === "ar" ? 'aaaa/mm/jj ' : 'jj/mm/aaaa'}
                                                    okLabel={I18n.t('admin.confirmer')}
                                                    clearLabel={I18n.t('admin.effacer')}
                                                    cancelLabel={I18n.t('action.annuler')}
                                                    value={date ? moment(date, moment.defaultFormat).toDate() : moment.now()}
                                                    label={I18n.t('admin.date_naissance')}
                                                    fullWidth
                                                    id="date"
                                                    name="date"
                                                    format={"dd-MM-yyyy"}
                                                    ampm={false}
                                                    onChange={(e) => { setDate(moment(moment(e)).format("YYYY-MM-DD")) }}
                                                    inputStyle={{ textAlign: langPage === 'ar' ? "right" : "left" }}
                                                />
                                            </MuiPickersUtilsProvider>

                                        </Grid>

                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.adress')}
                                                id="adress"
                                                name="adress"
                                                value={adress}
                                                onChange={(e) => setAdress(e.target.value)}
                                                className={classes.textFont}
                                            />

                                        </Grid>
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.adressar')}
                                                id="adressAR"
                                                name="adressAR"
                                                value={adressAR}
                                                onChange={(e) => setAdressAR(e.target.value)}
                                                className={classes.textFont}
                                            />

                                        </Grid>


                                        <Grid item md={2} sm={4} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <div dir='ltr'>
                                                <TextField
                                                    inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                    label={I18n.t('admin.code')}
                                                    fullWidth
                                                    select
                                                    variant="outlined"
                                                    id="codePhone"
                                                    name="codePhone"
                                                    value={codePhone}
                                                    margin="dense"
                                                    onChange={(e) => { setPhoneCode(e.target.value) }}
                                                >
                                                    {CODE_PHONE.map(option => (
                                                        <MenuItem key={option} value={option} style={{ direction: langPage === 'ar' ? "rtl" : "ltr" }}>
                                                            {option}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </Grid>
                                        <Grid item md={4} sm={4} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('admin.mobile')}
                                                id="numberPhone"
                                                name={'numberPhone'}
                                                value={numberPhone}
                                                onChange={(e) => setPhoneNumber(e.target.value)}
                                                className={classes.textFont}
                                            />
                                        </Grid>




                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"}>

                                            <input
                                                accept="image/*"
                                                type="file"
                                                id="select-image"
                                                name="image"
                                                style={{ display: 'none' }}
                                                onChange={handleChangeImage}
                                            />
                                            <label htmlFor="select-image">
                                                <Button variant="contained" component="span">
                                                    {I18n.t('admin.importer')}
                                                </Button>
                                            </label>
                                            {picture && (
                                                <Box mt={2} textAlign="center">
                                                    {/* <div>{I18n.t('dash.articles.image')}</div> */}
                                                    <img src={API_LINK + picture} alt={picture.name} height="100px" />
                                                </Box>
                                            )}



                                        </Grid>


                                        <Grid item container alignContent="flex-end" >
                                            <Grid item md={3} sm={6} alignContent="flex-end">
                                                <Button onClick={() => setOpenModalEdit(!OpenModalEdit)}>{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item md={3} sm={6} alignContent="flex-end">
                                                <Button
                                                    disabled={!(validator.fieldValid('lastname') && validator.fieldValid('mail') && validator.fieldValid('name'))}
                                                    onClick={() => updateUser(type)}>{I18n.t('action.modifier')}</Button>
                                            </Grid>
                                        </Grid>

                                    </>


                                </Grid>

                            </Grid>

                        </Grid>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )

    }
    function renderDeleteModel() {
        return (

            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="sm"
                        open={OpenModalDelete}
                        onClose={() => setOpenModalDelete(!OpenModalDelete)}

                    >
                        <DialogContent className={classes.padding}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <Grid container>
                                        <Grid item xs={12} align={langPage === 'ar' ? "left" : "right"} className={classes.padding}>
                                            <IconButton
                                                edge="start"
                                                align={langPage === 'ar' ? "left" : "right"}
                                                color="inherit"
                                                aria-label="Close"
                                                style={{ padding: 8 }}
                                                onClick={() => setOpenModalDelete(!OpenModalDelete)}
                                            //  className={classes.padding}
                                            >
                                                <CloseIcon />
                                            </IconButton>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row" className={classes.mainHeader}>
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className='text-center text-red' >
                                            <Typography style={{ 'color': 'red' }} variant="h5">
                                                {I18n.t('admin.desactiver')}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid

                                        container
                                        dir={langPage === 'ar' ? "rtl" : 'ltr'}
                                        className={classes.mainContent}
                                        spacing={4}
                                    >

                                        <Grid item container alignContent="flex-end" >
                                            <Grid item xs={3} align={langPage === 'ar' ? "right" : "left"} alignContent="flex-end" className='text-center'>
                                                <Button onClick={(e) => setOpenModalDelete(!OpenModalDelete)} >{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item xs={3} alignContent="flex-end" className='text-center'>
                                                <Button onClick={() => deleteAuthor(userID)} >{isActive === true ? I18n.t('admin.desactive') : I18n.t('admin.activer')}</Button>
                                            </Grid>


                                        </Grid>

                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )
    }



    const renderViewModal = () => {

        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="md"
                        open={OpenModalView}
                        onClose={() => setOpenModalView(!OpenModalView)}

                    >
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={12} align={langPage === "ar" ? "left" : "right"} className={classes.padding}>
                                        <IconButton
                                            edge="start"
                                            align={langPage === 'ar' ? 'left' : "right"}
                                            color="inherit"
                                            aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalView(!OpenModalView)}
                                            className={classes.padding}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={classes.mainHeader}>
                                    <Grid item xs={12} align={langPage === "ar" ? "right" : "left"} className={classes.padding}>
                                        <Typography className={classes.primaryColor} variant="h5">
                                            {I18n.t('admin.info')}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid

                                    container
                                    dir={langPage === "ar" ? "rtl" : "ltr"}
                                    className={classes.mainContent}
                                    spacing={4}
                                >

                                    <Grid item md={4} sm={4} xs={4} ></Grid>
                                    <Grid item md={4} sm={4} xs={4}>
                                        <div className="text-center"> <img
                                            src={picture ? API_LINK + picture : null}
                                            ariaReadonly="true"
                                        /></div>

                                    </Grid>
                                    <Grid item md={4} sm={4} xs={4} ></Grid>
                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            fullWidth
                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.sexe')}
                                            value={sexe ? sexe === 'Homme' ? I18n.t('admin.male') : I18n.t('admin.female') : '/'}
                                            className={classes.textFont}
                                            ariaReadonly="true"
                                        />

                                    </Grid>
                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.nom')}
                                            value={name ? name : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true"
                                        />

                                    </Grid>
                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.nomar')}
                                            value={nameAR ? nameAR : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true"
                                        />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.prenom')}
                                            value={lastname ? lastname : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true"
                                        />

                                    </Grid>
                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.prenomar')}
                                            value={lastnameAR ? lastnameAR : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true"
                                        />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.pseudo')}
                                            value={pseudo ? pseudo : '/'}
                                            className={classes.textFont}

                                        />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.pseudoar')}
                                            value={pseudoAR ? pseudoAR : '/'}
                                            className={classes.textFont}

                                        />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.date_naissance')}
                                            value={date ? langPage === "ar" ? moment(date).format("YYYY-MM-DD") : moment(date).format("DD-MM-YYYY") : '/'}
                                            className={classes.textFont}

                                        />

                                    </Grid>
                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.mail')}
                                            value={mail ? mail : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true"
                                        />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.adress')}
                                            value={adress ? adress : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true"
                                        />

                                    </Grid>
                                    <Grid item md={6} sm={6} xs={12} align={langPage === "ar" ? "right" : "left"}>
                                        <TextField
                                            fullWidth
                                            inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('admin.adressar')}
                                            value={adressAR ? adressAR : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true"
                                        />

                                    </Grid>


                                    <Grid item md={6} sm={6} xs={12} direction={langPage === "ar" ? "rtl" : "ltr"} align={langPage === "ar" ? "right" : "left"}>
                                        <div dir='ltr'>
                                            <TextField
                                                inputProps={
                                                    { style: { textAlign: langPage === "ar" ? "right" : "left" } }}
                                                fullWidth
                                                margin="dense"
                                                variant="standard"
                                                label={I18n.t('admin.mobile')}
                                                value={phone ? phone : '/'}
                                                className={classes.textFont}
                                                aria-readonly="true"
                                            />
                                        </div>
                                    </Grid>


                                </Grid>
                            </Grid>
                        </Grid>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )
    }
    if (
        !verifyPermission(
            props.user.UserID,
            useRouter().pathname,
            props.user.UserType ? props.user.UserType : "simple",
            props.user.UserIsValid ? props.user.UserIsValid : ""
        )
    ) {
        return (
            <>
                <React.Fragment>
                    <VuroxLayout>

                        <ContentLayout width="100%" className="p-3 vurox-scroll-y">
                            <Row align={props.i18n.locale === 'ar' ? "right" : "left"}>
                                <Col md="4"></Col>
                                <Col md="4">
                                    <b
                                        className="text-center text-danger"
                                        style={{ marginTop: "210px", fontSize: "55px" }}
                                    >
                                        {" "}
                                        {props.i18n.locale == "ar" ? "ACCESS DENIED" : "ACCESS DENIED"}
                                    </b>
                                </Col>
                                <Col md="4"></Col>
                            </Row>
                        </ContentLayout>
                    </VuroxLayout>
                </React.Fragment>
            </>
        );
    }
    return (
        <React.Fragment>
            <div dir={langPage === 'ar' ? 'rtl' : 'ltr'}>
                <HeaderLayout className="sticky-top">
                    <HeaderDark />
                </HeaderLayout>
                <VuroxLayout>
                    <VuroxSidebar width={240} className={`sidebar-container  ${toggleClass}`} >
                        <Sidebar className={toggleClass} />
                    </VuroxSidebar>
                    <ContentLayout width='100%' className='p-3 vurox-scroll-y'>

                        <Row className="vurox-admin-content-top-section" align={props.i18n.locale === 'ar' ? "right" : "left"}>
                            <Col md={3} align={langPage === 'ar' ? "right" : "left"}>
                                <Title level={2}>{I18n.t('admin.users')}</Title>
                            </Col>

                            <Col md={2} xs={4}></Col>
                            <Col md={8} align={langPage === 'ar' ? "right" : "left"}>
                                <div className='vurox-search-from inline-flex mb-1'>
                                    <label>{I18n.t('action.show_entries')}:</label>&nbsp;
                                    <input style={{ width: "60px", border: "1px solid #ccc", borderRadius: "10px", padding: "0 0px 0 4px" }} type="text" onChange={typeIndex == 1 ? ChangePageSize : typeIndex == 2 ? ChangePageSizeClient : null} />


                                </div>

                            </Col>
                            
                            <Col md={2} xs={4}></Col>
                            <Col md={8} align={langPage === 'ar' ? "right" : "left"}>
                                <div className='vurox-search-from mb-1'>
                                    <input type="text" placeholder={I18n.t('action.recherche') + " ..."} onChange={typeIndex === 1 ? searchUsersPress : typeIndex === 2 ? searchClientPress : null} />
                                    <SearchOutlined />

                                </div>
                            </Col>
                            
                            <Col md={2} xs={4}></Col>
                            <Col md={5} align={langPage === 'ar' ? "right" : "left"}>

                                <div className={props.i18n.locale === 'ar' ? "float-md-left mb-2" : "float-md-right mb-2"}>
                                    <button
                                        onClick={() => {
                                            if (typeIndex == 1) {
                                                { setOpenModalAdd(true), resetState() }
                                            } else if (typeIndex == 2) {
                                                { setOpenModalAddClient(true), resetState() }

                                            }

                                        }}
                                        type="button"
                                        className="dash_button float-none float-sm-right mr-2 btn white bg-magenta-5 btn-md rounded hover-color my-3 my-sm-0 d-block"
                                        style={{
                                            background: "#00C1D8",
                                            color: "white",
                                            fontSize: "16px",
                                        }}
                                    >
                                        {I18n.t('action.ajouter')}
                                    </button>
                                </div>


                            </Col>
                        </Row>


                        <Row dir={langPage === 'ar' ? "rtl" : "ltr"}>
                            <Col md={3} className={`${langPage == "ar" ? 'text-right' : ''}`}>
                                {props.user.UserType === 'principal' ?
                                    <div className="vurox-mail-links vurox-admin-grey-bg rounded d-sm-block">
                                        <ul>
                                            <li>
                                                <p
                                                    style={{
                                                        color: typeIndex == 1 ? "#00C1D8" : "#000000",
                                                    }}
                                                    onClick={() => {
                                                        handleChangetypeIndex(1);
                                                    }}
                                                >
                                                    {I18n.t('admin.listeAdmin')}

                                                </p>
                                            </li>
                                            <li>
                                                <p
                                                    style={{
                                                        color: typeIndex == 2 ? "#00C1D8" : "#000000",
                                                    }}
                                                    onClick={() => {
                                                        handleChangetypeIndex(2);
                                                    }}
                                                >
                                                    {I18n.t('admin.listUsers')}
                                                </p>
                                            </li>
                                            <li>
                                                <p
                                                    style={{
                                                        color: typeIndex == 3 ? "#00C1D8" : "#000000",
                                                    }}
                                                    onClick={() => {
                                                        handleChangetypeIndex(3);
                                                    }}
                                                >
                                                    {I18n.t('perm.permission')}
                                                </p>
                                            </li>

                                        </ul>
                                    </div>
                                    :
                                    <div className="vurox-mail-links vurox-admin-grey-bg rounded d-sm-block">
                                        <ul>

                                            <li>
                                                <p
                                                    style={{
                                                        color: typeIndex == 2 ? "#00C1D8" : "#000000",
                                                    }}
                                                    onClick={() => {
                                                        handleChangetypeIndex(2);
                                                    }}
                                                >
                                                    {I18n.t('admin.listUsers')}
                                                </p>
                                            </li>


                                        </ul>
                                    </div>
                                }

                            </Col>

                            <Col md={21} align={langPage === 'ar' ? "right" : "left"}>
                                {
                                    loadingUsers ?

                                        <Spinner loading={loadingUsers} />
                                        :
                                        <>
                                            <ConfigProvider direction={langPage === 'ar' ? "rtl" : 'ltr'}>
                                                <Table columns={typeIndex === 1 ? columns : typeIndex === 2 ? columns : typeIndex === 3 ? columnsPer : columns}
                                                    dataSource={typeIndex === 1 ? users : typeIndex === 2 ? clientUsers : typeIndex === 3 ? dataPermission : []}
                                                    scroll={{ x: 1500, y: 3000 }} pagination={false} locale={{ emptyText: I18n.t("admin.noAdmin") }} />
                                                <div className={`${langPage == "ar" ? "alignLeft float-left mt-2" : "alignRight float-right mt-2"}`}>
                                                    <Pagination langue={langPage} perPage={pageSize} total={typeIndex === 1 ? totalCount : typeIndex === 2 ? totalCountClient : totalCount} />
                                                </div>
                                            </ConfigProvider>


                                        </>

                                }
                            </Col>

                        </Row>

                        {OpenModalAdd ? renderAddModal() : null}
                        {OpenModalEdit ? renderEditModal() : null}
                        {OpenModalView ? renderViewModal() : null}
                        {OpenModalDelete ? renderDeleteModel() : null}

                        {OpenModalAddClient ? renderAddModalClient() : null}




                    </ContentLayout>

                </VuroxLayout>
            </div>
        </React.Fragment>

    )
}

export default connect(state => state)(withStyles(styles)(Users))
