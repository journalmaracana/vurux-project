import React, { useState, useContext, Fragment, useEffect, useRef } from 'react'
import { Divider, Typography, Space, Comment, Tooltip, Avatar, Input, Form, List, Row, Col, Radio } from 'antd'
const { Title, Paragraph } = Typography
const { TextArea } = Input
import { Table, ConfigProvider } from 'antd'
import { FormGroup } from 'reactstrap'
import moment from 'moment'
import Chip from "@material-ui/core/Chip";
import Paper from '@mui/material/Paper'
import HeaderDark from 'Templates/HeaderDark';
import Sidebar from 'Templates/HeaderSidebar';
import { verifyPermission } from "../utils/functions";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import Spinner from '../Components/spinner'
import {
    VuroxLayout,
    HeaderLayout,
    VuroxSidebar,
    ContentLayout,
    VuroxComponentsContainer
} from 'Components/layout'
import { Pagination } from '../Components/pagination'
import { vuroxContext } from '../context'
import Box from '@material-ui/core/Box';
import axios from 'axios'
import { API_LINK } from '../utils/constantes'
import Button from '@material-ui/core/Button';
import { teal, grey } from "@material-ui/core/colors";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { FaEye, FaTrash, FaPencilAlt, FaUserTimes, FaCommentAlt } from 'react-icons/fa';
import Dialog from "@material-ui/core/Dialog";
import { CardMedia, MenuItem, ButtonGroup, withStyles } from '@material-ui/core';
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import TextField from "@material-ui/core/TextField";
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import Swal from 'sweetalert2';
import { Markup } from "interweave";
import SimpleReactValidator from "simple-react-validator";
import TagsInput from './TagsInput'
import dynamic from "next/dynamic";
import DateFnsUtils from '@date-io/date-fns';

import {
    MuiPickersUtilsProvider,
    DatePicker as DateP,
} from "material-ui-pickers";
import { fr } from "date-fns/locale";
import ar from "date-fns/locale/ar-DZ";
import { useRouter } from 'next/router'

import { SearchOutlined } from '@ant-design/icons';
import { I18n } from 'react-redux-i18n'
import { connect } from 'react-redux'
import { createMuiTheme } from "@material-ui/core/styles";
import { StylesProvider, ThemeProvider, jssPreset } from "@material-ui/styles";
import { create } from "jss";
import rtl from "jss-rtl";

let majid = []
const SunEditor = dynamic(() =>
    import("suneditor-react"), { //besure to import dynamically
    ssr: false,
});
const styles = theme => ({
    root: {
        flexGrow: 5,
        fontSize: "1.5rem"
    },
    primaryColor: {
        color: "rgb(0, 193, 216)",
        fontSize: "27px"
    },
    secondaryColor: {
        color: grey[700]
    },

    padding: {
        padding: 0,

    },
    textFont: {
        fontSize: "1.5rem"
    },
    mainHeader: {
        backgroundColor: grey[100],
        padding: 20,
        alignItems: "center"
    },
    mainContent: {
        padding: 40,

    },
    secondaryContainer: {
        padding: "20px 25px",
        backgroundColor: grey[200]
    }
});
const article = (props) => {
    const menu = [I18n.t("article.articles_red"), I18n.t("article.articles_publier"), I18n.t("article.articles_prog"), I18n.t("article.articles_correc"), I18n.t("article.articles_sup")];
    const [selectedIndex, setSelectedIndex] = React.useState(1);
    console.log('user redux', props)
    const langPage = props.i18n.locale
    const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
    const theme = createMuiTheme({
        direction: langPage === 'fr' ? "ltr" : "rtl"
    });

    const router = useRouter();
    const query = useRouter().query;
    const [open2, setOpen] = React.useState(false);
    const validator = new SimpleReactValidator({ autoForceUpdate: this });
    const anchorRef = React.useRef(null);
    const { classes, open, onClose } = props;
    const [pageOfItems, setPageOfItems] = useState([])
    const [articleID, setArticleID] = useState("")
    const [articles, setArticles] = useState([])
    const [tournois, setTournois] = useState([])
    const [tournoisSingle, setTournoisSingle] = useState([])
    const [sports, setSport] = useState([])
    const [clubsSingle, setClubsSingle] = useState([])
    const [clubs, setClubs] = useState([])
    const [currentPage, setCurrentPage] = useState('1');
    const [totalCount, setTotalCount] = useState(0);
    const [pageSize, setPageSize] = useState('15');
    const [commentsOfArticle, setCommentsOfArticle] = useState([]);
    const [categories, setCategories] = useState([]);
    const [keyWord, setKeyWord] = useState([]);
    const [tags, Settags] = useState([]);
    const [category, setCategory] = useState("");
    const [checked, setChecked] = useState(I18n.t("article.accepter"));
    const [state, setState] = useState("");
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [publishedHour, setPublishedHour] = useState("");
    const [description, setDescription] = useState("");
    const [authorName, setAuthorName] = useState({});
    const [author, setAuthor] = useState("");
    const [editor, setEditor] = useState("");
    const [image, setImage] = useState("");
    const [lng, setLang] = useState(langPage === 'ar' ? "AR" : "FR");
    const [typearticle, setTypearticle] = useState("Football");
    const [typeequipe, setTypeEquipe] = useState("");
    const [entitled, setEntitled] = useState("");
    const [entitledAR, setEntitledAR] = useState("");
    const [catID, setCatID] = useState("");
    const [categoriesdata, setcategoriesdata] = useState([]);
    const [typeModalAdd, setTypeModalAdd] = useState(1)
    const [typeModalSet, setTypeModalSet] = useState(2)

    const [editImage, SetEditImage] = useState(false);

    const [numero, setNumero] = useState("")
    const [header, setHeader] = useState("")
    const [stateR, setStateR] = useState('Nosearch')
    const [search, setSearch] = useState('')
    const [isLoading, setIsLoading] = useState(true);

    const [OpenModalView, setOpenModalView] = useState(false);
    const [OpenModalAdd, setOpenModalAdd] = useState(false);
    const [OpenModalDelete, setOpenModalDelete] = useState(false);
    const [OpenModalEdit, setOpenModalEdit] = useState(false);
    const [OpenModalComment, setOpenModalComment] = useState(false);
    const [OpenModalCat, setOpenModalCat] = useState(false);
    const [OpenModalViewCat, setOpenModalViewCat] = useState(false);
    const [OpenModalDeleteCat, setOpenModalDeleteCat] = useState(false);
    const [OpenModalEditCat, setOpenModalEditCat] = useState(false);
    const [moderateModal, setModerateModal] = useState(false);
    const [commentID, setCommentID] = useState("");
    const [typeIndex, setTypeIndex] = useState(1)
    const stateOptions = [I18n.t("article.accepter"), I18n.t("article.moderer"), I18n.t("article.nonAut")];
    const [dataAuth, setAuthors] = useState([]);
    const { menuState } = useContext(vuroxContext)
    const toggleClass = menuState ? 'menu-closed' : 'menu-open'

    const handleClick = (type) => {
        console.info(`You clicked ${menu[selectedIndex]}`);
        console.log('type', type);
        setState(menu[selectedIndex])
        setTimeout(function () {
            if (type === 2) {
                updateArticle(menu[selectedIndex])
            } else if (type === 1) {
                addArticle(menu[selectedIndex])
            }
        }, 10000);


    };

    const handleMenuItemClick = (event, index) => {
        console.log('indesindes', index)
        // index programmer
        setSelectedIndex(index);
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    const [columnsCat, setColumnsCat] = useState([
        {
            title: I18n.t('category.categorie_FR'), dataIndex: "entitled", width: 150, key: "1",
            //fixed: 'left' 
        },
        { title: I18n.t('category.categorie_AR'), width: 100, dataIndex: "entitledAR", key: "2" },
        {
            title: I18n.t('action.action'),
            dataIndex: "action",
            key: "operation",
            //fixed: "right",
            width: 50,
        }
    ])
    const [columns, setColumns] = useState([{

        title: I18n.t('article.titre'),
        width: 400,
        dataIndex: 'title',
        key: 'title',
        //fixed: 'left'
    },
    { title: I18n.t('article.author'), dataIndex: 'author', key: '1', width: 150 },
    { title: I18n.t('article.etat'), dataIndex: 'state', key: '2', width: 150 },
    { title: I18n.t('article.date_pub'), dataIndex: 'publishedDate', key: '3', width: 150 },
    { title: I18n.t('article.date_Maj'), dataIndex: 'updateDate', key: '4', width: 150 },
    { title: I18n.t('article.categorie'), dataIndex: 'category', key: '5', width: 150 },
    { title: I18n.t('article.add'), dataIndex: 'addBy', key: '5', width: 150 },
    { title: I18n.t('article.update'), dataIndex: 'editBy', key: '5', width: 150 },
    {
        title: I18n.t('article.liste_comment'),
        dataIndex: 'commentaire',
        key: 'operation'
        //, fixed: 'right'
        ,
        width: 150
    },
    {
        title: I18n.t('action.action'),
        key: 'operation',
        dataIndex: 'action',
        //fixed: 'right',
        width: 150,
        //render: () => <OptionButtons />
    }
    ]);
    const OptionButtons = (props) => {
        return (<Fragment>
            <Button color="success"
                outline className="btn-sm mx-1"
                onClick={
                    () => props.onView()} > <FaEye /> </Button>
            <Button color="success"
                outline className="btn-sm mx-1"
                onClick={
                    () => props.onEdit()} > < FaPencilAlt /> </Button> <Button color="danger"
                        outline className="btn-sm mx-1"
                        onClick={
                            () => props.onDelete()} > < FaTrash /> </Button>

        </Fragment>
        );
    }
    const OptionButtons2 = (props) => {
        return (<Fragment>
            {/* <Button color="success"
                outline className="btn-sm mx-1"
                onClick={
                    () => props.onView()} > <FaEye /> </Button> */}
            <Button color="success"
                outline className="btn-sm mx-1"
                onClick={
                    () => props.onEdit()} > < FaPencilAlt /> </Button>
            <Button color="danger"
                outline className="btn-sm mx-1"
                onClick={
                    () => props.onDelete()} > < FaTrash /> </Button>

        </Fragment>
        );
    }

    useEffect(() => {
        const pageQuery = router.query.page ? router.query.page : 1
        console.log("routerrr", pageQuery)
        setCurrentPage(pageQuery)
        getArticles(pageQuery, pageSize)
        getTournois('Football');
        getClubs();
        getCategory();
        getUser()
        getSports();
        getAuthors()
    }, [router.isReady, router.query, search]);

    useEffect(() => {
        console.log('useEffect total', totalCount)
    }, [totalCount])

    useEffect(() => {
        setColumnsCat([
            {
                title: I18n.t('category.categorie_FR'), dataIndex: "entitled", width: 150, key: "1",
                //fixed: 'left' 
            },
            { title: I18n.t('category.categorie_AR'), width: 100, dataIndex: "entitledAR", key: "2" },
            {
                title: I18n.t('action.action'),
                dataIndex: "action",
                key: "operation",
                //fixed: "right",
                width: 50,
            }
        ]),
            setColumns([{

                title: I18n.t('article.titre'),
                width: 400,
                dataIndex: 'title',
                key: 'title',
                //fixed: 'left'
            },
            { title: I18n.t('article.author'), dataIndex: 'author', key: '1', width: 150 },
            { title: I18n.t('article.etat'), dataIndex: 'state', key: '2', width: 150 },
            { title: I18n.t('article.date_pub'), dataIndex: 'publishedDate', key: '3', width: 150 },
            { title: I18n.t('article.date_Maj'), dataIndex: 'updateDate', key: '4', width: 150 },
            { title: I18n.t('article.add'), dataIndex: 'addBy', key: '5', width: 150 },
            { title: I18n.t('article.update'), dataIndex: 'editBy', key: '5', width: 150 },
            {
                title: I18n.t('article.liste_comment'),
                dataIndex: 'commentaire',
                key: 'operation'
                //, fixed: 'right'
                ,
                width: 150
            },
            {
                title: I18n.t('action.action'),
                key: 'operation',
                dataIndex: 'action',
                //fixed: 'right',
                width: 150,
                //render: () => <OptionButtons />
            }
            ])
    }, [langPage])


    function getArticles(index, limit) {
        let linkGetAll = ""
        let lang = ""
        langPage === 'ar' ? lang = 'AR' : lang = 'FR'
        console.log('hhh', lang)
        if (search) {
            linkGetAll = 'v1/articlesPaginationFindlanguage/' + search + '/' + lang + '/' + index + '/' + limit

        }
        else {
            linkGetAll = "v1/articlespagination/language/" + lang + '/' + index + '/' + limit
        }


        axios
            .get(API_LINK + linkGetAll)
            .then((response) => {
                console.log("articlesDash", response.data);
                setArticles(response.data.articles)
                setStateR('Nosearch')
                setTotalCount(response.data.count)
                setIsLoading(false)
            })
            .catch((err) => {
                setIsLoading(false)
                console.log(err);
            })
    };
    function getAuthors() {
        axios.get(API_LINK + 'v1/user/type/simple').then(res => {
            console.log('reeeee', res.data)
            setAuthors(res.data.map(item => {
                return { name: item.name + " " + item.lastname, value: item._id };
            }))
        }).catch(err => {
            console.log('err', err.response)
        })
    }

    const searchArticlePress = (e) => {
        console.log("seaarch", e.target.value)
        if (e) {
            setIsLoading(true)
            setArticles([])
            if (e.target.value !== '') {

                setSearch(e.target.value)
            }
            else {
                setStateR('Nosearch')
                setSearch("")
            }
            getArticles(1, pageSize)
            router.replace({
                pathname: '/article',
                query: { page: 1 }
            },
                undefined, { shallow: true }
            )
        }

    }
    /****************fin affichage */
    const ChangePageSize = (e) => {
        if (e) {
            if (e.target.value === '') {
                setPageSize(50)
                getArticles(1, 50)
                router.replace({
                    pathname: '/article',
                    query: { page: 1 }
                },
                    undefined, { shallow: true }
                )
            }
            else {
                setPageSize(e.target.value)
                getArticles(1, e.target.value)
                router.replace({
                    pathname: '/article',
                    query: { page: 1 }
                },
                    undefined, { shallow: true }
                )

            }
        }

    }



    const getTournois = (sport) => {
        let URLGetTournoi = API_LINK + 'v1/tournois-sport/' + sport + '/';
        axios
            .get(URLGetTournoi)
            .then(response => {
                console.log('tournoisssss', response.data)
                setTournoisSingle(response.data.length ? response.data.map(item => {
                    return { _id: item._id, entitled: langPage === 'ar' ? item.entitledAR : item.entitled }
                }) : [])

            })
            .catch(err => console.log(err));
    };


    const getClubs = () => {
        let URLGetClub = API_LINK + 'v1/clubArticle'; // il faut ajouté dans l'api le champ en arabe
        axios
            .get(URLGetClub)
            .then(response => {
                setClubsSingle(response.data.length ? response.data.map(item => {
                    return { _id: item.id, name:langPage === 'ar' ? item.nameAR : item.abreviation }
                }) : [])
            })
            .catch(err => console.log(err));
    };

    const getCategory = () => {
        let URLGetCategory = API_LINK + 'v1/categories';
        axios
            .get(URLGetCategory)

            .then(response => {
                console.log('cattttttt', response.data)
                setcategoriesdata(response.data)
                setCategories(response.data.length ? response.data.map(item => {
                    return { _id: item._id, entitled: langPage === 'ar' ? item.entitledAR : item.entitled }
                }) : []
                )

            })
            .catch(err => console.log(err));
    };
    const getSports = () => {
        let URLGetCategory = API_LINK + 'v1/sport';
        axios
            .get(URLGetCategory)
            .then(response => {
                console.log('sporttttttt', response.data)
                setSport(response.data)

            })
            .catch(err => console.log(err));
    };
    const addArticle = (index) => {
        console.log("in add", state, index)
        let newstate = ""

        if (index == I18n.t("article.articles_correc")) { newstate = 'En attente de correction' }
        else if (index == I18n.t("article.articles_red")) { newstate = 'Enregistrer comme brouillon' }
        else if (index == I18n.t("article.articles_publier")) { newstate = 'Publier' }
        else if (index == I18n.t("article.articles_prog")) { newstate = 'Programmer' }
        else if (index == I18n.t("article.articles_sup")) { newstate = 'Supprimer' }

        let newchecked = ""
        checked === I18n.t("article.accepter") ? newchecked = 'Accepter' :
            checked === I18n.t("article.moderer") ? newchecked = 'Modérer' : checked === I18n.t("article.nonAut") ? newchecked = 'Ne pas autoriser' : newchecked

        const action = {
            user: props.user.UserID,
            date: moment()
        }

        let URLAjout = 'v1/articles';

        let form_data = new FormData();      // pour Ajout ou Edit des champs de type file

        form_data.append('image', image);
        form_data.append('typeequipe', typeequipe);
        form_data.append('typearticle', typearticle);

        form_data.append('author', author)
        form_data.append('date', moment().format());
        form_data.append('category', JSON.stringify(category));
        form_data.append('keyWord', JSON.stringify(tags));
        form_data.append('tags', JSON.stringify([]));
        form_data.append('tags2', JSON.stringify([]));

        form_data.append('tournois', JSON.stringify(tournois));
        form_data.append('clubs', JSON.stringify(clubs));
        form_data.append('state', newstate);
        form_data.append('isActive', newchecked);

        //---------
        form_data.append('title', title);
        form_data.append('content', content);
        form_data.append('resumer', description);
        form_data.append('lng', lng);
        form_data.append('action', JSON.stringify(action));
        if (state === 'Programmer') {
            form_data.append('publishedHour', publishedHour);
        }

        axios.post(API_LINK + URLAjout, form_data)

            .then(res => {
                getArticles(1, pageSize)
                setOpenModalAdd(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('article.successadd'),
                        showConfirmButton: true,
                    }))


            })
            .catch(err => {
                setOpenModalAdd(false)
                console.log('err article', err.response)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('article.erroradd'),
                        showConfirmButton: true,
                    }))
            })


    };
    const updateArticle = (index) => {
        console.log('author set', author)
        let URLModifie = 'v1/articles/';
        let newstate = ""

        if (index == I18n.t("article.articles_correc")) { newstate = 'En attente de correction' }
        else if (index == I18n.t("article.articles_red")) { newstate = 'Enregistrer comme brouillon' }
        else if (index == I18n.t("article.articles_publier")) { newstate = 'Publier' }
        else if (index == I18n.t("article.articles_prog")) { newstate = 'Programmer' }
        else if (index == I18n.t("article.articles_sup")) { newstate = 'Supprimer' }
        let newchecked = ""
        checked == I18n.t("article.accepter") ? newchecked = 'Accepter' :
            checked == I18n.t("article.moderer") ? newchecked = 'Modérer' : checked == I18n.t("article.nonAut") ? newchecked = 'Ne pas autoriser' : newchecked

        const action = {
            user: props.user.UserID,
            date: moment()
        }
        let form_data = new FormData();
        form_data.append('image', image);

        form_data.append('typeequipe', typeequipe);
        form_data.append('typearticle', typearticle);
        if (author === '') {
            form_data.append('author', 'Maracana Foot');
        } else {
            form_data.append('author', author);
        }
        form_data.append('date', moment().format());
        form_data.append('category', JSON.stringify(category));
        form_data.append('tournois', JSON.stringify(tournois));

        form_data.append('clubs', JSON.stringify(clubs));
        form_data.append('keyWord', JSON.stringify(tags));
        form_data.append('state', newstate);
        form_data.append('isActive', newchecked);

        //---------
        form_data.append('title', title);
        form_data.append('content', content);
        form_data.append('resumer', description);
        form_data.append('lng', lng);
        form_data.append('action', JSON.stringify(action));
        if (state === 'Programmer') {
            form_data.append('publishedHour', publishedHour);
        }

        axios
            .put(API_LINK + URLModifie + articleID, form_data, {
                headers: {
                    'content-type': 'multipart/form-data',
                },
            })
            .then(res => {
                getArticles(1, pageSize)
                setOpenModalEdit(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('article.successet'),
                        showConfirmButton: true,
                    }))


            })
            .catch(err => {
                setOpenModalEdit(false)
                console.log('err set', err.response)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('article.errorset'),
                        showConfirmButton: true,
                    }))
            })

    }


    const deleteArticle = () => {
        let URLDelete = 'v1/articles/';

        axios.delete(API_LINK + URLDelete + articleID)
            .then(response => {
                getArticles(1, pageSize)
                setOpenModalDelete(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('article.successdeleteart'),
                        showConfirmButton: true,
                    })
                )


            })
            .catch(err => {
                setOpenModalDelete(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('article.errordeleteart'),
                        showConfirmButton: true,
                    })
                )

            });
    }



    const data = [];
    articles ? articles.forEach((item, i) => {

        let sorted = item.actions.sort((a, b) => {
            return b.date.localeCompare(a.date);
        });


        let add = item.actions.map(action => {
            if (action.action === 'Add') {
                return action.user
                    ? langPage === 'ar' ? action.user.nameAR + ' ' + action.user.lastnameAR : action.user.name + ' ' + action.user.lastname
                    : null;
            }
        });

        let updatdBy = sorted.map(action => {
            if (action.action === 'Update') {
                return action.user
                    ? langPage === 'ar' ? action.user.nameAR + ' ' + action.user.lastnameAR : action.user.name + ' ' + action.user.lastname
                    : null;
            }
        })
        data.push({
            key: i,
            title: item.title,
            author: item.author ? item.author.name + " " + item.author.lastname : null,
            state: item.state === "Programmer" ? I18n.t('article.articles_prog') :
                item.state === "En attente de correction" ? I18n.t('article.articles_correc') :
                    item.state === "Supprimer" ? I18n.t('article.articles_sup') :
                        item.state === "Publier" ? I18n.t('article.articles_publier') :
                            item.state === "Enregistrer comme brouillon" ? I18n.t('article.articles_red') :
                                null,
            publishedDate: item.date ? langPage === 'ar' ? moment(item.date).format("YYYY-MM-DD") : moment(item.date).format("DD-MM-YYYY") : null,
            updateDate: item.dateUpdate ? langPage === 'ar' ? moment(item.dateUpdate).format("YYYY-MM-DD, HH:mm:ss") : moment(item.dateUpdate).format("DD-MM-YYYY, HH:mm:ss") :
                I18n.t('article.no_modif'),
            addBy: add ? add : "",
            editBy: updatdBy ? updatdBy[0] : "",
            commentaire: <Button color="primary"
                onClick={() => showComments(item)}
            >
                < FaCommentAlt /> </Button>,

            action: <OptionButtons onEdit={() => UpdateModal(item)}
                onView={() => showModal(item)}
                onDelete={() => deleteModal(item._id)} />,
        });
    }) : null
    const deleteModal = (id) => {
        setOpenModalDelete(true)
        setArticleID(id)
    }
    const handleChangeCat = (event, values) => {
        console.log("love", event, values);
        if (event) {
            setCategory(values);
        } else {
            setCategory([]);
        }
    };

    const getArticlesByState = async (langue, state, page, limit) => {
        setIsLoading(true)
        setArticles("")
        console.log("state in article state", API_LINK +
            'v1/articlesStatepagination/language/' +
            langue +
            '/' +
            state +
            '/' +
            page +
            '/' +
            limit)
        await axios
            .get(
                API_LINK +
                'v1/articlesStatepagination/language/' +
                langue +
                '/' +
                state +
                '/' +
                page +
                '/' +
                limit,
            )
            .then(response => {
                console.log("article state", response.data);
                setArticles(response.data.article)
                setStateR('search')
                setTotalCount(response.data.count)
                setIsLoading(false)
            })

            .catch((err) => {
                setIsLoading(false)
                console.log(err);
            })
    }
    const handleChangeState = (e) => {

        if (e) {
            setState(e.target.value)
            getArticlesByState(
                lng,
                e.target.value,
                currentPage,
                pageSize,
            );
            console.log("state in handel", e.target.value)
        } else {
            setState('')
            getArticles(
                lng,
                currentPage,
                pageSize,
            );

        }
    };
    const handleDeleteCat = (event, item) => {

        if (event) {
            const newSelectedItem = [...category];
            console.log("handleDeletecat", newSelectedItem.indexOf(item))
            newSelectedItem.splice(newSelectedItem.indexOf(item), 1);
            setCategory(newSelectedItem);
        }
    };
    const handleChangeClubs = (event, values) => {
        console.log("love", event, values);
        if (event) {
            setClubs(values);
        } else {
            setClubs([]);
        }
    };

    const handleChangeSelectAuthor = (event) => {
        console.log('handleChangeSelectAuthor ', event)
        if (event) {
            setAuthor(event.target.value)
        }
        else {
            setAuthor("")
        }
    }


    const handleDeleteClubs = (event, item) => {

        if (event) {
            const newSelectedItem = [...category];
            console.log("handleDeletecat", newSelectedItem.indexOf(item))
            newSelectedItem.splice(newSelectedItem.indexOf(item), 1);
            setClubs(newSelectedItem);
        }
    };
    const handleChangeTournois = (event, values) => {
        console.log("love", event, values);
        if (event) {
            setTournois(values);
        } else {
            setTournois([]);
        }
    };



    const handleDeleteTournois = (event, item) => {

        if (event) {
            const newSelectedItem = [...category];
            console.log("handleDeletecat", newSelectedItem.indexOf(item))
            newSelectedItem.splice(newSelectedItem.indexOf(item), 1);
            setTournois(newSelectedItem);
        }
    };

    const tagRef = useRef();

    const handleDelete = (value) => {
        const newtags = keyWord.filter((val) => val !== value);
        Settags(newtags);
    };
    const handleOnSubmit = (e) => {
        e.preventDefault();
        Settags([...keyWord, tagRef.current.value]);
        tagRef.current.value = "";
    };
    const onChangeState = (e) => {
        setChecked(e.target.value)
    };

    function handleChangeSun(content) {
        //Get Content Inside Editor

        setContent(content)

    };

    function handleDropSun(event) {
        setContent(event)
    }
    const handleChangeImage = (e) => {
        if (e) {
            if (e.target.name === 'image') {
                setImage(e.target.files[0]),
                    SetEditImage(true)
            }
        }
    }

    function handleSelecetedTags(items) {
        if (items) {
            setKeyWord(items)
            Settags(items)
            console.log('items', items);
        }
    }
    function resetState() {
        setImage("")
        setArticleID("")
        setTypearticle("Football")
        setDescription("")
        setContent("")
        setTitle("")
        setKeyWord("")
        setAuthor("")
        setState("")
        setClubs("")
        setTournois("")
        setTypeEquipe("")
        //setTypearticle("")
        setPublishedHour("")
        setChecked(I18n.t("article.accepter"))
        setCategory("")
    }
    function showModal(item) {
        console.log('VIEW', item)
        let newState = ""
        item.state === "Programmer" ? newState = I18n.t('article.articles_prog') :
            item.state === "En attente de correction" ? newState = I18n.t('article.articles_correc') :
                item.state === "Supprimer" ? newState = I18n.t('article.articles_sup') :
                    item.state === "Publier" ? newState = I18n.t('article.articles_publier') :
                        item.state === "Enregistrer comme brouillon" ? newState = I18n.t('article.articles_red') : null
        setOpenModalView(true)
        setImage(item.image)
        setArticleID(item._id)
        setTypeEquipe(item.typeequipe)
        setTypearticle(item.typearticle)
        setDescription(item.resumer)
        setContent(item.content)
        setTitle(item.title)
        setKeyWord(item.keyWord)
        setAuthor(item.author)
        setState(newState)
        setChecked(item.isActive)
        setCategory(item.category)
        setTournois(item.tournois)
        setClubs(item.clubs)
    }
    function UpdateModal(item) {
        console.log('statet', item)
        setOpenModalEdit(true)
        setLang(item.lng)
        setArticleID(item._id)
        setTypearticle(item.typearticle)
        setTypeEquipe(item.typeequipe)
        setImage(item.image)
        setDescription(item.resumer)
        setContent(item.content)
        setTitle(item.title)
        Settags(item.keyWord)
        setAuthor(item.author ? item.author._id : null)
        setState(item.state)
        setChecked(item.isActive)
        setCategory(item.category)
        setTournois(item.tournois)
        setClubs(item.clubs)
    }


    //******** part of comment  */
    function showComments(article) {
        setOpenModalComment(true)
        setCommentsOfArticle(article.comments)
        setArticleID(article._id)


    }
    const renderComments = () => {
        const cols = [{
            title: I18n.t("article.comment_description"),
            width: 100,
            dataIndex: 'description',
            key: 'description',
            //fixed: 'left'
        },
        // { title: "Utilisateur", dataIndex: 'user', key: '1', width: 150 },
        {
            title: I18n.t("article.etat_comment"), dataIndex: 'isModerate', key: '2',
            width: 50
        },
        {
            title: I18n.t("action.action"),
            key: 'operation',
            dataIndex: 'action',
            fixed: 'right',
            width: 50,
            //render: () => <OptionButtons />
        }
        ];



        const dataSource = commentsOfArticle.length ?
            commentsOfArticle.map(item => {
                return ({
                    description: item.description,
                    isModerate: item.isModerate === false ? I18n.t("action.oui") : I18n.t("action.non"),
                    //user: item.user.name+" "+ item;user.lastname,
                    action: <Button variant="outlined" color="primary" onClick={() => showModerateModal(articleID, item._id)}>{I18n.t("article.moderer")}</Button>
                })
            }) : null


        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="md"
                        open={OpenModalComment}
                        onClose={() => setOpenModalComment(!OpenModalComment)}

                    >
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={12} align={langPage === 'ar' ? 'left' : "right"} className={classes.padding}>
                                        <IconButton
                                            edge="start"
                                            align={langPage === 'ar' ? 'left' : "right"}
                                            color="inherit"
                                            aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalComment(!OpenModalComment)}
                                            className={classes.padding}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={classes.mainHeader}>
                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} className={classes.padding}>
                                        <Typography className={classes.primaryColor} variant="h5">
                                            {I18n.t("article.liste_comment")}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid

                                    container
                                    direction={langPage === 'ar' ? 'rtl' : "ltr"}
                                    className={classes.mainContent}
                                    spacing={4}
                                >
                                    <ConfigProvider direction={langPage === 'ar' ? 'rtl' : "ltr"}>
                                        <Table dataSource={dataSource} columns={cols} scroll={{ x: 1500, y: 3000 }} />
                                    </ConfigProvider>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )
    }
    const showModerateModal = (idArt, idCom) => {
        setModerateModal(true)
        setArticleID(idArt)
        setCommentID(idCom)
    }
    function moderateCommentModal() {
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="sm"
                        open={moderateModal}
                        onClose={() => setModerateModal(!moderateModal)}

                    >
                        <DialogContent className={classes.padding}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <Grid container>
                                        <Grid item xs={12} align={langPage === 'ar' ? 'left' : "right"} className={classes.padding}>
                                            <IconButton
                                                edge="start"
                                                align={langPage === 'ar' ? 'left' : "right"}
                                                color="inherit"
                                                aria-label="Close"
                                                style={{ padding: 8 }}
                                                onClick={() => setModerateModal(!moderateModal)}
                                            //  className={classes.padding}
                                            >
                                                <CloseIcon />
                                            </IconButton>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row" className={classes.mainHeader}>
                                        <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} className={classes.padding}>
                                            <Typography style={{ 'color': 'red' }} variant="h5">
                                                {I18n.t('article.modererq')}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid

                                        container
                                        direction="row"
                                        className={classes.mainContent}
                                        spacing={4}
                                    >

                                        <Grid item container alignContent="flex-end" >
                                            <Grid item xs={3} alignContent="flex-end" className='text-center'>
                                                <Button variant='outlined' onClick={(e) => setModerateModal(!moderateModal)} >{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item xs={3} alignContent="flex-end" className='text-center'>
                                                <Button variant='outlined' onClick={() => moderateComment(articleID, commentID)} >{I18n.t('article.moderer')}</Button>
                                            </Grid>


                                        </Grid>

                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )
    }

    async function moderateComment(idA, idC) {
        await axios
            .put(API_LINK + "v1/moderateComment/" + idA + '/' + idC, { isModerate: true })
            .then((res) => {
                console.log("commentaire modéré avec succes");
                setModerateModal(false)
                setCommentsOfArticle(res.data)
                getArticles(1, pageSize)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('article.successComment'),
                        showConfirmButton: true,
                    })
                )
            })
            .catch((err) => {
                console.log(
                    ` Une erreur s'est produite `,
                    err
                );
                setModerateModal(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('article.errorComment'),
                        showConfirmButton: true,

                    })
                )

            });
    };




    //***************************** */
    const renderViewModal = () => {

        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog className={classes.root} fullWidth maxWidth="md" open={OpenModalView} onClose={
                        () => setOpenModalView(!OpenModalView)} >
                        < Grid container >
                            <Grid item xs={12} >
                                < Grid container >
                                    < Grid item xs={12} align={langPage === 'ar' ? 'left' : "right"} className={classes.padding} >
                                        <IconButton edge="start"
                                            align={langPage === 'ar' ? 'left' : "right"}
                                            color="inherit" aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalView(!OpenModalView)}
                                            className={classes.padding} >
                                            <CloseIcon />
                                        </IconButton> </Grid> </Grid>

                                <Grid container direction="row" className={classes.mainHeader} >
                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} className={classes.padding} >
                                        < Typography className={classes.primaryColor}
                                            variant="h5" > {I18n.t('article.info')} </Typography> </Grid> </Grid>
                                <Grid container direction={langPage === 'ar' ? 'rtl' : "ltr"} className={classes.mainContent} spacing={4} >
                                    <Grid item xs={4} > </Grid>
                                    <Grid item xs={4} >
                                        <Box mt={2} textAlign="center" >
                                            <img src={image ? API_LINK + image : null} alt={image ? image.name : null} height="100px" />
                                        </Box></Grid >
                                    <Grid item xs={4} > </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"}>
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            fullWidth margin="dense"
                                            variant="standard"
                                            label={I18n.t('article.titre')}
                                            id="title"
                                            name="title"
                                            value={title ? title : '/'}
                                            className={classes.textFont}
                                            ariaReadonly="true" />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"}>
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            fullWidth margin="dense"
                                            variant="standard"
                                            label={I18n.t('article.description_fr')}
                                            id="description"
                                            name="description"
                                            value={description ? description : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true" />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"}>
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            fullWidth margin="dense"
                                            variant="standard"
                                            label={I18n.t('article.etat')}
                                            id="state"
                                            name="state"
                                            value={state ? state : '/'}
                                            className={classes.textFont}
                                            aria-readonly="true" />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"}>
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            fullWidth
                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('article.author')}
                                            id="author"
                                            name="author"
                                            value={author.name + " " + author.lastname}
                                            className={classes.textFont}

                                        />

                                    </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"}>


                                        <Divider variant="h6"
                                            component="div" > < small > {I18n.t('article.categorie')} </small>
                                        </Divider>
                                        <List > {
                                            category.length ? category.map(item => {
                                                return < >
                                                    <ListItem >
                                                        <ListItemText className='text-center'
                                                            primary={item.entitled}
                                                        //secondary={secondary ? 'Secondary text' : null}
                                                        />
                                                    </ListItem> </>
                                            }) : null
                                        } </List> </Grid>


                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"}>
                                        <Divider variant="h6"
                                            component="div" >
                                            <small> {I18n.t("article.keyWord")} </small>
                                        </Divider>
                                        <List > {
                                            keyWord.length ? keyWord.map(item => {
                                                return < >
                                                    <ListItem >
                                                        <ListItemText className='text-center'
                                                            primary={item}
                                                        //secondary={secondary ? 'Secondary text' : null}
                                                        /> </ListItem> </>
                                            }) : null
                                        } </List> </Grid> <Divider > </Divider>

                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"}>
                                        <small > {I18n.t("article.contenu")} </small>
                                        < Markup fullWidth className={langPage === 'ar' ? "text-right" : "text-left"}
                                            content={content ? content : I18n.t("article.no_info")}
                                        />


                                    </Grid>

                                </Grid> </Grid> </Grid> </Dialog>
                </ThemeProvider>
            </StylesProvider>

        )
    }

    const renderAddModal = () => {
        const langueDefault = langPage === 'ar' ? "AR" : "FR"
        console.log('addmodaz', lng, typearticle)
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog className={classes.root} fullWidth maxWidth="md" open={OpenModalAdd} onClose={
                        () => setOpenModalAdd(!OpenModalAdd)} >
                        < Grid container >
                            <Grid item xs={12} >
                                < Grid container >
                                    < Grid item xs={12} align={langPage === 'ar' ? 'left' : "right"}
                                        className={classes.padding} >
                                        <IconButton edge="start"
                                            align={langPage === 'ar' ? 'left' : "right"}
                                            color="inherit" aria-label="Close"
                                            style={
                                                { padding: 8 }}
                                            onClick={
                                                () => setOpenModalAdd(!OpenModalAdd)}
                                            className={classes.padding} >
                                            <CloseIcon />
                                        </IconButton> </Grid> </Grid>

                                <Grid container direction="row" className={classes.mainHeader} >
                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"}
                                        className={classes.padding} >
                                        < Typography className={classes.primaryColor}
                                            variant="h5" > {I18n.t('article.add_article')}</Typography> </Grid> </Grid>
                                <Grid container dir={langPage === 'ar' ? 'rtl' : "ltr"} className={classes.mainContent} spacing={4} >
                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            label={I18n.t('article.langue')}
                                            disabled
                                            fullWidth select required variant="outlined"
                                            id="lng"
                                            name="lng"
                                            value={langueDefault}
                                            margin="dense"
                                            onChange={
                                                (e) => setLang(e.target.value)}
                                            onBlur={
                                                () => validator.showMessageFor('lng')} >
                                            <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('lng', lng, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span>
                                            
                                            <MenuItem key={'FR'}
                                                value="FR"
                                                style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.fr')} </MenuItem>
                                            <MenuItem key={'AR'}
                                                value={'AR'}
                                                style={ langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.ar')} </MenuItem>


                                        </TextField> </Grid>

                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            label={I18n.t('article.sport')}
                                            fullWidth
                                            select
                                            required
                                            variant="outlined"
                                            id="typearticle"
                                            name="typearticle"
                                            value={typearticle}
                                            margin="dense"
                                            onChange={(e) => setTypearticle(e.target.value)}
                                            onBlur={
                                                () => validator.showMessageFor('typearticle')} >
                                            <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('typearticle', typearticle, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span>
                                            {sports.length ? sports.map(item => {
                                                return <MenuItem key={item._id} value={item.sport} style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {langPage === 'ar' ? item.sportAR : item.sport} </MenuItem>
                                            }) : null}
                                        </TextField> </Grid>

                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} >

                                        <small >{I18n.t('article.categorie') + "*"} </small>
                                        <Autocomplete
                                            multiple
                                            freeSolo
                                            id="tags-outlined"
                                            options={categories}
                                            getOptionLabel={(option) => option.entitled}
                                            getOptionValue={(option) => option._id}
                                            value={category.length > 0 ? category : []}
                                            PaperComponent={({ children }) => (
                                                <Paper style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                            )}
                                            onChange={(event, newValue) =>
                                                handleChangeCat(event, newValue)
                                            }
                                            getOptionSelected={(option, value) => {
                                                return option._id === value._id
                                            }}
                                            filterSelectedOptions
                                            renderInput={(params) => {

                                                return (
                                                    <TextField
                                                        inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                        {...params}
                                                        InputProps={{
                                                            ...params.InputProps,
                                                            startAdornment:
                                                                //langPage === 'ar'? null :
                                                                (

                                                                    category.length > 0 ? category.map((item, index) => (
                                                                        <Chip
                                                                            tabIndex={-1}
                                                                            label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.entitled}</Typography>}
                                                                            inputLabelProps={{ style: { textAlign: 'left' } }}
                                                                            //style={{height:"100%"}}
                                                                            className={classes.chip}
                                                                            onDelete={(e, v) => { console.log("valuuees", v, e), handleDeleteCat(e, item) }}
                                                                        />
                                                                    ))

                                                                        : null
                                                                ),
                                                            // endAdornment: langPage === 'ar' ?
                                                            // category.length > 0 ? category.map((item, index) => (
                                                            //     <>

                                                            //         <Chip
                                                            //             label={<Typography style={{ whiteSpace: 'normal', float: "right" }}>{item.entitled}</Typography>}
                                                            //             inputLabelProps={{ style: { textAlign: 'right' } }}
                                                            //             className={classes.chip}
                                                            //             onDelete={(e) => handleDeleteCat(e, item)}
                                                            //         />
                                                            //     </>
                                                            // )) : null : null,
                                                        }}
                                                        variant="outlined"
                                                        label={""}
                                                        margin="normal"
                                                        fullWidth
                                                        name="category"
                                                        id="category"
                                                        onBlur={() => validator.showMessageFor("category")}

                                                    />
                                                );
                                            }}
                                        />

                                        <span style={{ color: "red" }}>
                                            {" "}
                                            {validator.message("category", category, "required",
                                                {
                                                    messages: {
                                                        required: I18n.t('article.required'),
                                                    },
                                                }
                                            )}
                                        </span>  </Grid>

                                    {category.length > 0 ?
                                        category.map(cat => {
                                            if (cat.entitled === 'Equipe nationale' || cat.entitled === 'Equipe nationale ar') {
                                                return (
                                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                                        <TextField
                                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                            label={I18n.t('article.categorie_equipe')}
                                                            fullWidth
                                                            select
                                                            required
                                                            variant="outlined"
                                                            id="typeequipe"
                                                            name="typeequipe"
                                                            value={typeequipe}
                                                            margin="dense"
                                                            onChange={(e) => setTypeEquipe(e.target.value)}
                                                        //onBlur={ () => validator.showMessageFor('typeequipe')} 
                                                        >
                                                            {/* <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('typearticle', typeequipe, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span> */}
                                                            <MenuItem value="u">--</MenuItem>
                                                            <MenuItem value="EN A" selected={typeequipe === 'EN A'}>
                                                                {I18n.t('cat.ena')}
                                                            </MenuItem>
                                                            <MenuItem value="EN A`" selected={typeequipe === 'EN A`'}>
                                                                {I18n.t('cat.ena_')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN U23"
                                                                selected={typeequipe === 'EN U23'}
                                                            >
                                                                {I18n.t('cat.enu23')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN JEUNES"
                                                                selected={typeequipe === 'EN JEUNES'}
                                                            >
                                                                {I18n.t('cat.enjeunes')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN FIMININE"
                                                                selected={typeequipe === 'EN FIMININE'}
                                                            >
                                                                {I18n.t('cat.enfimin')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN MELITAIRE"
                                                                selected={typeequipe === 'EN MELITAIRE'}
                                                            >
                                                                {I18n.t('cat.enmili')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="TALENT"
                                                                selected={typeequipe === 'TALENT'}
                                                            >
                                                                {I18n.t('cat.entalent')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="LES PROFESSIONNELS"
                                                                selected={typeequipe === 'PROFESSIONNELS'}
                                                            >
                                                                {I18n.t('cat.pro')}
                                                            </MenuItem>


                                                        </TextField> </Grid>
                                                )
                                            }
                                        })
                                        : null}
                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} >

                                        <small style={{ whiteSpace: 'normal' }}>{I18n.t('article.tournoi')} </small>
                                        <Autocomplete
                                            multiple
                                            freeSolo
                                            id="tags-outlined"
                                            options={tournoisSingle}
                                            getOptionLabel={(option) => option.entitled}
                                            getOptionValue={(option) => option._id}
                                            value={tournois.length > 0 ? tournois : []}
                                            PaperComponent={({ children }) => (
                                                <Paper style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                            )}
                                            onChange={(event, newValue) =>
                                                handleChangeTournois(event, newValue)
                                            }
                                            getOptionSelected={(option, value) => {
                                                return option._id === value._id
                                            }}
                                            filterSelectedOptions
                                            renderInput={(params) => {

                                                return (
                                                    <TextField
                                                        inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                        {...params}
                                                        InputProps={{
                                                            ...params.InputProps,
                                                            startAdornment:
                                                                //langPage === 'ar' ? null :
                                                                (

                                                                    tournois.length > 0 ? tournois.map((item, index) => (
                                                                        <Chip
                                                                            tabIndex={-1}
                                                                            label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.entitled}</Typography>}
                                                                            inputLabelProps={{ style: { textAlign: 'left' } }}
                                                                            //style={{height:"100%"}}
                                                                            className={classes.chip}
                                                                            onDelete={(e, v) => { console.log("valuuees", v, e), handleDeleteTournois(e, item) }}
                                                                        />
                                                                    ))

                                                                        : null
                                                                ),
                                                            // endAdornment: langPage === 'ar' ?
                                                            // tournois.length > 0 ? tournois.map((item, index) => (
                                                            //     <>

                                                            //         <Chip
                                                            //             label={<Typography style={{ whiteSpace: 'normal', float: "right" }}>{item.entitled}</Typography>}
                                                            //             inputLabelProps={{ style: { textAlign: 'right' } }}
                                                            //             className={classes.chip}
                                                            //             onDelete={(e) => handleDeleteTournois(e, item)}
                                                            //         />
                                                            //     </>
                                                            // )) : null : null,
                                                        }}
                                                        variant="outlined"
                                                        label={""}
                                                        margin="normal"
                                                        fullWidth
                                                        name="tournois"
                                                        id="tournois"
                                                        onBlur={() => validator.showMessageFor("tournois")}

                                                    />
                                                );
                                            }}
                                        />
                                        <span style={{ color: "red" }}>
                                            {" "}
                                            {validator.message(
                                                "tournois",
                                                tournois,
                                                "required",
                                                {
                                                    messages: {
                                                        required: I18n.t('article.required'),
                                                    },
                                                }
                                            )}
                                        </span>
                                    </Grid>

                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} >

                                        <small style={{ whiteSpace: 'normal' }}>{I18n.t('article.teams')} </small>
                                        <Autocomplete
                                            multiple
                                            freeSolo
                                            id="tags-outlined"
                                            options={clubsSingle}
                                            getOptionLabel={(option) => option.name}
                                            getOptionValue={(option) => option._id}
                                            value={clubs.length > 0 ? clubs : []}
                                            PaperComponent={({ children }) => (
                                                <Paper style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                            )}
                                            onChange={(event, newValue) =>
                                                handleChangeClubs(event, newValue)
                                            }
                                            getOptionSelected={(option, value) => {
                                                return option._id === value._id
                                            }}
                                            filterSelectedOptions
                                            renderInput={(params) => {

                                                return (
                                                    <TextField
                                                        inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                        {...params}
                                                        InputProps={{
                                                            ...params.InputProps,
                                                            startAdornment:
                                                                //langPage === 'ar' ? null : 
                                                                (

                                                                    clubs.length > 0 ? clubs.map((item, index) => (
                                                                        <Chip
                                                                            tabIndex={-1}
                                                                            label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.name}</Typography>}
                                                                            inputLabelProps={{ style: { textAlign: 'left' } }}
                                                                            //style={{height:"100%"}}
                                                                            className={classes.chip}
                                                                            onDelete={(e, v) => { console.log("valuuees", v, e), handleDeleteClubs(e, item) }}
                                                                        />
                                                                    ))

                                                                        : null
                                                                ),
                                                            // endAdornment: langPage === 'ar' ?
                                                            // clubs.length > 0 ? clubs.map((item, index) => (
                                                            //     <>

                                                            //         <Chip
                                                            //             label={<Typography style={{ whiteSpace: 'normal', float: "right" }}>{item.name}</Typography>}
                                                            //             inputLabelProps={{ style: { textAlign: 'right' } }}
                                                            //             className={classes.chip}
                                                            //             onDelete={(e) => handleDeleteClubs(e, item)}
                                                            //         />
                                                            //     </>
                                                            // )) : null : null,
                                                        }}
                                                        variant="outlined"
                                                        label={""}
                                                        margin="normal"
                                                        fullWidth
                                                        name="clubs"
                                                        id="clubs"
                                                        onBlur={() => validator.showMessageFor("clubs")}

                                                    />
                                                );
                                            }}
                                        />
                                        <span style={{ color: "red" }}>
                                            {" "}
                                            {validator.message(
                                                "clubs",
                                                clubs,
                                                "required",
                                                {
                                                    messages: {
                                                        required: I18n.t('article.required'),
                                                    },
                                                }
                                            )}
                                        </span>
                                    </Grid>

                                    {/* <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"} >
                                        <TextField inputProps={langPage === 'ar' ?  { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            label={I18n.t('article.etat')}
                                            fullWidth select required variant="outlined"
                                            id="state"
                                            name="state"
                                            value={state}
                                            margin="dense"
                                            onChange={
                                                (e) => setState(e.target.value)}
                                            onBlur={
                                                () => validator.showMessageFor('state')} >
                                            <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('state', state, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span>
                                            <MenuItem key={'Publier'}
                                                value="Publier"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_publier')} </MenuItem>
                                            <MenuItem key={'Programmer'}
                                                value="Programmer"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} >{I18n.t('article.articles_prog')}</MenuItem>
                                            <MenuItem key={'Supprimer'}
                                                value="Supprimer"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_sup')} </MenuItem>
                                            <MenuItem key={'Enregistrer comme brouillon'}
                                                value="Enregistrer comme brouillon"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_red')} </MenuItem>
                                            <MenuItem key={'En attente de correction'}
                                                value={'En attente de correction'}
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_correc')} </MenuItem>


                                        </TextField> </Grid>

                                    {
                                        (state === 'Programmer') ?
                                            <>
                                                <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"} >


                                                    < MuiPickersUtilsProvider className={"text-left"} utils={DateFnsUtils} locale={langPage === "ar" ? ar : fr}>
                                                        <DateP inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                            okLabel={I18n.t('admin.confirmer')}
                                                            clearLabel={I18n.t('article.effacer')}
                                                            cancelLabel={I18n.t('action.annuler')}
                                                            //value = { publishedHour ? moment(publishedHour, moment.defaultFormat).toDate() : moment.now() }
                                                            label={I18n.t('article.date_pub')}
                                                            id="publishedHour"
                                                            name="publishedHour"
                                                            format={props.i18n.locale === "ar" ? "yyyy-MM-dd HH:mm" :"dd-MM-yyyy HH:mm"}
                                                            placeholder={props.i18n.locale === "ar" ? 'aaaa/mm/jj ' : 'jj/mm/aaaa'}
                                                            ampm={false}
                                                            onChange={(e) => { setPublishedHour(e) }}
                                                            required
                                                            inputStyle={{ textAlign: 'left' }}
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </Grid> </> :
                                            null
                                    } */}


                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} className={classes.padding} >
                                        <TextField inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            multiline fullWidth margin="dense"

                                            variant="standard"
                                            label={I18n.t('article.titre') + "*"}
                                            onChange={(e) => setTitle(e.target.value)}
                                            id="title"
                                            name="title"
                                            value={title}
                                            className={classes.textFont}
                                            onBlur={
                                                () => validator.showMessageFor('title')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('title', title, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span>
                                    </Grid>
                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} className={classes.padding} >
                                        <TextField inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            multiline fullWidth margin="dense"
                                            variant="outlined"
                                            label={I18n.t('article.description_fr')}
                                            id="description"
                                            name="description"
                                            required onChange={
                                                (e) => setDescription(e.target.value)}
                                            onBlur={
                                                () => validator.showMessageFor('description')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('description', description, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                        // 'min':I18n.t('regex.min_desc')
                                                    }
                                                })
                                            } </span>
                                    </Grid>
                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} className={classes.padding} >

                                        <small> {I18n.t('article.contenu') + "*"} </small>
                                        <SunEditor setOptions={
                                            {
                                                buttonList: [
                                                    ["undo", "redo"],
                                                    ["font", "fontSize", "formatBlock"],
                                                    ["paragraphStyle", "blockquote"],
                                                    [
                                                        "bold",
                                                        "underline",
                                                        "italic",
                                                        "strike",
                                                        "subscript",
                                                        "superscript",
                                                    ],
                                                    ["fontColor", "hiliteColor", "textStyle"],
                                                    ["removeFormat"],
                                                    "/", // Line break
                                                    ["outdent", "indent"],
                                                    ["align", "horizontalRule", "list", "lineHeight"],
                                                    [
                                                        "table",
                                                        "link",
                                                        "image",
                                                        "video",
                                                        "audio" /** ,'math' */,
                                                    ], // You must add the 'katex' library at options to use the 'math' plugin. // You must add the "imageGalleryUrl".
                                                    /** ['imageGallery'] */
                                                    [
                                                        "fullScreen",
                                                        "showBlocks",
                                                        "codeView",
                                                    ],
                                                    ["preview", "print"],
                                                    ["save", "template"],
                                                ],
                                            }
                                        }
                                            onChange={handleChangeSun}
                                            onDrop={handleDropSun}
                                            setContents={content}
                                            height="300px"
                                            onBlur={
                                                () => validator.showMessageFor('content')} />

                                        <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('content', content, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span>

                                    </Grid>
                                    < Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"}
                                        className={classes.padding} >

                                        <input className='text-center'
                                            variant='outlined'
                                            accept="image/*"
                                            type="file"
                                            id="select-image"
                                            name={'image'}
                                            label={I18n.t('article.image') + "*"}
                                            style={
                                                { display: 'none' }}
                                            onChange={handleChangeImage}
                                            onBlur={
                                                () => validator.showMessageFor('image')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('image', image, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span> <label htmlFor="select-image"
                                                className='text-center' >
                                            <Button variant="contained"
                                                component="span" > {I18n.t('article.importer')} {"*"}</Button> </label> {
                                            image && (
                                                <span>{image.name}</span>
                                                // <Box mt={2}
                                                //     xs={
                                                //         { flexDirection: 'row' }} > { /* <div>{('articles.image')}</div> */}
                                                //     <img src={null}
                                                //         alt={image.name}
                                                //         height="100px" />
                                                // </Box>

                                            )
                                        }


                                    </Grid>
                                    {props.user.UserType === "simple" ?
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                namez='author'
                                                label={I18n.t("admin.auth")}
                                                value={authorName.name + " " + authorName.lastname}
                                                className={classes.textFont}
                                            //onBlur={() => validator.showMessageFor('entitledAR')}
                                            /></Grid>
                                        :
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                label={I18n.t("admin.auth")}
                                                fullWidth
                                                select
                                                variant="outlined"
                                                name="author"
                                                id="author"
                                                value={dataAuth ? dataAuth.filter(({ value }) => value === author)[0] : null}
                                                margin="dense"
                                                onChange={handleChangeSelectAuthor}
                                            >
                                                {console.log("categorie press", dataAuth)}
                                                {dataAuth
                                                    ? dataAuth.map((item) => {
                                                        console.log("categorie map", item.label);
                                                        return (
                                                            <MenuItem key={item.value} value={item.value} style={langPage == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }}>
                                                                {item.name}
                                                            </MenuItem>
                                                        );
                                                    })
                                                    : null}
                                            </TextField>
                                            <span style={{ 'color': 'red' }}>{validator.message('author', author, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t("article.required"),
                                                    }
                                                })}
                                            </span>
                                        </Grid>
                                    }

                                    <Divider > < small > {I18n.t('article.etat_comment')} </small></Divider >
                                    <Grid item xs={12}
                                        align={langPage === 'ar' ? 'right' : "left"}
                                        className={classes.padding} >
                                        <Radio.Group options={stateOptions}
                                            onChange={onChangeState}
                                            value={checked}
                                        /> </Grid>
                                    <Divider > </Divider >
                                    <Grid item xs={12}
                                        className={classes.padding} dir={langPage === 'ar' ? "rtl" : "ltr"}
                                    >
                                        <TagsInput inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            required
                                            selectedTags={handleSelecetedTags}
                                            fullWidth variant="outlined"
                                            id="tags"
                                            name="tags"
                                            placeholder={I18n.t('article.keyWord')}
                                            label={I18n.t('article.keyWord')}
                                            onBlur={
                                                () => validator.showMessageFor('tags')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('tags', tags, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span> </Grid>
                                </Grid>
                                <Grid item container alignContent="flex-start" className={classes.secondaryContainer}>

                                    <Divider><small></small></Divider>
                                    <Grid item container alignContent="flex-end" >
                                        <Grid item md={3} sm={6} xs={6} alignContent="flex-end">
                                            <Button variant='outlined' onClick={() => setOpenModalAdd(!OpenModalAdd)}>{I18n.t('action.annuler')}</Button>
                                        </Grid>
                                        <Grid item md={3} sm={3} xs={6} alignContent="flex-end">
                                            {/* <Button variant='outlined'
                      //disabled={!(validator.fieldValid('entitled') || validator.fieldValid('entitledAR'))} 
                      onClick={addArticle}>{I18n.t('articles.ajouter')}</Button> */}
                                            {/* <Dropdown overlay={menu}>
                       <Button>
                       {I18n.t('articles.ajouter')} <DownOutlined />
                        </Button>
                        </Dropdown> */}
                                            <ButtonGroup variant="contained" ref={anchorRef}>
                                                <Button onClick={() => handleClick(typeModalAdd)}
                                                    disabled={!(validator.fieldValid('title') && validator.fieldValid('image') && validator.fieldValid('content') && validator.fieldValid('category')
                                                        && validator.fieldValid('description') && validator.fieldValid('lng') && validator.fieldValid('author') && validator.fieldValid('tags'))}
                                                >
                                                    {menu[selectedIndex]}</Button>
                                                <Button
                                                    size="small"
                                                    aria-controls={open2 ? 'split-button-menu' : undefined}
                                                    aria-expanded={open2 ? 'true' : undefined}
                                                    aria-label="select merge strategy"
                                                    aria-haspopup="menu"
                                                    onClick={handleToggle}
                                                >
                                                    <ArrowDropDownIcon />
                                                </Button>
                                            </ButtonGroup>
                                            <Popper
                                                open={open2}
                                                anchorEl={anchorRef.current}
                                                role={undefined}
                                                transition
                                                disablePortal
                                            >
                                                {({ TransitionProps, placement }) => (
                                                    <Grow
                                                        {...TransitionProps}
                                                        style={{
                                                            transformOrigin:
                                                                placement === 'bottom' ? 'center top' : 'center bottom',
                                                        }}
                                                    >
                                                        <Paper>
                                                            <ClickAwayListener onClickAway={handleClose}>
                                                                <MenuList id="split-button-menu">
                                                                    {menu.map((option, index) => (
                                                                        <MenuItem
                                                                            key={option}
                                                                            selected={index === selectedIndex}
                                                                            onClick={(event) => handleMenuItemClick(event, index)}
                                                                        >
                                                                            {option}
                                                                        </MenuItem>
                                                                    ))}
                                                                </MenuList>
                                                            </ClickAwayListener>
                                                        </Paper>
                                                    </Grow>
                                                )}
                                            </Popper>

                                        </Grid>
                                        {(selectedIndex === 2) ?

                                            <Grid item md={6} sm={6} xs={12} >
                                                < MuiPickersUtilsProvider className={"text-left"} utils={DateFnsUtils} locale={langPage === "ar" ? ar : fr}>
                                                    <DateP inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                        okLabel={I18n.t('admin.confirmer')}
                                                        clearLabel={I18n.t('article.effacer')}
                                                        cancelLabel={I18n.t('action.annuler')}
                                                        //value = { publishedHour ? moment(publishedHour, moment.defaultFormat).toDate() : moment.now() }
                                                        label={I18n.t('article.date_pub')}
                                                        id="publishedHour"
                                                        name="publishedHour"
                                                        format={props.i18n.locale === "ar" ? "yyyy-MM-dd HH:mm" : "dd-MM-yyyy HH:mm"}
                                                        placeholder={props.i18n.locale === "ar" ? 'aaaa/mm/jj ' : 'jj/mm/aaaa'}
                                                        ampm={false}
                                                        onChange={(e) => { setPublishedHour(e) }}
                                                        required
                                                        inputStyle={{ textAlign: 'left' }}
                                                    />
                                                </MuiPickersUtilsProvider>


                                            </Grid>
                                            : null
                                        }
                                    </Grid>

                                </Grid>
                            </Grid>

                        </Grid>  </Dialog>
                </ThemeProvider>
            </StylesProvider>

        )
    }
    const renderUpdateModal = () => {
        let keyWordd = tags.length ? tags : []

        if (keyWordd.length) {
            majid = keyWordd
        }

        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog className={classes.root} fullWidth maxWidth="md" open={OpenModalEdit} onClose={
                        () => setOpenModalEdit(!OpenModalEdit)} >
                        < Grid container >
                            <Grid item xs={12}>
                                < Grid container >
                                    < Grid item xs={12}
                                        align={langPage === 'ar' ? 'left' : "right"}
                                        className={classes.padding} >
                                        <IconButton edge="start"
                                            align={langPage === 'ar' ? 'left' : "right"}
                                            color="inherit" aria-label="Close" style={{ padding: 8 }}
                                            onClick={
                                                () => setOpenModalEdit(!OpenModalEdit)}
                                            className={classes.padding} >
                                            <CloseIcon />
                                        </IconButton> </Grid> </Grid>

                                <Grid container direction="row" className={classes.mainHeader} >
                                    <Grid item xs={12}
                                        align={langPage === 'ar' ? 'right' : "left"}
                                        className={classes.padding} >
                                        < Typography className={classes.primaryColor} variant="h5" > {I18n.t('article.set_article')} </Typography>
                                    </Grid>
                                </Grid>
                                <Grid container dir={langPage === 'ar' ? 'rtl' : "ltr"} className={classes.mainContent} spacing={4} >
                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                           disabled
                                            label={I18n.t('article.langue')}
                                            fullWidth select required variant="outlined"
                                            id="lng"
                                            name="lng"
                                            value={lng}
                                            margin="dense"
                                            onChange={(e) => setLang(e.target.value)}
                                            onBlur={() => validator.showMessageFor('lng')} >
                                            <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('lng', lng, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span>
                                            <MenuItem key={'FR'}
                                                value="FR"
                                                style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.fr')} </MenuItem>
                                            <MenuItem key={'AR'}
                                                value={'AR'}
                                                style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.ar')}</MenuItem>


                                        </TextField> </Grid>
                                    <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                        <TextField
                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            label={I18n.t('article.sport')}
                                            fullWidth
                                            select
                                            required
                                            variant="outlined"
                                            id="typearticle"
                                            name="typearticle"
                                            value={typearticle}
                                            margin="dense"
                                            onChange={(e) => setTypearticle(e.target.value)}
                                            onBlur={
                                                () => validator.showMessageFor('typearticle')} >
                                            <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('typearticle', typearticle, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span>
                                            {sports.length ? sports.map(item => {
                                                return <MenuItem key={item._id} value={item.sport} style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {item.sport} </MenuItem>
                                            }) : null}
                                        </TextField> </Grid>

                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} >
                                        <small style={{ whiteSpace: 'normal' }}>{I18n.t('article.categorie')} </small>
                                        <Autocomplete
                                            multiple
                                            freeSolo
                                            id="tags-outlined"
                                            options={categories}
                                            getOptionLabel={(option) => option.entitled}
                                            getOptionValue={(option) => option._id}
                                            value={category.length > 0 ? category : []}
                                            PaperComponent={({ children }) => (
                                                <Paper style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                            )}
                                            onChange={(event, newValue) =>
                                                handleChangeCat(event, newValue)
                                            }
                                            getOptionSelected={(option, value) => {
                                                return option._id === value._id
                                            }}
                                            filterSelectedOptions
                                            renderInput={(params) => {

                                                return (
                                                    <TextField
                                                        inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                        {...params}
                                                        InputProps={{
                                                            ...params.InputProps,
                                                            startAdornment:
                                                                // langPage === 'ar' ? null :
                                                                (

                                                                    category.length > 0 ? category.map((item, index) => (
                                                                        <Chip
                                                                            tabIndex={-1}
                                                                            label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.entitled}</Typography>}
                                                                            inputLabelProps={{ style: { textAlign: 'left' } }}
                                                                            //style={{height:"100%"}}
                                                                            className={classes.chip}
                                                                            onDelete={(e, v) => { console.log("valuuees", v, e), handleDeleteCat(e, item) }}
                                                                        />
                                                                    ))

                                                                        : null
                                                                ),
                                                            //                                             endAdornment: langPage === 'ar' ?
                                                            //                                             category.length> 0? category.map((item,index) => (
                                                            //       <>

                                                            //    <Chip
                                                            //      label={<Typography style={{whiteSpace: 'normal' , float:"right"}}>{ item.entitled}</Typography>}
                                                            //      inputLabelProps={{ style: { textAlign: 'right' }}}
                                                            //      className={classes.chip}
                                                            //      onDelete={(e)=> handleDeleteCat(e,item)}
                                                            //    />
                                                            //    </>
                                                            //  )):null:null, 
                                                        }}
                                                        variant="outlined"
                                                        label={""}
                                                        margin="normal"
                                                        fullWidth
                                                        name="category"
                                                        id="category"
                                                        onBlur={() => validator.showMessageFor("category")}

                                                    />
                                                );
                                            }}
                                        />

                                        <span style={{ color: "red" }}>
                                            {" "}
                                            {validator.message("category", category, "required",
                                                {
                                                    messages: {
                                                        required: I18n.t('article.required'),
                                                    },
                                                }
                                            )}
                                        </span>

                                    </Grid>
                                    {category.length > 0 ?
                                        category.map(cat => {
                                            if (cat.entitled === 'Equipe nationale' || cat.entitled === 'Equipe nationale ar') {
                                                return (
                                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} >
                                                        <TextField
                                                            inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                            label={I18n.t('article.categorie_equipe')}
                                                            fullWidth
                                                            select
                                                            required
                                                            variant="outlined"
                                                            id="typeequipe"
                                                            name="typeequipe"
                                                            value={typeequipe}
                                                            margin="dense"
                                                            onChange={(e) => setTypeEquipe(e.target.value)}
                                                        //onBlur={ () => validator.showMessageFor('typeequipe')} 
                                                        >
                                                            {/* <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('typearticle', typeequipe, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span> */}
                                                            <MenuItem value="u">--</MenuItem>
                                                            <MenuItem value="EN A" selected={typeequipe === 'EN A'}>
                                                                {I18n.t('cat.ena')}
                                                            </MenuItem>
                                                            <MenuItem value="EN A`" selected={typeequipe === 'EN A`'}>
                                                                {I18n.t('cat.ena_')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN U23"
                                                                selected={typeequipe === 'EN U23'}
                                                            >
                                                                {I18n.t('cat.enu23')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN JEUNES"
                                                                selected={typeequipe === 'EN JEUNES'}
                                                            >
                                                                {I18n.t('cat.enjeunes')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN FIMININE"
                                                                selected={typeequipe === 'EN FIMININE'}
                                                            >
                                                                {I18n.t('cat.enfimin')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="EN MELITAIRE"
                                                                selected={typeequipe === 'EN MELITAIRE'}
                                                            >
                                                                {I18n.t('cat.enmili')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="TALENT"
                                                                selected={typeequipe === 'TALENT'}
                                                            >
                                                                {I18n.t('cat.entalent')}
                                                            </MenuItem>
                                                            <MenuItem
                                                                value="LES PROFESSIONNELS"
                                                                selected={typeequipe === 'PROFESSIONNELS'}
                                                            >
                                                                {I18n.t('cat.pro')}
                                                            </MenuItem>


                                                        </TextField> </Grid>
                                                )
                                            }
                                        })
                                        : null}

                                    <Grid item xs={12} align={langPage === 'ar' ? 'right' : "left"} >


                                        <small style={{ whiteSpace: 'normal' }}>{I18n.t('article.tournoi')} </small>
                                        <Autocomplete
                                            multiple
                                            freeSolo
                                            id="tags-outlined"
                                            options={tournoisSingle}
                                            getOptionLabel={(option) => option.entitled}
                                            getOptionValue={(option) => option._id}
                                            value={tournois.length > 0 ? tournois : []}
                                            PaperComponent={({ children }) => (
                                                <Paper style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                            )}
                                            onChange={(event, newValue) =>
                                                handleChangeTournois(event, newValue)
                                            }
                                            getOptionSelected={(option, value) => {
                                                return option._id === value._id
                                            }}
                                            filterSelectedOptions
                                            renderInput={(params) => {

                                                return (
                                                    <TextField
                                                        inputProps={{ style: { textAlign: 'left' } }}
                                                        {...params}
                                                        InputProps={{
                                                            ...params.InputProps,
                                                            startAdornment:
                                                                //langPage === 'ar' ? null :
                                                                (

                                                                    tournois.length > 0 ? tournois.map((item, index) => (
                                                                        <Chip
                                                                            tabIndex={-1}
                                                                            label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.entitled}</Typography>}
                                                                            inputLabelProps={{ style: { textAlign: 'left' } }}
                                                                            //style={{height:"100%"}}
                                                                            className={classes.chip}
                                                                            onDelete={(e, v) => { console.log("valuuees", v, e), handleDeleteTournois(e, item) }}
                                                                        />
                                                                    ))

                                                                        : null
                                                                ),
                                                            // endAdornment: langPage === 'ar' ?
                                                            // tournois.length > 0 ? tournois.map((item, index) => (
                                                            //     <>

                                                            //         <Chip
                                                            //             label={<Typography style={{ whiteSpace: 'normal', float: "right" }}>{item.entitled}</Typography>}
                                                            //             inputLabelProps={{ style: { textAlign: 'right' } }}
                                                            //             className={classes.chip}
                                                            //             onDelete={(e) => handleDeleteTournois(e, item)}
                                                            //         />
                                                            //     </>
                                                            // )) : null : null,
                                                        }}
                                                        variant="outlined"
                                                        label={""}
                                                        margin="normal"
                                                        fullWidth
                                                        name="tournois"
                                                        id="tournois"
                                                        onBlur={() => validator.showMessageFor("tournois")}

                                                    />
                                                );
                                            }}
                                        />
                                        <span style={{ color: "red" }}>
                                            {" "}
                                            {validator.message(
                                                "tournois",
                                                tournois,
                                                "required",
                                                {
                                                    messages: {
                                                        required: I18n.t('article.required'),
                                                    },
                                                }
                                            )}
                                        </span>
                                    </Grid>
                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} >

                                        <small style={{ whiteSpace: 'normal' }}>{I18n.t('article.teams')} </small>
                                        <Autocomplete
                                            multiple
                                            freeSolo
                                            id="tags-outlined"
                                            options={clubsSingle}
                                            getOptionLabel={(option) => option.name}
                                            getOptionValue={(option) => option._id}
                                            value={clubs.length > 0 ? clubs : []}
                                            PaperComponent={({ children }) => (
                                                <Paper style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                            )}
                                            onChange={(event, newValue) =>
                                                handleChangeClubs(event, newValue)
                                            }
                                            getOptionSelected={(option, value) => {
                                                return option._id === value._id
                                            }}
                                            filterSelectedOptions
                                            renderInput={(params) => {

                                                return (
                                                    <TextField
                                                        inputProps={{ style: { textAlign: 'left' } }}
                                                        {...params}
                                                        InputProps={{
                                                            ...params.InputProps,
                                                            startAdornment:
                                                                //langPage === 'ar' ? null :
                                                                (

                                                                    clubs.length > 0 ? clubs.map((item, index) => (
                                                                        <Chip
                                                                            tabIndex={-1}
                                                                            label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.name}</Typography>}
                                                                            inputLabelProps={{ style: { textAlign: 'left' } }}
                                                                            //style={{height:"100%"}}
                                                                            className={classes.chip}
                                                                            onDelete={(e, v) => { console.log("valuuees", v, e), handleDeleteClubs(e, item) }}
                                                                        />
                                                                    ))

                                                                        : null
                                                                ),
                                                            // endAdornment: langPage === 'ar' ?
                                                            // clubs.length > 0 ? clubs.map((item, index) => (
                                                            //     <>

                                                            //         <Chip
                                                            //             label={<Typography style={{ whiteSpace: 'normal', float: "right" }}>{item.name}</Typography>}
                                                            //             inputLabelProps={{ style: { textAlign: 'right' } }}
                                                            //             className={classes.chip}
                                                            //             onDelete={(e) => handleDeleteClubs(e, item)}
                                                            //         />
                                                            //     </>
                                                            // )) : null : null,   
                                                        }}
                                                        variant="outlined"
                                                        label={""}
                                                        margin="normal"
                                                        fullWidth
                                                        name="clubs"
                                                        id="clubs"
                                                        onBlur={() => validator.showMessageFor("clubs")}

                                                    />
                                                );
                                            }}
                                        />
                                        <span style={{ color: "red" }}>
                                            {" "}
                                            {validator.message(
                                                "clubs",
                                                clubs,
                                                "required",
                                                {
                                                    messages: {
                                                        required: I18n.t('article.required'),
                                                    },
                                                }
                                            )}
                                        </span>
                                    </Grid>



                                    {/* <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"} >
                                        <TextField inputProps={langPage === 'ar' ?  { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            label={I18n.t('article.etat')}
                                            fullWidth select required variant="outlined"
                                            id="state"
                                            name="state"
                                            value={state}
                                            margin="dense"
                                            onChange={
                                                (e) => setState(e.target.value)}
                                            onBlur={
                                                () => validator.showMessageFor('state')} >
                                            <span style={
                                                { 'color': 'red' }} > {
                                                    validator.message('state', state, 'required', {
                                                        messages: {
                                                            'required': I18n.t('article.required'),
                                                        }
                                                    })
                                                } </span>
                                            <MenuItem key={'Publier'}
                                                value="Publier"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_publier')} </MenuItem>
                                            <MenuItem key={'Programmer'}
                                                value="Programmer"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} >{I18n.t('article.articles_prog')}</MenuItem>
                                            <MenuItem key={'Supprimer'}
                                                value="Supprimer"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_sup')} </MenuItem>
                                            <MenuItem key={'Enregistrer comme brouillon'}
                                                value="Enregistrer comme brouillon"
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_red')} </MenuItem>
                                            <MenuItem key={'En attente de correction'}
                                                value={'En attente de correction'}
                                                style={langPage === 'ar' ?{ direction: 'rtl' } :{ direction: 'ltr' }} > {I18n.t('article.articles_correc')} </MenuItem>


                                        </TextField> </Grid>

                                    {
                                        (state === 'Programmer') ?
                                            <>
                                                <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                                    < MuiPickersUtilsProvider className={"text-left"} utils={DateFnsUtils} locale={langPage === "ar" ? ar : fr}>
                                                        <DateP inputProps={{ style: { textAlign: "left" } }}
                                                            okLabel={I18n.t('admin.confirmer')}
                                                            clearLabel={I18n.t('article.effacer')}
                                                            cancelLabel={I18n.t('action.annuler')}
                                                            value={publishedHour ? moment(publishedHour, moment.defaultFormat).toDate() : moment.now()}
                                                            label={I18n.t('article.date_pub')}
                                                            id="publishedHour"
                                                            name="publishedHour"
                                                            format={props.i18n.locale === "ar" ? "yyyy-MM-dd HH:mm" :"dd-MM-yyyy HH:mm"}
                                                            placeholder={props.i18n.locale === "ar" ? 'aaaa/mm/jj ' : 'jj/mm/aaaa'}
                                                            ampm={false}
                                                            onChange={(e) => { setPublishedHour(e) }}
                                                            required
                                                            inputStyle={{ textAlign: 'left' }}
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </Grid> </> :
                                            null
                                    } */}


                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding} >
                                        <TextField inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            fullWidth
                                            margin="dense"
                                            variant="standard"
                                            label={I18n.t('article.titre')}
                                            onChange={(e) => setTitle(e.target.value)}
                                            id="title"
                                            name="title"
                                            value={title}
                                            className={classes.textFont}
                                            onBlur={
                                                () => validator.showMessageFor('title')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('title', title, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span>
                                    </Grid>
                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding} >
                                        <TextField inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}

                                            multiline fullWidth margin="dense"
                                            variant="outlined"
                                            label={I18n.t('article.description_fr')}
                                            id="description"
                                            name="description"
                                            value={description}
                                            required onChange={
                                                (e) => setDescription(e.target.value)}
                                            onBlur={
                                                () => validator.showMessageFor('description')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('description', description, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                        // 'min':I18n.t('regex.min_desc')
                                                    }
                                                })
                                            } </span>
                                    </Grid>
                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding} >

                                        <small> {I18n.t('article.contenu')} </small>
                                        <SunEditor setOptions={
                                            {
                                                buttonList: [
                                                    ["undo", "redo"],
                                                    ["font", "fontSize", "formatBlock"],
                                                    ["paragraphStyle", "blockquote"],
                                                    [
                                                        "bold",
                                                        "underline",
                                                        "italic",
                                                        "strike",
                                                        "subscript",
                                                        "superscript",
                                                    ],
                                                    ["fontColor", "hiliteColor", "textStyle"],
                                                    ["removeFormat"],
                                                    "/", // Line break
                                                    ["outdent", "indent"],
                                                    ["align", "horizontalRule", "list", "lineHeight"],
                                                    [
                                                        "table",
                                                        "link",
                                                        "image",
                                                        "video",
                                                        "audio" /** ,'math' */,
                                                    ], // You must add the 'katex' library at options to use the 'math' plugin. // You must add the "imageGalleryUrl".
                                                    /** ['imageGallery'] */
                                                    [
                                                        "fullScreen",
                                                        "showBlocks",
                                                        "codeView",
                                                    ],
                                                    ["preview", "print"],
                                                    ["save", "template"],
                                                ],
                                            }
                                        }
                                            onChange={handleChangeSun}
                                            onDrop={handleDropSun}
                                            setContents={content}
                                            height="300px"
                                            onBlur={() => validator.showMessageFor('content')} />

                                        <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('content', content, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span>

                                    </Grid>
                                    < Grid item xs={12} align={langPage === 'ar' ? "right" : "left"}
                                        className={classes.padding} >

                                        <input className='text-center'
                                            variant='outlined'
                                            accept="image/*"
                                            type="file"
                                            id="select-image"
                                            name={'image'}
                                            label={I18n.t('article.image')}
                                            style={
                                                { display: 'none' }}
                                            onChange={handleChangeImage}
                                            onBlur={
                                                () => validator.showMessageFor('image')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('image', image, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span> <label htmlFor="select-image"
                                                className='text-center' >
                                            <Button variant="contained"
                                                component="span" > {I18n.t('article.importer')} </Button> </label> {
                                            image && (

                                                <Box mt={2}
                                                    xs={
                                                        { flexDirection: 'row' }} > { /* <div>{('articles.image')}</div> */}
                                                    <img src={image ? API_LINK + image : null}
                                                        alt={image.name}
                                                        height="100px" />
                                                </Box>

                                            )
                                        }


                                    </Grid>
                                    {props.user.UserType === "simple" ?
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                namez='author'
                                                label={I18n.t("admin.auth")}
                                                value={authorName.name + " " + authorName.lastname}
                                                className={classes.textFont}
                                            //onBlur={() => validator.showMessageFor('entitledAR')}
                                            /></Grid>
                                        :
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding}>
                                            <TextField
                                                inputProps={langPage == 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                label={I18n.t("admin.auth")}
                                                fullWidth
                                                select
                                                variant="outlined"
                                                name="author"
                                                id="author"
                                                value={dataAuth.find(({ value }) => {
                                                    return value === author
                                                }).value}
                                                margin="dense"
                                                onChange={handleChangeSelectAuthor}
                                            >
                                                {console.log("data auth press", dataAuth.filter(({ value }) => value == author._id)[0])}
                                                {dataAuth
                                                    ? dataAuth.map((item) => {
                                                        console.log("categorie map", item.name);
                                                        return (
                                                            <MenuItem key={item.value} value={item.value} style={langPage == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }}>
                                                                {item.name}
                                                            </MenuItem>
                                                        );
                                                    })
                                                    : null}
                                            </TextField>
                                            <span style={{ 'color': 'red' }}>{validator.message('author', author, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t("article.required"),
                                                    }
                                                })}
                                            </span>
                                        </Grid>
                                    }
                                    <Divider > < small > {I18n.t('article.etat_comment')} </small></Divider >
                                    <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding} >
                                        <Radio.Group options={stateOptions}
                                            onChange={onChangeState}
                                            value={checked}
                                        /> </Grid>
                                    <Divider > </Divider >
                                    <Grid item xs={12} className={classes.padding} align={langPage === 'ar' ? "right" : "left"} >
                                        <TagsInput inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                            required
                                            selectedTags={handleSelecetedTags}
                                            fullWidth variant="outlined"
                                            id="tags"
                                            name="tags"
                                            tags={majid}
                                            placeholder={I18n.t('article.keyWord')}
                                            label={I18n.t('article.keyWord')}
                                            onBlur={
                                                () => validator.showMessageFor('tags')}
                                        /> <span style={
                                            { 'color': 'red' }} > {
                                                validator.message('tags', tags, 'required', {
                                                    messages: {
                                                        'required': I18n.t('article.required'),
                                                    }
                                                })
                                            } </span> </Grid>
                                </Grid>
                                <Divider><small></small></Divider>
                                <Grid item container alignContent="flex-start" className={classes.secondaryContainer}>

                                    <Divider><small></small></Divider>
                                    <Grid item container alignContent="flex-end" >
                                        <Grid item md={3} sm={6} xs={6} alignContent="flex-end">
                                            <Button variant='outlined' onClick={() => setOpenModalEdit(!OpenModalEdit)}>{I18n.t('action.annuler')}</Button>
                                        </Grid>
                                        <Grid item md={3} sm={3} xs={6} alignContent="flex-end">
                                            {/* <Button variant='outlined'
//disabled={!(validator.fieldValid('entitled') || validator.fieldValid('entitledAR'))} 
onClick={addArticle}>{I18n.t('articles.ajouter')}</Button> */}
                                            {/* <Dropdown overlay={menu}>
<Button>
{I18n.t('articles.ajouter')} <DownOutlined />
</Button>
</Dropdown> */}
                                            <ButtonGroup variant="contained" ref={anchorRef} aria-label="split button">
                                                <Button
                                                    disabled={!(validator.fieldValid('title')
                                                        && validator.fieldValid('image')
                                                        && validator.fieldValid('content')
                                                        && validator.fieldValid('category')
                                                        && validator.fieldValid('description')
                                                        && validator.fieldValid('lng')
                                                        && validator.fieldValid('author')
                                                        && validator.fieldValid('tags')
                                                    )}
                                                    onClick={() => handleClick(typeModalSet)}>{menu[selectedIndex]}</Button>
                                                <Button
                                                    size="small"
                                                    aria-controls={open2 ? 'split-button-menu' : undefined}
                                                    aria-expanded={open2 ? 'true' : undefined}
                                                    aria-label="select merge strategy"
                                                    aria-haspopup="menu"
                                                    onClick={handleToggle}
                                                >
                                                    <ArrowDropDownIcon />
                                                </Button>
                                            </ButtonGroup>
                                            <Popper
                                                open={open2}
                                                anchorEl={anchorRef.current}
                                                role={undefined}
                                                transition
                                                disablePortal
                                            >
                                                {({ TransitionProps, placement }) => (
                                                    <Grow
                                                        {...TransitionProps}
                                                        style={{
                                                            transformOrigin:
                                                                placement === 'bottom' ? 'center top' : 'center bottom',
                                                        }}
                                                    >
                                                        <Paper>
                                                            <ClickAwayListener onClickAway={handleClose}>
                                                                <MenuList id="split-button-menu">
                                                                    {menu.map((option, index) => (
                                                                        <MenuItem style={langPage == 'ar' ? { direction: "rtl" } : { direction: 'ltr' }}
                                                                            key={option}
                                                                            selected={index === selectedIndex}
                                                                            onClick={(event) => handleMenuItemClick(event, index)}
                                                                        >
                                                                            {option}
                                                                        </MenuItem>
                                                                    ))}
                                                                </MenuList>
                                                            </ClickAwayListener>
                                                        </Paper>
                                                    </Grow>
                                                )}
                                            </Popper>
                                        </Grid>
                                        {(selectedIndex === 2) ?

                                            <Grid item md={6} sm={6} xs={12} >
                                                < MuiPickersUtilsProvider className={"text-left"} utils={DateFnsUtils} locale={langPage === "ar" ? ar : fr}>
                                                    <DateP inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                        okLabel={I18n.t('admin.confirmer')}
                                                        clearLabel={I18n.t('article.effacer')}
                                                        cancelLabel={I18n.t('action.annuler')}
                                                        //value = { publishedHour ? moment(publishedHour, moment.defaultFormat).toDate() : moment.now() }
                                                        label={I18n.t('article.date_pub')}
                                                        id="publishedHour"
                                                        name="publishedHour"
                                                        format={props.i18n.locale === "ar" ? "yyyy-MM-dd HH:mm" : "dd-MM-yyyy HH:mm"}
                                                        placeholder={props.i18n.locale === "ar" ? 'aaaa/mm/jj ' : 'jj/mm/aaaa'}
                                                        ampm={false}
                                                        onChange={(e) => { setPublishedHour(e) }}
                                                        required
                                                        inputStyle={{ textAlign: 'left' }}
                                                    />
                                                </MuiPickersUtilsProvider>


                                            </Grid>
                                            : null
                                        }
                                    </Grid>

                                </Grid>

                            </Grid>

                        </Grid>  </Dialog>
                </ThemeProvider>
            </StylesProvider>

        )
    }
    const renderDeleteModel = () => {
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="sm"
                        open={OpenModalDelete}
                        onClose={() => setOpenModalDelete(!OpenModalDelete)}

                    >
                        <DialogContent className={classes.padding}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <Grid container>
                                        <Grid item xs={12} Chip className={classes.padding}>
                                            <IconButton
                                                edge="start"
                                                align={langPage === 'ar' ? "left" : "right"}
                                                color="inherit"
                                                aria-label="Close"
                                                style={{ padding: 8 }}
                                                onClick={() => setOpenModalDelete(!OpenModalDelete)}
                                            //  className={classes.padding}
                                            >
                                                <CloseIcon />
                                            </IconButton>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row" className={classes.mainHeader}>
                                        <Grid item xs={12} align={langPage === 'ar' ? "right" : "left"} className={classes.padding}>
                                            <Typography style={{ 'color': 'red' }} variant="h5">
                                                {I18n.t('action.supelementar')}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid

                                        container
                                        direction={langPage === 'ar' ? "rtl" : "ltr"}
                                        className={classes.mainContent}
                                        spacing={4}
                                    >

                                        <Grid item container alignContent="flex-end" >
                                            <Grid item md={3} sm={6} xs={6} alignContent="flex-end" className='text-center'>
                                                <Button variant='outlined' onClick={() => setOpenModalDelete(!OpenModalDelete)} >{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={6} alignContent="flex-end" className='text-center'>
                                                <Button variant='outlined' onClick={() => deleteArticle(articleID)} >{I18n.t('action.supprimer')}</Button>
                                            </Grid>


                                        </Grid>

                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                    </Dialog></ThemeProvider>
            </StylesProvider>
        )
    }
    function onChangePage(pageOfItems) {
        // update state with new page of items
        setPageOfItems(pageOfItems);
    }
    const handleChangetypeIndex = (type) => {
        if (type == 1) {
            getArticles(1, pageSize)
            setTypeIndex(1)
            setColumns([{

                title: I18n.t('article.titre'),
                width: 400,
                dataIndex: 'title',
                key: 'title',
                //fixed: 'left'
            },
            { title: I18n.t('article.author'), dataIndex: 'author', key: '1', width: 150 },
            { title: I18n.t('article.etat'), dataIndex: 'state', key: '2', width: 150 },
            { title: I18n.t('article.date_pub'), dataIndex: 'publishedDate', key: '3', width: 150 },
            { title: I18n.t('article.date_Maj'), dataIndex: 'updateDate', key: '4', width: 150 },
            { title: I18n.t('article.add'), dataIndex: 'addBy', key: '5', width: 150 },
            { title: I18n.t('article.update'), dataIndex: 'editBy', key: '5', width: 150 },
            {
                title: I18n.t('article.liste_comment'),
                dataIndex: 'commentaire',
                key: 'operation'
                //, fixed: 'right'
                ,
                width: 150
            },
            {
                title: I18n.t('action.action'),
                key: 'operation',
                dataIndex: 'action',
                //fixed: 'right',
                width: 150,
                //render: () => <OptionButtons />
            }
            ])
        } else if (type == 2) {
            getCategory();
            setTypeIndex(2)
            setColumnsCat([
                {
                    title: I18n.t('category.categorie_FR'), dataIndex: "entitled", width: 150, key: "1",
                    //fixed: 'left' 
                },
                { title: I18n.t('category.categorie_AR'), width: 100, dataIndex: "entitledAR", key: "2" },
                {
                    title: I18n.t('action.action'),
                    dataIndex: "action",
                    key: "operation",
                    //fixed: "right",
                    width: 50,
                }
            ])

        }
    };
    /****************************** render models category *****************/
    const data2 = [];
    categoriesdata.length ? categoriesdata.forEach((item, i) => {

        data2.push({
            key: i,
            entitled: item.entitled,
            entitledAR: item.entitledAR ? item.entitledAR : '/',
            action: <OptionButtons2
                onEdit={() => showModalCat(item)}
                onDelete={() => viewModalCategory(item._id)}
            />,
        })
    }) : null
    function showModalCat(item) {
        console.log('showModal', item)
        setOpenModalEditCat(true)
        setCatID(item._id)
        setEntitled(item.entitled)
        setEntitledAR(item.entitledAR)

    };
    const viewModalCategory = (id) => {
        setOpenModalDeleteCat(true)
        setCatID(id)
        console.log('here in delete')
    }
    function ShowModalView(item) {
        console.log('vvvvv', item)
        setOpenModalViewCat(true)
        setCatID(item._id)
        setEntitledAR(item.entitledAR)
        setEntitled(item.entitled)

    };
    function getUser() {
        const user = props.user.UserID
        console.log('here', user)
        axios
            .get(API_LINK + "v1/user/" + user)
            .then(response => {
                console.log('user', response.data)
                setAuthorName(response.data)
            })
            .catch((err) => console.log('authorrr', err));
    };


    const addCategory = () => {
        console.log("in add");
        const action = {
            user: props.user.UserID,
            date: moment(),
        };

        let form_data = {
            entitled: entitled,
            entitledAR: entitledAR,
            action: JSON.stringify(action)
        }

        axios
            .post(API_LINK + "v1/categories", form_data)
            .then((response) => {
                getCategory()
                resetState();
                setOpenModalCat(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('category.succesaddcat'),
                        showConfirmButton: true,
                    }))

            })
            .catch((err) => {
                console.log(err.response);
                setOpenModalCat(false)
                getCategory()
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('category.erroraddcat'),
                        showConfirmButton: true,
                    }))
            });
    };
    const updateCategory = () => {
        console.log("in add");
        const action = {
            user: props.user.UserID,
            date: moment(),
        };

        let form_data = {
            entitled: entitled,
            entitledAR: entitledAR,

            action: JSON.stringify(action)
        }

        axios
            .put(API_LINK + "v1/categories/" + catID, form_data)
            .then((response) => {
                getCategory()
                resetState();
                setOpenModalEditCat(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('category.successsetcat'),
                        showConfirmButton: true,
                    }))
            })
            .catch((err) => {
                console.log(err.response);
                setOpenModalEditCat(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('category.errorsetcat'),
                        showConfirmButton: true,
                    }))
            });
    };
    const renderAddModalCat = () => {
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="md"
                        open={OpenModalCat}
                        onClose={() => setOpenModalCat(!OpenModalCat)}

                    >
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={12} className={classes.padding} align={langPage === 'ar' ? 'left' : "right"}>
                                        <IconButton
                                            edge="start"
                                            color="inherit"
                                            aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalCat(!OpenModalCat)}
                                        //  className={classes.padding}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={classes.mainHeader}>
                                    <Grid item xs={12} className={classes.padding} align={langPage === 'ar' ? 'right' : "left"}>
                                        <Typography className={classes.primaryColor} variant="h5">
                                            {I18n.t('category.addCat')}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid

                                    container
                                    direction={langPage === 'ar' ? 'rtl' : "ltr"}
                                    className={classes.mainContent}
                                    spacing={4}
                                >
                                    <Grid>
                                    </Grid>

                                    <>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : 'left'}>
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('category.intitule_FR')}
                                                id="entitled"
                                                name="entitled"
                                                className={classes.textFont}
                                                onChange={(e) => setEntitled(e.target.value)}
                                                required
                                                onBlur={() => validator.showMessageFor('entitled')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('entitled', entitled, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('category.required'),
                                                    }
                                                })}
                                            </span>

                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : 'left'}>
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('category.intitule_AR')}
                                                id="entitledAR"
                                                name="entitledAR"
                                                onChange={(e) => setEntitledAR(e.target.value)}
                                                className={classes.textFont}
                                                required
                                                onBlur={() => validator.showMessageFor('entitledAR')}
                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('entitledAR', entitledAR, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('category.required'),
                                                    }
                                                })}
                                            </span>
                                        </Grid>





                                        <Grid item container alignContent="flex-end" >
                                            <Grid item md={3} sm={6} xs={6} alignContent="flex-end">
                                                <Button onClick={() => setOpenModalCat(!OpenModalCat)}>{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={6} alignContent="flex-end">
                                                <Button
                                                    disabled={!(validator.fieldValid('entitled') && validator.fieldValid('entitledAR'))}
                                                    onClick={addCategory}>{I18n.t('action.enregistrer')}</Button>
                                            </Grid>
                                        </Grid>

                                    </>


                                </Grid>

                            </Grid>

                        </Grid>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )

    }
    const renderEditModalCat = () => {
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="md"
                        open={OpenModalEditCat}
                        onClose={() => setOpenModalEditCat(!OpenModalEditCat)}

                    >
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={12} align={langPage === 'ar' ? 'left' : "right"} className={classes.padding}>
                                        <IconButton
                                            edge="start"
                                            color="inherit"
                                            aria-label="Close"
                                            style={{ padding: 8 }}
                                            onClick={() => setOpenModalEditCat(!OpenModalEditCat)}
                                        //  className={classes.padding}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={classes.mainHeader}>
                                    <Grid item xs={12} className={classes.padding} align={langPage === 'ar' ? 'right' : 'left'}>
                                        <Typography className={classes.primaryColor} variant="h5">
                                            {I18n.t('category.setCat')}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid

                                    container
                                    className={classes.mainContent}
                                    spacing={4}
                                >


                                    <>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : 'left'}>
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('category.intitule_FR')}
                                                id="entitled"
                                                value={entitled}
                                                required
                                                name="entitled"
                                                onChange={(e) => setEntitled(e.target.value)}
                                                className={classes.textFont}
                                                onBlur={() => validator.showMessageFor('entitled')}

                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('entitled', entitled, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('category.required'),
                                                    }
                                                })}
                                            </span>
                                        </Grid>
                                        <Grid item md={6} sm={6} xs={12} align={langPage === 'ar' ? 'right' : 'left'}>
                                            <TextField
                                                inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                required
                                                fullWidth
                                                margin="dense"
                                                variant="outlined"
                                                label={I18n.t('category.intitule_AR')}
                                                id="entitledAR"
                                                name="entitledAR"
                                                onChange={(e) => setEntitledAR(e.target.value)}
                                                value={entitledAR}
                                                className={classes.textFont}
                                                onBlur={() => validator.showMessageFor('entitledAR')}

                                            />
                                            <span style={{ 'color': 'red' }}>{validator.message('entitledAR', entitledAR, 'required',
                                                {
                                                    messages: {
                                                        'required': I18n.t('category.required'),
                                                    }
                                                })}
                                            </span>
                                        </Grid>

                                        <Grid item container alignContent="flex-end" >
                                            <Grid item md={3} sm={6} xs={6} alignContent="flex-end">
                                                <Button onClick={() => setOpenModalEditCat(!OpenModalEditCat)}>{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={6} alignContent="flex-end">
                                                <Button disabled={!(validator.fieldValid('entitled') && validator.fieldValid('entitledAR'))}
                                                    onClick={updateCategory}>{I18n.t('action.modifier')}</Button>
                                            </Grid>
                                        </Grid>

                                    </>


                                </Grid>

                            </Grid>

                        </Grid>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )

    }

    function renderDeleteModelCat() {
        return (
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    <Dialog
                        className={classes.root}
                        fullWidth
                        maxWidth="sm"
                        open={OpenModalDeleteCat}
                        onClose={() => setOpenModalDeleteCat(!OpenModalDeleteCat)}

                    >
                        <DialogContent className={classes.padding}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <Grid container>
                                        <Grid item xs={12} align={langPage === 'ar' ? 'left' : "right"} className={classes.padding}>
                                            <IconButton
                                                edge="start"

                                                color="inherit"
                                                aria-label="Close"
                                                style={{ padding: 8 }}
                                                onClick={() => setOpenModalDeleteCat(!OpenModalDeleteCat)}
                                            //  className={classes.padding}
                                            >
                                                <CloseIcon />
                                            </IconButton>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row" className={classes.mainHeader}>
                                        <Grid item xs={12} className={classes.padding} align={langPage === 'ar' ? 'right' : "left"}>
                                            <Typography style={{ 'color': 'red' }} variant="h5">
                                                {I18n.t('action.supelement')}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid

                                        container
                                        direction="row"
                                        className={classes.mainContent}
                                        spacing={4}
                                    >

                                        <Grid item container alignContent="flex-end" >
                                            <Grid item md={3} sm={3} xs={6} alignContent="flex-end" className='text-center'>
                                                <Button onClick={(e) => setOpenModalDeleteCat(!OpenModalDeleteCat)} >{I18n.t('action.annuler')}</Button>
                                            </Grid>
                                            <Grid item md={3} sm={3} xs={6} alignContent="flex-end" className='text-center'>
                                                <Button onClick={() => deleteCategory(catID)} >{I18n.t('action.supprimer')}</Button>
                                            </Grid>


                                        </Grid>

                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                    </Dialog>
                </ThemeProvider>
            </StylesProvider>
        )
    }



    const deleteCategory = async (id) => {
        await axios
            .delete(API_LINK + "v1/categories/" + id)
            .then((res) => {
                setOpenModalDeleteCat(false)
                getCategory();
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('category.successdeletecat'),
                        showConfirmButton: true,
                    }))
            })
            .catch((err) => {
                console.log(` Une erreur s'est produite lors de la suppression de numéro de press `, err);
                setOpenModalDeleteCat(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('category.errordeletecat'),
                        showConfirmButton: true,
                    }))

            });
    };
    if (
        !verifyPermission(
            props.user.UserID,
            useRouter().pathname,
            props.user.UserType ? props.user.UserType : "simple",
            props.user.UserIsValid ? props.user.UserIsValid : ""
        )
    ) {
        return (
            <>
                <React.Fragment>
                    <VuroxLayout>

                        <ContentLayout width="100%" className="p-3 vurox-scroll-y">
                            <Row align={props.i18n.locale === 'ar' ? "right" : "left"}>
                                <Col md="4"></Col>
                                <Col md="4">
                                    <b
                                        className="text-center text-danger"
                                        style={{ marginTop: "210px", fontSize: "55px" }}
                                    >
                                        {" "}
                                        {props.i18n.locale == "ar" ? "ACCESS DENIED" : "ACCESS DENIED"}
                                    </b>
                                </Col>
                                <Col md="4"></Col>
                            </Row>
                        </ContentLayout>
                    </VuroxLayout>
                </React.Fragment>
            </>
        );
    }

    return (

        <React.Fragment>
            <div dir={langPage === 'ar' ? 'rtl' : 'ltr'}>
                <HeaderLayout className="sticky-top">
                    <HeaderDark />
                </HeaderLayout>
                <VuroxLayout>
                    <VuroxSidebar width={240} className={`sidebar-container  ${toggleClass}`} >
                        <Sidebar className={toggleClass} />
                    </VuroxSidebar>
                    <ContentLayout width='100%' className='p-3 vurox-scroll-y'>

                        <Row className="vurox-admin-content-top-section" dir={langPage === 'ar' ? 'rtl' : "ltr"}>

                            <Col md={3} align={langPage === 'ar' ? 'right' : "left"}>
                                <Title level={2}>{I18n.t("article.article")}</Title>
                            </Col>
                           {typeIndex == 1 ? 
                           <>
                            <Col md={2} xs={4}></Col>
                            <Col md={4} align={langPage === 'ar' ? 'right' : "left"}>
                                <TextField
                                    inputProps={langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                    //label={I18n.t('article.langue')}
                                    fullWidth select required variant="outlined"
                                    id="state"
                                    name="state"
                                    style={{background :"#fff", borderRadius: "10px"}}
                                    margin="dense"

                                    onChange={(event) =>
                                        handleChangeState(event)
                                    }
                                >
                                    <MenuItem key={'En cours de redaction'}
                                        value="En cours de redaction"
                                        style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.articles_red')} </MenuItem>
                                    <MenuItem key={'En attente de correction'}
                                        value={'En attente de correction'}
                                        style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.articles_correc')} </MenuItem>
                                    <MenuItem key={'Publier'}
                                        value={'Publier'}
                                        style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.articles_publier')} </MenuItem>

                                    <MenuItem key={'Programmer'}
                                        value={'Programmer'}
                                        style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.articles_prog')} </MenuItem>
                                    <MenuItem key={'Supprimer'}
                                        value={'Supprimer'}
                                        style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }} > {I18n.t('article.articles_sup')} </MenuItem>
                                </TextField>
                            </Col>

                            <Col md={2} xs={4}></Col>
                            <Col md={4} align={langPage === 'ar' ? 'right' : "left"}>
                                <div className='vurox-search-from inline-flex mb-1'>
                                    <label>{I18n.t("action.show_entries")}:</label>&nbsp;
                                    <input style={{ width: "60px", border: "1px solid #ccc", borderRadius: "10px", padding: "0 0px 0 4px" }} type="text" onChange={ChangePageSize} />


                                </div>

                            </Col>
                        </>  : null}
                    
                            <Col md={2} xs={4}></Col>
                            <Col md={4} align={langPage === 'ar' ? 'right' : "left"}>
                                <div className='vurox-search-from mb-1'>
                                    <input type="text" placeholder={I18n.t("action.recherche") + " ..."} onChange={searchArticlePress} />
                                    <SearchOutlined />

                                </div>
                            </Col>



                            <Col md={2} xs={4}></Col>
                            <Col md={5} >

                                <div className={props.i18n.locale === 'ar' ? "float-md-left mb-2" : "float-md-right mb-2"}>
                                    <button
                                        onClick={() => {
                                            if (typeIndex == 1) {
                                                { setOpenModalAdd(true), resetState() }
                                            } else if (typeIndex == 2) {
                                                { setOpenModalCat(true), resetState() }

                                            }
                                        }}
                                        type="button"
                                        className="dash_button float-none float-sm-right mr-2 btn white bg-magenta-5 btn-md rounded hover-color my-3 my-sm-0 d-block"
                                        style={{
                                            background: "#00C1D8",
                                            
                                            color: "white",
                                            fontSize: "16px",
                                        }}
                                    >
                                        {I18n.t("action.ajouter")}
                                    </button>
                                </div>


                            </Col>
                        </Row>


                        <Row dir={langPage === 'ar' ? 'rtl' : "ltr"}>
                            <Col md={3} className={`${langPage == "ar" ? 'text-right' : ''}`}>

                                <div className="vurox-mail-links vurox-admin-grey-bg rounded d-sm-block">
                                    <ul>
                                        <li>
                                            <p
                                                style={{
                                                    color: typeIndex == 1 ? "#00C1D8" : "#000000",
                                                }}
                                                onClick={() => {
                                                    handleChangetypeIndex(1);
                                                    // this.setState({ typeIndex: 1 });
                                                }}
                                            >
                                                {I18n.t("article.liste_articles")}

                                            </p>
                                        </li>
                                        <li>
                                            <p
                                                style={{
                                                    color: typeIndex == 2 ? "#00C1D8" : "#000000",
                                                }}
                                                onClick={() => {
                                                    handleChangetypeIndex(2);
                                                    //this.setState({ typeIndex: 2 });
                                                }}
                                            >
                                                {I18n.t("category.liste_categories")}
                                            </p>
                                        </li>


                                    </ul>
                                </div>

                            </Col>

                            <Col md={21} align={langPage === 'ar' ? 'right' : "left"}>
                                {

                                    isLoading ?

                                        <Spinner loading={isLoading} />
                                        :
                                        <>

                                            <ConfigProvider direction={langPage === 'ar' ? 'rtl' : "ltr"}>
                                                <Table columns={typeIndex === 1 ? columns : columnsCat} dataSource={typeIndex === 1 ? data : data2} scroll={{ x: 1500, y: 3000 }}
                                                    pagination={false} />
                                                {typeIndex === 1 ?
                                                    <div className={`${langPage == "ar" ? "alignLeft float-left mt-2" : "alignRight float-right mt-2"}`}>
                                                        <Pagination perPage={pageSize} total={totalCount} langue={langPage} />
                                                    </div> : null}
                                            </ConfigProvider>
                                        </>

                                }
                            </Col>

                        </Row>


                        {OpenModalAdd ? renderAddModal() : null}
                        {OpenModalEdit ? renderUpdateModal() : null}
                        {OpenModalView ? renderViewModal() : null}
                        {OpenModalDelete ? renderDeleteModel() : null}
                        {OpenModalComment ? renderComments() : null}
                        {moderateModal ? moderateCommentModal() : null}

                        {/* //------****** Categoriess----- */}
                        {OpenModalCat ? renderAddModalCat() : null}
                        {OpenModalDeleteCat ? renderDeleteModelCat() : null}
                        {OpenModalEditCat ? renderEditModalCat() : null}

                    </ContentLayout>

                </VuroxLayout>
            </div>
        </React.Fragment>


    )
}


export default connect(state => state)(withStyles(styles)(article))
