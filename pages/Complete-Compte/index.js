
import {
    AutoComplete,
    Avatar,
    Card,
    Space,
    Col,
    Divider,
    Form,
    Input,
    Menu,
    Button,
    Progress,
    DatePicker,
    Row,
    Select,
    Timeline,
    Tooltip, ConfigProvider,
    Upload,
    Icon,
    Table,
} from "antd";
import {
    UploadOutlined,
    InboxOutlined,
    LinkedinOutlined,
    MinusCircleOutlined,
    PlusOutlined,
} from "@ant-design/icons";
import SimpleReactValidator from 'simple-react-validator';
import { verifyPermission } from "../../utils/functions";
import { FaEye, FaTrash, FaPencilAlt, FaCheckCircle, FaTimesCircle, FaPlus } from 'react-icons/fa';
import Swal from "sweetalert2"
import "date-fns";
//import 'antd/es/grid/style/index.css'
import DateFnsUtils from "@date-io/date-fns";
import {
    MuiPickersUtilsProvider,
    DatePicker as DateP,
} from "material-ui-pickers";
import React, { useState, useEffect, useContext, Fragment } from "react";
import moment from "moment";
// import Spinner from "../Components/spinner";
import ProfileBadge from "../../Components/profile";
import { API_LINK, CODE_PHONE } from "../../utils/constantes";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { connect } from 'react-redux'
import { StylesProvider, ThemeProvider, jssPreset } from "@material-ui/styles";

import { fr } from "date-fns/locale";
import ar from "date-fns/locale/ar-DZ";
import { createMuiTheme } from "@material-ui/core/styles";

import { I18n } from "react-redux-i18n";
import { vuroxContext } from "../../context/index";
import HeaderDark from "../../Templates/HeaderDark";
import Sidebar from "../../Templates/HeaderSidebar";
import {
    VuroxLayout,
    HeaderLayout,
    VuroxSidebar,
    ContentLayout,
} from "../../Components/layout";
import { BellOutlined } from "@ant-design/icons";
import TextArea from "antd/lib/input/TextArea";
import { withStyles, MenuItem } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import { Divider as Dvd } from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
// import Button from "@material-ui/core/Button";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { teal, grey } from "@material-ui/core/colors";
import { useRouter } from "next/router";
import { create } from "jss";
import rtl from "jss-rtl";
// import { Domaine } from "../../utils/constantes";
const { Dragger } = Upload;
const FormItem = Form.Item;
const Option = Select.Option;

const OptionButtons = (props) => {
    return (
        <Fragment>
            {props.valid ? <Button color="warning" outline className="btn-sm mx-1" onClick={() => props.onValid()} > {" "} <FaCheckCircle />{" "} </Button> :
                <Button color="warning" outline className="btn-sm mx-1" onClick={() => props.onValid()} > {" "} <FaTimesCircle />{" "}</Button>}
            <Button type="success" outline className="btn-sm mx-1" onClick={() => props.onEdit()}> <FaPencilAlt /> </Button>
            <Button outline className="btn-sm mx-1" onClick={() => props.onDelete()}> <FaTrash /> </Button>

        </Fragment>
    );
}


const styles = (theme) => ({
    root: {
        flexGrow: 5,
        fontSize: "1.5rem",
    },
    inputText: {
        style: { textAlign: "right" },
    },
    primaryColor: {
        color: "rgb(0, 193, 216)",
        fontSize: "27px",
    },
    secondaryColor: {
        color: grey[700],
    },

    padding: {
        padding: 0,
    },
    textFont: {
        fontSize: "1.5rem",
    },
    mainHeader: {
        backgroundColor: grey[100],
        padding: 20,
        alignItems: "center",
    },
    mainContent: {
        padding: 40,
    },
    secondaryContainer: {
        padding: "20px 25px",
        backgroundColor: grey[200],
    },
});






const formItemLayout = {
    labelCol: {
        lg: { span: 6 },
        md: { span: 24 },
        xs: { span: 24 },
        sm: { span: 24 },
    },
    wrapperCol: {
        lg: { span: 18 },
        md: { span: 24 },
        xs: { span: 24 },
        sm: { span: 24 },
    }
};
const formItemLayoutDate = {
    labelCol: {
        lg: { span: 6 },
        md: { span: 24 },
        xs: { span: 24 },
        sm: { span: 24 },
    },
    wrapperCol: {
        lg: { span: 18 },
        md: { span: 24 },
        xs: { span: 24 },
        sm: { span: 24 },
    }
};
const formItemLayoutPassword = {
    labelCol: {
        xs: { span: 8 },
        sm: { span: 8 }
    },
    wrapperCol: {
        xs: { span: 8 },
        sm: { span: 8 }
    }
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};

function Compte(props) {
    const { classes, open, onClose } = props;
    const Router = useRouter()
    const [activeTab, setActiveTab] = useState('1');
    const [autoCompleteResult, setAutoCompleteResult] = useState([]);
    const { menuState } = useContext(vuroxContext)
    const toggleClass = menuState ? 'menu-closed' : 'menu-open'
    const [description, setDescription] = useState("");
    const [loadUser, setLoadUser] = useState(false)
    const [name, setName] = useState("")
    const [lastname, setLastName] = useState('')
    const [mail, setMail] = useState("")
    const [date, setBirthday] = useState("")
    const [pseudo, setPseudo] = useState("")
    const [sexe, setSexe] = useState("")
    const [adress, setAdress] = useState("")
    const [codePhoneA, setCodePhoneA] = useState("")
    const [phone, setPhone] = useState("")
    const [ville, setVille] = useState("")
    const [pays, setPays] = useState("")
    const [image, setImage] = useState("")
    const [user, setUser] = useState("")
    const [type, setType] = useState("")
    const [type2, setType2] = useState("")
    const [id, setId] = useState("")
    const [resLien, setResLien] = useState([])
    const [reseauResult, setReseau] = useState([]);
    const [resType, setResType] = useState([])
    const [password, setPassword] = useState("")
    const [passwordnew, setPasswordnew] = useState("")
    const [biographie, setBiographie] = useState("")
    const [passwordconfirm, setPasswordconfirm] = useState("")
    // const [lien, setLien] = useState(Domaine+"Complete-Compte")
    const dispatch = useDispatch();
    const [form] = Form.useForm();
    const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
    const theme = createMuiTheme({
        direction: "ltr"
    });
    const themeAR = createMuiTheme({
        direction: "rtl"
    });
    const validator = new SimpleReactValidator({ autoForceUpdate: this });

    useEffect(() => {
        console.log("prooops useeeer", Router.asPath.split("?")[1])

        const userID = Router.asPath.split("?")[1]

        if (userID) {

            getUserById(userID)
        }
    }, []);

    useEffect(() => {
        console.log("prooops phone", phone, codePhoneA, pseudo)
    }, [phone, codePhoneA, pseudo]);
    function getUserById(id) {
        console.log('here in get', API_LINK + "v1/userByMail/" + id)
        axios.get(API_LINK + "v1/userByMail/" + id)
            .then((res) => {
                console.log('user after mail', res.data)
                if (res.data.name) {
                    setName(res.data.name)
                }
                if (res.data.lastname) {

                    setLastName(res.data.lastname)
                }

                setMail(res.data.mail ? res.data.mail : "")
                setBirthday(res.data.date ? res.data.date : "1900-12-01T00:00:00.000+00:00")
                setPseudo(res.data.pseudo ? res.data.pseudo : "")
                setSexe(res.data.sexe ? res.data.sexe : "")
                setPhone(res.data.phone ? res.data.phone.split("-")[1] : "")
                setCodePhoneA(res.data.phone ? res.data.phone.split("-")[0] : "+213")
                setVille(res.data.ville ? res.data.ville : "")
                setPays(res.data.pays ? res.data.pays : "")
                setImage(res.data.picture ? res.data.picture : "")
                setDescription(res.data.description ? res.data.description : "")
                setAdress(res.data.adress ? res.data.adress : "")
                setBiographie(res.data.biographie ? res.data.biographie : "")
                setUser(res.data)
                if (res.data.reseaux && res.data.reseaux.length > 0) {
                    res.data.reseaux.map((ress) => {
                        resx.push({
                            type: ress.type,
                            lien: ress.lien
                        })
                    })
                    setReseau(resx)
                }

                console.log('here in get', res.data)
                setLoadUser(true)
                setType(res.data.type ? res.data.type : "")
                if (res.data.type2 == "auteur") {
                    setType2("Auteur")
                } else if (res.data.type2 == "admin") {
                    setType2("Admin")
                }

                setId(res.data._id ? res.data._id : "")
            })
            .catch((err) => {
                console.log("err", err)
            })
    }
    function onModifier(v) {
     
        const userID = Router.asPath.split("?")[1]
        form.validateFields();
        let reseau = []
        let unreseau = { type: "", lien: "" }
        if (resLien.length > 0) {
            for (let i = 0; i < resLien.length; i++) {
                unreseau.lien = resLien[i]
                unreseau.type = resType[i] ? resType[i] : "Facebook"
                reseau.push(unreseau)
                unreseau = { type: "", lien: "" }
            }
        } 
          console.log("rezzzz", reseau, "le resteeeee ",passwordnew)
        console.log("rezzzz", reseau, "le resteeeee ", resLien, resType)
        if (reseauResult.length > 0) {
            reseau = reseau.length > 0 ? reseau.concat(reseauResult) : reseauResult
        }

        const phonemobile = codePhoneA && phone ? codePhoneA + "-" + phone : ""
        let newUser = new FormData();
        let action = {
            user: id,
            date: moment()
        }
        newUser.append("mail", mail);
        newUser.append("name", name);
        newUser.append("lastname", lastname);
        newUser.append("description", description);
        newUser.append("type", type);
        newUser.append("date", date);
        newUser.append("ville", ville);
        newUser.append("pays", pays);
        newUser.append("adress", adress);
        newUser.append("sexe", sexe);
        newUser.append("pseudo", pseudo);
        newUser.append("phonemobile", phonemobile);
        newUser.append("password", passwordnew);
        newUser.append("biographie", biographie);
        newUser.append("reseau", JSON.stringify(reseau));
        newUser.append("image", image);
        // newUser.append("lien", lien);
        newUser.append("action", JSON.stringify(action));
        console.log("onmodifier", userID);
        axios
            .put(API_LINK + "v1/editeuserByMail/" + userID, newUser, {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            })
            .then((res) => {
                console.log("reeeesss", res.data);
                if (res.data) {
                    //   props.dispatch({ type: 'NAMEUSER_LOGGEDIN', payload: user.name });
                    //   props.dispatch({ type: 'LASTNAMEUSER_LOGGEDIN', payload: user.lastname });
                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: "",
                        text: I18n.t("profil.setsuccess"),
                        showConfirmButton: true,
                    })
                    const user = {
                        UserID: res.data.user._id,
                        UserMail: res.data.user.mail,
                        UserType:res.data.user.type,
                        UserToken:res.data.token,
                        UserIsValid:res.data.user.isvalid,
                        userName:"",
                        userLastName:""
                      }
                 
                      props.dispatch({ type: 'LOGGED_IN', payload: user });

                   Router.replace("/profil");
                }
            })
            .catch((err) => {
                console.log("eeerrr", err);
                Swal.fire({
                    icon: "error",
                    title: " ",
                    confirmButtonColor: "#a6c76c",
                    cancelButtonColor: "#a6c76c",
                    confirmButtonText: "Oui",
                    text: I18n.t("profil.seterror"),
                });
            });
    }
    const HandleChangeSelect = (e, index) => {
        console.log("reseauuu",e,index,reseauResult)
        let rest = reseauResult
        const oneres = { type: e, lien: reseauResult[index].lien }
        rest.splice(index, 1, oneres),
            setReseau(rest)
        console.log("seleeect", e, index, reseauResult)
    };
    const HandleChangeInput = (e, index) => {
        console.log("seleeect", e, index)
        let resl = reseauResult
        const oneres = { type: reseauResult[index].type, lien: e.target.value }
        resl.splice(index, 1, oneres),
            setReseau(resl)
    };
    const handleRemoveClick = index => {
        const list = [...reseauResult];
        list.splice(index, 1);
        setReseau(list);
    };

    const prefixSelectorAuteur = (
        <Select dir="ltr" defaultValue={codePhoneA} onChange={(e) => setCodePhoneA(e)} >
            {CODE_PHONE.map(item => { return (<Option className={props.i18n.locale == "ar" ? "text-right" : ""} value={item}>{item}</Option>) })}

        </Select>
    );
    return (
        <React.Fragment>
            {console.log("user in return", user)}
            <div dir={props.i18n.locale === 'ar' ? "rtl" : "ltr"}>
                <StylesProvider jss={jss}>
                    <ThemeProvider theme={props.i18n.locale == "ar" ? themeAR : theme}>
                        {/* <HeaderLayout className="sticky-top">
                            <HeaderDark />
                        </HeaderLayout> */}
                        <VuroxLayout>
                            {/* <VuroxSidebar width={240} className={`sidebar-container  ${toggleClass}`} >
                                <Sidebar className={toggleClass} />
                            </VuroxSidebar> */}


                            <ContentLayout width='100%' className='p-3 vurox-scroll-y' >
                                {/* <Card
                                    headStyle={{
                                        dir: props.i18n.locale == "ar" ? "rtl" : "ltr",
                                        backgroundColor: "#00C1D8",
                                        backgroundSize: 'cover',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundPosition: 'center center'
                                    }}
                                    bodyStyle={{ padding: 0 }}
                                    className="mb-4 overflow-hidden w-100"
                                    title={
                                        <Row type="flex" align="middle" >
                                            {user.picture ?
                                                <Avatar size={64} src={API_LINK + user.picture} /> : <ProfileBadge name='S' size='lg' shape='rounded' version='dark' className='mt-3 ml-2 vurox-dropdown' badgeColor='bg-white' badgeShape='circle' />}

                                            <div
                                                className={`px-4 text-light ${props.i18n.locale == "ar" ? "text-right" : ""}`} css={` display: inline-block; `} >
                                                <h5 className="my-0 text-white  ">
                                                    <span>{user.name}</span>
                                                    <b> {user.lastname}</b>
                                                </h5>
                                                <small>{type}</small>
                                            </div>
                                        </Row>
                                    }
                                    extra={
                                        <Row type="flex" align="middle" className="p-4">
                                            <Button type="dashed" shape="circle" ghost>
                                                <BellOutlined style={{ fontSize: '20px' }} />
                                            </Button>
                                        </Row>
                                    }
                                >
                                    <Menu
                                        onClick={tab => {
                                            if (activeTab !== tab.key) setActiveTab(tab.key);
                                        }}
                                        selectedKeys={[activeTab]}
                                        mode="horizontal"
                                        className="border-bottom-0"
                                        align={props.i18n.locale == "ar" ? "right" : "left"}
                                        direction={props.i18n.locale == "ar" ? "rtl" : "ltr"}
                                    >
                                        <Menu.Item key="1" style={{ direction: props.i18n.locale == "ar" ? "rtl" : "ltr" }} >{I18n.t("profil.profil")}</Menu.Item>
                                        <Menu.Item key="2" style={{ direction: props.i18n.locale == "ar" ? "rtl" : "ltr" }} >Sécurité</Menu.Item>
                                    </Menu>
                                </Card> */}


                                <Row type="flex" gutter={16}>
                                    {/* <Col
                                        xl={8}
                                        lg={24}
                                        md={24}
                                        sm={24}
                                        xs={24}
                                        order={2}
                                    >
                                        <Card bodyStyle={{ padding: 0 }} className={`mb-4 ${props.i18n.locale == "ar" ? "text-right" : ""}`}>
                                            <div className="px-4 pt-4">

                                                <Row type="flex" align="top" justify="space-between">
                                                    <Col>
                                                        <h5 className="m-0">
                                                            <span>{user.name}</span> <b>{user.lastname}</b>
                                                        </h5>
                                                        <p className="mb-0">{type}</p>
                                                        <a
                                                            href="#"
                                                            style={{ display: "block" }}
                                                        >
                                                            {user.mail}
                                                        </a>
                                                        {phone ? <a
                                                            href="#"
                                                            style={{ display: "block" }}
                                                            dir="ltr"
                                                        >
                                                            {user.phone}
                                                        </a> : null}
                                                    </Col>


                                                    <Col>
                                                        <div className="text-center">
                                                            {user.picture ?
                                                                <Avatar size={64} src={API_LINK + user.picture} /> : <ProfileBadge name='S' size='lg' shape='rounded' version='dark' className='mt-3 ml-2 vurox-dropdown' badgeColor='bg-white' badgeShape='circle' />}
                                                        </div>
                                                        <div className="m-1 text-center">
                                                            <p className="mb-1">Account Storage</p>
                                                            <Tooltip title="3 done / 3 in progress / 4 to do">
                                                                <Progress percent={60} successPercent={30} />
                                                            </Tooltip>
                                                        </div>
                                                    </Col>
                                                </Row>

                                            </div>

                                            <Divider >
                                                <small>Stats</small>
                                            </Divider>

                                            <Row
                                                className="text-center w-100 px-4"
                                                type="flex"
                                                align="middle"
                                                justify="space-between"
                                            >
                                                <Col span={8}>
                                                    <h2 className="m-0">
                                                        <b>23,8K</b>
                                                    </h2>
                                                    <small>Followers</small>
                                                </Col>
                                                <Col span={8}>
                                                    <h2 className="m-0">
                                                        <b>569</b>
                                                    </h2>
                                                    <small>Following</small>
                                                </Col>
                                                <Col span={8}>
                                                    <h2 className="m-0">
                                                        <b>67</b>
                                                    </h2>
                                                    <small>Posts</small>
                                                </Col>
                                            </Row>

                                            <Divider />
                                            <div className="px-4 pb-4">
                                                <p className="text-uppercase mb-4">
                                                    <strong>About Me</strong>
                                                </p>
                                                <p>
                                                    Maecenas sed diam eget risus varius blandit sit amet non magna.
                                                    Curabitur blandit tempus porttitor. Vestibulum id ligula porta
                                                    felis euismod semper.
                                                </p>
                                            </div>
                                        </Card>

                                    </Col> */}

                                    <Col
                                        xl={16}
                                        lg={24}
                                        md={24}
                                        sm={24}
                                        xs={24}
                                        order={1}
                                    >

                                        {activeTab === '1' && (

                                            <Card className={`${props.i18n.locale == "ar" ? "text-right" : ""}`}>

                                                {type === "simple" ?

                                                    <Form form={form} onFinish={(values) => onModifier(values)}
                                                        initialValues={{
                                                            lname: name,
                                                            llastname: lastname,
                                                            lcivil: sexe,
                                                            email: mail,
                                                            pseudo: pseudo,
                                                            phone: phone,
                                                            date: date ? moment(date) : "",
                                                            pays: pays,
                                                            ville: ville,
                                                            reseaux: reseauResult

                                                        }}>
                                                        <FormItem   {...formItemLayout} label={I18n.t("profil.mail")} >
                                                            <Input defaultValue={mail} value={mail} onChange={(e) => setMail(e.target.value)} readOnly disabled />
                                                        </FormItem>
                                                        <FormItem    {...formItemLayout} label={I18n.t("profil.sexe")}>
                                                            <Select style={{ width: '100%' }} defaultValue={sexe} value={sexe} onChange={(e) => setSexe(e)}>
                                                                <Option className={props.i18n.locale == "ar" ? "text-right" : ""} value="Femme">{I18n.t("profil.male")}</Option>
                                                                <Option className={props.i18n.locale == "ar" ? "text-right" : ""} value="Homme">{I18n.t("profil.female")}</Option>
                                                            </Select>
                                                        </FormItem>

                                                        <FormItem   {...formItemLayout} value="fgdfg" label={I18n.t("profil.nom")} rules={[
                                                            {
                                                                required: true,
                                                                message: I18n.t("journal.required")
                                                            }
                                                        ]}>
                                                            <Input defaultValue={name} value={name} onChange={(e) => setName(e.target.value)} />
                                                        </FormItem>

                                                        <FormItem   {...formItemLayout} label={I18n.t("profil.prenom")} defaultValue={lastname} rules={[
                                                            {
                                                                required: true,
                                                                message: I18n.t("journal.required")
                                                            }
                                                        ]}>
                                                            <Input defaultValue={lastname} value={lastname} onChange={(e) => setLastName(e.target.value)} />
                                                        </FormItem>


                                                        <FormItem   {...formItemLayout} label={I18n.t("profil.pseudo")}  >
                                                            <Input defaultValue={pseudo} value={pseudo} onChange={(e) => setPseudo(e.target.value)} />
                                                        </FormItem>
                                                        <FormItem   {...formItemLayoutDate} label={I18n.t("profil.date_naissance")}  >

                                                            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={props.i18n.locale == "ar" ? ar : fr}   >

                                                                <DateP

                                                                    // inputProps={props.i18n.locale  == "ar" ? { style: { textAlign: "right" } } : { style: { textAlign: "left" } }}
                                                                    okLabel={I18n.t("journal.ok")}
                                                                    clearLabel={I18n.t("journal.clear")}
                                                                    cancelLabel={I18n.t("journal.cancel")}
                                                                    //variant="outline"
                                                                    label={""}

                                                                    format="dd-MM-yyyy"
                                                                    value={date ? moment(date) : ""} defaultValue={date ? moment(date) : ""} onChange={(date, dateString) => setBirthday(date)}
                                                                />

                                                            </MuiPickersUtilsProvider>
                                                        </FormItem>



                                                        <FormItem   {...formItemLayout} label={I18n.t("profil.Phone")} rules={[
                                                            {
                                                                pattern: /(5|6|7)[0-9]{8}$/,
                                                                message: I18n.t("regex.phone")
                                                            },

                                                        ]}>

                                                            <div dir="ltr">
                                                                <Input
                                                                    className="text-left"
                                                                    addonBefore={prefixSelectorAuteur}
                                                                    style={{ width: '100%' }}
                                                                    defaultValue={phone}
                                                                    value={phone}
                                                                    onChange={(e) => setPhone(e.target.value)}
                                                                />
                                                            </div>
                                                        </FormItem>
                                                        {type2 == "Auteur" ?
                                                                reseauResult.length > 0 ?
                                                                    <>
                                                                        <Form.Item >
                                                                            <Form.List compact name="Ereseaux"  >
                                                                                {(fields, { add, remove }) => {
                                                                                    let rest, resl
                                                                                    return (
                                                                                        <>
                                                                                            {
                                                                                                reseauResult.map((resx, index) => (
                                                                                                    <Form.Item
                                                                                                        labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                        {...formItemLayout}
                                                                                                        label={index == 0 ? I18n.t('Profil.medea') : "    "}
                                                                                                        required={false}
                                                                                                        colon={index == 0 ? true : false}
                                                                                                        key={index}
                                                                                                    >
                                                                                                        <Form.Item
                                                                                                            labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                            validateTrigger={['onChange', 'onBlur']}
                                                                                                            rules={[
                                                                                                                {
                                                                                                                    type: 'url',
                                                                                                                    message: I18n.t('dash.compte.url')
                                                                                                                }
                                                                                                            ]}
                                                                                                            noStyle
                                                                                                        >


                                                                                                            <Input
                                                                                                                addonBefore={
                                                                                                                    <Select defaultValue={resx.type} style={{ width: 110 }} onChange={(e) => HandleChangeSelect(e, index)}>
                                                                                                                        <Option value="Twitter">{I18n.t('profil.tw')}</Option>
                                                                                                                        <Option value="Facebook" selected>{I18n.t('profil.fb')}</Option>
                                                                                                                        <Option value="Instagram" selected>{I18n.t('profil.insta')}</Option>
                                                                                                                        <Option value="Youtube" selected>{I18n.t('profil.you')}</Option>
                                                                                                                        <Option value="LinkedIn" selected>{I18n.t('profil.Link')}</Option>
                                                                                                                        <Option value="WhatsApp" selected>{I18n.t('profil.whatsapp')}</Option>
                                                                                                                        <Option value="Viber" selected>{I18n.t('profil.viber')}</Option>
                                                                                                                        <Option value="Telegram" selected>{I18n.t('Profil.telegram')}</Option>

                                                                                                                    </Select>
                                                                                                                }
                                                                                                                style={{ width: '70%' }}
                                                                                                                defaultValue={resx.lien}
                                                                                                                onChange={(e) => HandleChangeInput(e, index)}
                                                                                                                allowClear />
                                                                                                        </Form.Item>
                                                                                                        &nbsp;
                                                                                                        <Button
                                                                                                            style={{ width: '10%' }}
                                                                                                            type="dashed"
                                                                                                            onClick={() => handleRemoveClick(index)}
                                                                                                            icon={<MinusCircleOutlined />}
                                                                                                        />
                                                                                                    </Form.Item>

                                                                                                ))

                                                                                            }


                                                                                            {fields.length == 0 ?
                                                                                                <Form.Item
                                                                                                    labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                    {...formItemLayout}
                                                                                                    label="    "
                                                                                                    colon={false}>
                                                                                                    <Button
                                                                                                        style={{ width: '80%' }}
                                                                                                        type="dashed"
                                                                                                        onClick={() => add()}
                                                                                                        block
                                                                                                        icon={<PlusOutlined />} > {I18n.t('compte.addsocial')} </Button>
                                                                                                </Form.Item> : null}

                                                                                            {fields.map((field, index) => (
                                                                                                <Form.Item
                                                                                                    labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                    {...formItemLayout}
                                                                                                    label="    "
                                                                                                    colon={false}
                                                                                                    required={false}
                                                                                                    key={field.key}
                                                                                                >
                                                                                                    <Form.Item

                                                                                                        {...field}
                                                                                                        labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                        validateTrigger={['onChange', 'onBlur']}
                                                                                                        rules={[
                                                                                                            {
                                                                                                                type: 'url',
                                                                                                                message: I18n.t('dash.compte.url')
                                                                                                            }
                                                                                                        ]}
                                                                                                        noStyle
                                                                                                    >

                                                                                                        <Input
                                                                                                            addonBefore={
                                                                                                                <Select defaultValue="Facebook" style={{ width: 110 }} onChange={(e) => { rest = resType, rest.splice(index, 1, e), setResType(rest), console.log("reseauu test 1", resType, index, e) }}>
                                                                                                                    <Option value="Twitter">{I18n.t('profil.tw')}</Option>
                                                                                                                    <Option value="Facebook" selected>{I18n.t('profil.fb')}</Option>
                                                                                                                    <Option value="Instagram" selected>{I18n.t('profil.insta')}</Option>
                                                                                                                    <Option value="Youtube" selected>{I18n.t('profil.you')}</Option>
                                                                                                                    <Option value="LinkedIn" selected>{I18n.t('profil.Link')}</Option>
                                                                                                                    <Option value="WhatsApp" selected>{I18n.t('profil.whatsapp')}</Option>
                                                                                                                    <Option value="Viber" selected>{I18n.t('profil.viber')}</Option>
                                                                                                                    <Option value="Telegram" selected>{I18n.t('Profil.telegram')}</Option>
                                                                                                                </Select>
                                                                                                            }
                                                                                                            style={{ width: '70%' }}
                                                                                                            onChange={(e) => { resl = resLien, resl.splice(index, 1, e.target.value), setResLien(resl), console.log("reseauu test 2", resLien, index) }}
                                                                                                            allowClear />
                                                                                                    </Form.Item>
                                                                                                    &nbsp;
                                                                                                    {reseauResult.length + fields.length < 5 ?
                                                                                                        fields.length - 1 === index ?
                                                                                                            <Button
                                                                                                                style={{ width: '10%' }}
                                                                                                                type="dashed"
                                                                                                                onClick={() => add()}
                                                                                                                icon={<PlusOutlined />}
                                                                                                            /> : null
                                                                                                        : null}
                                                                                                    <Button
                                                                                                        style={{ width: '10%' }}
                                                                                                        type="dashed"
                                                                                                        onClick={() => {
                                                                                                            remove(field.name),
                                                                                                                resl = resLien, resl.splice(index, 1), setResLien(resl), console.log("defsdfffff", resLien, index),
                                                                                                                rest = resType, rest.splice(index, 1), setResType(rest), console.log("defsdf", resType, index)
                                                                                                        }}

                                                                                                        icon={<MinusCircleOutlined />}
                                                                                                    />
                                                                                                </Form.Item>

                                                                                            ))}






                                                                                        </>)
                                                                                }}
                                                                            </Form.List>
                                                                        </Form.Item>
                                                                        <FormItem   {...formItemLayout} label={I18n.t("profil.biographie")}  >
                                                            <Input aria-multiline defaultValue={biographie} value={biographie} onChange={(e) => setBiographie(e.target.value)} />
                                                        </FormItem> </>
                                                                    :
                                                                    <>
                                                                        <Form.Item >
                                                                            <Form.List compact
                                                                                name="Ereseaux"
                                                                            >
                                                                                {(fields, { add, remove }) => {
                                                                                    console.log("fffff", fields)
                                                                                    let rest, resl
                                                                                    return (
                                                                                        <>
                                                                                            {fields.length == 0 ?
                                                                                                <Form.Item {...formItemLayout}
                                                                                                    labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                    label="    "
                                                                                                    required={false}
                                                                                                    colon={false}
                                                                                                >
                                                                                                    <Button
                                                                                                        type="dashed"
                                                                                                        onClick={() => add()}
                                                                                                        block
                                                                                                        icon={<PlusOutlined />} >{I18n.t('dash.compte.addsocial')} </Button>
                                                                                                </Form.Item> : null}
                                                                                            {console.log("filedsss", fields.length)}
                                                                                            {
                                                                                                fields.map((field, index) => (
                                                                                                    <Form.Item
                                                                                                        labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                        {...formItemLayout}
                                                                                                        label={index == 0 ? I18n.t('dash.compte.medea') : "    "}
                                                                                                        required={false}
                                                                                                        colon={index == 0 ? true : false}

                                                                                                        key={field.key}
                                                                                                    >
                                                                                                        <Form.Item
                                                                                                            labelAlign={props.i18n.locale == "ar" ? "right" : "left"}
                                                                                                            {...field}
                                                                                                            validateTrigger={['onChange', 'onBlur']}
                                                                                                            rules={[
                                                                                                                {
                                                                                                                    type: 'url',
                                                                                                                    message: I18n.t('dash.compte.url')
                                                                                                                }
                                                                                                            ]}
                                                                                                            noStyle
                                                                                                        >

                                                                                                            <Input
                                                                                                                addonBefore={
                                                                                                                    <Select defaultValue="Facebook" style={{ width: 110 }} onChange={(e) => { rest = resType, rest.splice(index, 1, e), setResType(rest), console.log("reseauu test 1", resType, index, e) }}>
                                                                                                                        <Option value="Twitter">{I18n.t('profil.tw')}</Option>
                                                                                                                        <Option value="Facebook" selected>{I18n.t('profil.fb')}</Option>
                                                                                                                        <Option value="Instagram" selected>{I18n.t('profil.insta')}</Option>
                                                                                                                        <Option value="Youtube" selected>{I18n.t('profil.you')}</Option>
                                                                                                                        <Option value="LinkedIn" selected>{I18n.t('profil.Link')}</Option>
                                                                                                                        <Option value="WhatsApp" selected>{I18n.t('profil.whatsapp')}</Option>
                                                                                                                        <Option value="Viber" selected>{I18n.t('profil.viber')}</Option>
                                                                                                                        <Option value="Telegram" selected>{I18n.t('Profil.telegram')}</Option>
                                                                                                                    </Select>
                                                                                                                }
                                                                                                                style={{ width: '70%' }}
                                                                                                                onChange={(e) => { resl = resLien, resl.splice(index, 1, e.target.value), setResLien(resl), console.log("reseauu test 2", resLien, index) }}
                                                                                                                allowClear />
                                                                                                        </Form.Item>
                                                                                                        &nbsp;
                                                                                                        {fields.length < 5 && fields.length - 1 === index ?
                                                                                                            <Button
                                                                                                                type="dashed"
                                                                                                                onClick={() => add()}
                                                                                                                icon={<PlusOutlined />}
                                                                                                            /> : null}
                                                                                                        &nbsp;
                                                                                                        <Button
                                                                                                            type="dashed"
                                                                                                            onClick={() => {
                                                                                                                remove(field.name),
                                                                                                                    resl = resLien, resl.splice(index, 1), setResLien(resl), console.log("defsdfffff", resLien, index),
                                                                                                                    rest = resType, rest.splice(index, 1), setResType(rest), console.log("defsdf", resType, index)
                                                                                                            }}

                                                                                                            icon={<MinusCircleOutlined />}
                                                                                                        />
                                                                                                    </Form.Item>

                                                                                                ))

                                                                                            }



                                                                                        </>
                                                                                    )


                                                                                }}
                                                                            </Form.List>
                                                                        </Form.Item>
                                                                        <FormItem   {...formItemLayout} label={I18n.t("profil.biographie")}  >
                                                            <Input aria-multiline  defaultValue={biographie} value={biographie} onChange={(e) => setBiographie(e.target.value)} />
                                                        </FormItem> </>
                                                                : null}
                                                        <FormItem labelAlign={props.i18n.locale == "ar" ? "right" : "left"}  {...formItemLayout} label={I18n.t('profil.new')} name="passwordnew" rules={[
                                                            {
                                                                required: true,
                                                                message: I18n.t('profil.newpass')
                                                            }, {
                                                                pattern: new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[1-9])[A-Za-z\\d$#%*+@!?&]{8,}$"),
                                                                message: I18n.t('profil.info')
                                                            }
                                                        ]}>
                                                            <Input.Password onChange={(e) => { setPasswordnew(e.target.value) }} initialValues={passwordnew} />
                                                        </FormItem>

                                                        <FormItem labelAlign={props.i18n.locale == "ar" ? "right" : "left"}  {...formItemLayout} label={I18n.t('profil.confirmnew')} name="passwordconfirm" rules={[
                                                            {
                                                                required: true,
                                                                message: I18n.t('profil.not')
                                                            },
                                                            {
                                                                pattern: new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[1-9])[A-Za-z\\d$#%*+@!?&]{8,}$"),
                                                                message: I18n.t('profil.info')
                                                            }
                                                        ]}>
                                                            <Input.Password onChange={(e) => { setPasswordconfirm(e.target.value) }} initialValues={passwordconfirm} />
                                                        </FormItem>
                                                      
                                                        <FormItem   {...formItemLayout} label={I18n.t("profil.modifierImage")}  >
                                                            <Upload onChange={(e) => setImage(e.fileList[0] ? e.fileList[0].originFileObj : "")}>
                                                                <Button className="ml-2" icon={<UploadOutlined />} style={{ color: "#777" }}  >
                                                                    {I18n.t("profil.importerImage")}
                                                                </Button>
                                                            </Upload>
                                                        </FormItem>
                                                        <Divider />
                                                        <FormItem   {...tailFormItemLayout}>
                                                            <Button

                                                                style={{
                                                                    bordercolor: "#00C1D8", color: "#fff",
                                                                    background: "#00C1D8"
                                                                }} htmlType="submit">
                                                                {I18n.t("action.enregistrer")}
                                                            </Button>
                                                        </FormItem>

                                                    </Form>

                                                    : null
                                                }

                                            </Card>

                                        )}


                                    </Col>

                                </Row>
                            </ContentLayout>

                        </VuroxLayout>
                    </ThemeProvider>

                </StylesProvider>
            </div>
        </React.Fragment>
    );
};

export default connect(state => state)(withStyles(styles)(Compte));


