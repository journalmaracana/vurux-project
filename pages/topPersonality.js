
import { useContext, useState, useEffect } from 'react'
import { Typography, Divider, Space, notification, Button, Row, Col, Upload,  } from 'antd'
import { UploadOutlined, InboxOutlined } from '@ant-design/icons';
const { Dragger } = Upload;
const { Title, Paragraph } = Typography
import HeaderDark from 'Templates/HeaderDark';
import Sidebar from 'Templates/HeaderSidebar';
import {
    VuroxLayout,
    HeaderLayout,
    VuroxSidebar,
    ContentLayout,
    VuroxComponentsContainer
} from 'Components/layout'
import { teal, grey } from "@material-ui/core/colors";
import { CardMedia, MenuItem, ButtonGroup, withStyles } from '@material-ui/core';
import { vuroxContext } from '../context'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import Paper from '@mui/material/Paper'
import axios from "axios"
import {API_LINK} from '../utils/constantes'
import Swal from 'sweetalert2';
import Spinner from '../Components/spinner';
import { I18n } from 'react-redux-i18n';
import {connect} from 'react-redux'
import { verifyPermission } from '../utils/functions';
import { useRouter } from 'next/router';

const styles = theme => ({
    root: {
        flexGrow: 5,
        fontSize: "1.5rem"
    },
    primaryColor: {
        color: "rgb(0, 193, 216)",
        fontSize: "27px"
    },
    secondaryColor: {
        color: grey[700]
    },

    padding: {
        padding: 0,

    },
    textFont: {
        fontSize: "1.5rem"
    },
    mainHeader: {
        backgroundColor: grey[100],
        padding: 20,
        alignItems: "center"
    },
    mainContent: {
        padding: 40,

    },
    secondaryContainer: {
        padding: "20px 25px",
        backgroundColor: grey[200]
    }
});


const TopPersonality = (props) => {
    const langPage = props.i18n.locale
    const { classes, open, onClose } = props;
    const { menuState } = useContext(vuroxContext)
    const toggleClass = menuState ? 'menu-closed' : 'menu-open'
    const [isLoading, setIsLaoding] = useState(true)
    const [topPersonalities, setTopPersonalities] = useState([])
    const [Personalities, setPersonalities] = useState([])

    useEffect(() => {
        getPersonalities()
        getTopPersonalities()
    },[])
    const getPersonalities = async () => {
        await axios.get(API_LINK+ 'v1/personalitiesDash')
            .then(res => {
                console.log('res', res)
                setIsLaoding(false)
                setPersonalities(res.data.map(item => {
                        console.log('item',item)
                        return { value: item._id, label:langPage === "ar" ? item.firstNameAR + " "+item.lastNameAR  : item.nickname}
                    }))
            
            })
            .catch(err => {
                setIsLaoding(false)
            })
    }
    const getTopPersonalities = async () => {
        await axios.get(API_LINK + 'v1/topPersonalities')
            .then(res => {
                console.log('res', res)
               
                    setTopPersonalities(res.data.map(item => {
                        return { value: item._id, label: item.label }
                  
                })  )
            })
            .catch(err => {
                console.log(err);
            })

    }
    const addTopPersonalities = (topPersonality) => {
        axios.put(API_LINK + 'v1/topPersonalities', topPersonality)
            .then(res => {
                getTopPersonalities()
                setIsLaoding(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: I18n.t('topPerso.addsuccess'),
                        showConfirmButton: true,
                    }))
            })
            .catch(err => {
                console.error('err', err.response);
                setIsLaoding(false)
                return (
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: I18n.t('topPerso.adderror'),
                        showConfirmButton: true,
                    }))
            })
    }

    const handleChange = (event, values) => {
        console.log("love", event, values);
        if (event) {
            setTopPersonalities(values);
        } else {
            setTopPersonalities([]);
        }
    };

    const handleDelete = (event, item) => {

        if (event) {
            const newSelectedItem = [...topPersonalities];
            console.log("handleDeletecat", newSelectedItem.indexOf(item))
            newSelectedItem.splice(newSelectedItem.indexOf(item), 1);
            setTopPersonalities(newSelectedItem);
        }
    };
    if (
        !verifyPermission(
             props.user.UserID,
            useRouter().pathname,
            props.user.UserType ? props.user.UserType : "simple",
            props.user.UserIsValid? props.user.UserIsValid : ""
        )
    ) {
        return (
            <>
                <React.Fragment>
                    <VuroxLayout>
                      
                        <ContentLayout width="100%" className="p-3 vurox-scroll-y">
                            <Row align={props.i18n.locale === 'ar' ? "right" : "left"}>
                                <Col md="4"></Col>
                                <Col md="4">
                                    <b
                                        className="text-center text-danger"
                                        style={{ marginTop: "210px", fontSize: "55px" }}
                                    >
                                        {" "}
                                       {props.i18n.locale=="ar"?"ACCESS DENIED":"ACCESS DENIED"} 
                                    </b>
                                </Col>
                                <Col md="4"></Col>
                            </Row>
                        </ContentLayout>
                    </VuroxLayout>
                </React.Fragment>
            </>
        );
    }
    
    return (
        
        <React.Fragment>
        <div dir={langPage === 'ar' ? 'rtl' : 'ltr'}>    
         {!isLoading ? 
          <>
            <HeaderLayout className="sticky-top">
                <HeaderDark />
            </HeaderLayout>
            <VuroxLayout>
                <VuroxSidebar width={240} className={`sidebar-container  ${toggleClass}`} >
                    <Sidebar className={toggleClass} />
                </VuroxSidebar>
                <ContentLayout width='100%' className='p-1 vurox-scroll-y'>
                    <Row>
                        <Col md={24} align={langPage === 'ar' ? "right" :"left"}>
                            <VuroxComponentsContainer className='p-4 ml-2 mt-2'>
                                <Title level={2}>{I18n.t('topPerso.top_perso')}</Title>

                                <Divider dashed />

                                <Grid item xs={12} align={langPage === 'ar' ? "right" :"left"} >
                                    <Autocomplete
                                        multiple
                                        freeSolo
                                        id="tags-outlined"
                                        options={Personalities}
                                        getOptionLabel={(option) => option.label}
                                        getOptionValue={(option) => option.value}
                                        value={topPersonalities.length > 0 ? topPersonalities : []}
                                        PaperComponent={({ children }) => (
                                            <Paper style={langPage === 'ar' ? { direction: 'rtl' } : { direction: 'ltr' }}>{children}</Paper>
                                        )}
                                        onChange={(event, newValue) =>
                                            handleChange(event, newValue)
                                        }
                                        getOptionSelected={(option, value) => {
                                            return option.value === value.value
                                        }}
                                        filterSelectedOptions
                                        renderInput={(params) => {

                                            return (
                                                <TextField
                                                    inputProps={ langPage === 'ar' ? { style: { textAlign: 'right' } } : { style: { textAlign: 'left' } }}
                                                    {...params}
                                                    InputProps={{
                                                        ...params.InputProps,
                                                        startAdornment:
                                                        //langPage === 'ar' ? null :
                                                            (

                                                                topPersonalities.length > 0 ? topPersonalities.map((item, index) => (
                                                                    <Chip
                                                                        tabIndex={-1}
                                                                        label={<Typography style={{ whiteSpace: 'normal', float: "left" }}>{item.label}</Typography>}
                                                                        inputLabelProps={{ style: { textAlign: 'left' } }}
                                                                        //style={{height:"100%"}}
                                                                        className={classes.chip}
                                                                        onDelete={(e, v) => { console.log("valuuees", v, e), handleDelete(e, item) }}
                                                                    />
                                                                ))

                                                                    : null
                                                            ),
                //                                             endAdornment: langPage === 'ar' ?
                //     topPersonalities.length> 0? topPersonalities.map((item,index) => (
                //       <>

                //    <Chip
                //      label={<Typography style={{whiteSpace: 'normal' , float:"right"}}>{ item.label}</Typography>}
                //      inputLabelProps={{ style: { textAlign: 'right' }}}
                //      className={classes.chip}
                //      onDelete={(e)=> handleDelete(e,item)}
                //    />
                //    </>
                //  )):null:null, 
                                                    }}
                                                    variant="outlined"
                                                    label={""}
                                                    margin="normal"
                                                    fullWidth
                                                    name="selectValue"
                                                    id="selectValue"
                                                // onBlur={() => validator.showMessageFor("category")}

                                                />
                                            );
                                        }}
                                    />
                                </Grid>

                                <Button type="primary" onClick={()=>addTopPersonalities(topPersonalities)}>
                                    {I18n.t('action.enregistrer')}
                                </Button>
                                <br />
                                <br />
                                <Divider dashed />

                            </VuroxComponentsContainer>
                        </Col>

                    </Row>
                </ContentLayout>
            </VuroxLayout>
         </> :

        <Spinner  loading={isLoading}/>
} </div> </React.Fragment>
     
    )
}
export default connect(state=>state)(withStyles(styles)(TopPersonality))
