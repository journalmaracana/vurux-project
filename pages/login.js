import { Button, Checkbox, Form, Input, Message, Row } from 'antd';
import { EyeTwoTone, MailTwoTone, PlaySquareTwoTone } from '@ant-design/icons';
import { useDispatch, useSelector } from "react-redux";
import { connect } from 'react-redux'
import React, { useState, useEffect } from "react";
import Swal from 'sweetalert2';
import Link from 'next/link';
import { useRouter } from "next/router";
import styled from 'styled-components';
import axios from 'axios'
import { API_LINK } from '../utils/constantes';
import { I18n ,Translate} from 'react-redux-i18n'
const FormItem = Form.Item;
const Content = styled.div`
  max-width: 780px;
  z-index: 2;
  min-width: 780px;
`;

const Signin = (props,{ form }) => {
  const router = useRouter();
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    
     console.log("routerlogin", router,props)
    if (router.query.logout) {
      props.dispatch({ type: 'USER_RESET', payload: "" })
      router.push("/login").then(()=>{
        router.reload()
      })
  
    }
    else if (props.user.UserID) {
      props.dispatch({ type: 'LOGGED_IN', payload: props.user });
      router.replace("/")
    }
    
  }, []);
  
  const onlogin = () => {

     const data={mail,password}
       axios
         .post(API_LINK + "v1/loginNew", data)
         .then((res) => {
           console.log("res login", res.data)
           if (res.data.message) {
            Swal.fire({
              icon: "error",
              title: "",
              confirmButtonColor: "#a6c76c",
              cancelButtonColor: "#a6c76c",
              confirmButtonText: "Oui",
              text: I18n.t("login.failed"),
            });
            //  setText(res.data.message)
            //  openModalConfirmAddComment()
             console.log("res login message,",res.data.message)
           } else {
             const user = {
               UserID: res.data.user._id,
               UserMail: res.data.user.mail,
               UserType:res.data.user.type,
               UserToken:res.data.token,
               UserIsValid:res.data.user.isvalid,
               userName:"",
               userLastName:""
             }
        
             props.dispatch({ type: 'LOGGED_IN', payload: user });
             router.replace("/")
    
           }
         })
         .catch((err) => {
          console.log("res login failed",err)
          Swal.fire({
            icon: "error",
            title: "",
            confirmButtonColor: "#a6c76c",
            cancelButtonColor: "#a6c76c",
            confirmButtonText: "Oui",
            text: I18n.t("login.error"),
          });
      
         });
 
 
 
  }

  return (<Row
    type="flex"
    align="middle"
    justify="center"
    className="px-3 bg-white mh-page"
    style={{ minHeight: '100vh' }}
  >
    <div style={{ maxWidth: "100%", zIndex: "2", minWidth: "50%" }} >
      <div className="text-center mb-5">
        <Link href="/login">
          <img src='image/Logo2.png' style={{ display: "block", marginLeft: "auto", marginRight: "auto" }} />
        </Link>
        <h1 className="mb-0 mt-9 " style ={{textAlign:"center"}}>{I18n.t("login.login")}</h1>

      </div>

      <Form
        layout="vertical"
        onSubmit={e => {
          e.preventDefault();
          form.validateFields((err, values) => {
            if (!err) {
              Message.success(
                'Sign complete. Taking you to your dashboard!'
              ).then(() => router.push('/'));
            }
          });
        }}
      >
        <FormItem label="Email" name="email" rules={[
          {
            type: 'email',
            message: I18n.t("regex.email")
          },
          {
            required: true,
            message: I18n.t("journal.required")
          }
        ]}>
          <Input

            prefix={
              <MailTwoTone style={{ fontSize: '16px' }} />
            }
            type="email"
            placeholder= {I18n.t("login.email")}
            onChange={(e) => setMail(e.target.value)}

          />
        </FormItem>

        <FormItem label="Password" name="password" rules={[{ required: true, message: I18n.t("journal.required") }]}>
          <Input
            prefix={
              <EyeTwoTone color =" rgba(255, 226, 63, 0.445)" style={{ fontSize: '16px' }} />
            }
            type="password"
            placeholder={I18n.t("login.password")}
            onChange={(e) => setPassword(e.target.value)}

          />
        </FormItem>

        <FormItem name="remember" valuePropName="checked" initialValue={true}>
  
          <Button type="danger"  htmlType="submit" block className="mt-3"
            onClick={() => onlogin()}
          >
       { I18n.t("login.login")}
          </Button>
        </FormItem>
    
      </Form>
    </div>
  </Row>
  );
}
export default connect(state => state)(Signin)

