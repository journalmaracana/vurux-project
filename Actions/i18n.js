import { setLocale ,loadTranslations} from "react-redux-i18n";
import { supportedLocales, fallbackLocale } from "../utils/constantes";
import translations from "../utils/translation";
export function setLocaleWithFallback(desiredLocale) {
  const finalLocale = Object.keys(supportedLocales).includes(desiredLocale)
    ? desiredLocale
    : fallbackLocale;

  return dispatch => dispatch(setLocale(finalLocale));
}
export function setTranslations() {
  return dispatch => dispatch(loadTranslations(translations));

}