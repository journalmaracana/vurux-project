import Link from "next/link";

export default function ALink ( { children, className, style, disabled, ...props } ) {
    function defaultFunction ( e ) {
        if ( props.href == '#' ) {
            e.preventDefault();
        }
    }

    return (
        <Link { ...props }>
            <a className={ className } style={ style } onClick={disabled ? null :defaultFunction }>
                { children }
            </a>
        </Link>
    )
}
