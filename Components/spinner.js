import React from "react";
import { ClapSpinner } from "react-spinners-kit";


const Spinner = ({ loading }) => {
  return (
    <div >

        <div className="m-2 p-2" style={{  position: "absolute" ,top: "15%", left: "50%", transform: "translate(-50%, -50%)"}}>
          <ClapSpinner size={30}   frontColor= "#00C1D8" backColor= "#00C1D8" loading={loading} />
        </div>
    </div>
  );
};
export default Spinner;


