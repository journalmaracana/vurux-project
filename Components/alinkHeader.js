import Link from "next/link";

export default function ALinkHeader ( { children, className, style, ...props } ) {
    console.log("enter alink")
    function defaultFunction ( e ) {
        console.log("alink header")
       
        props.onChange(props.code)

    }

    return (
        <Link { ...props }>
            <a className={ className } style={ style } onClick={ defaultFunction } >
                { children  }
            </a>
        </Link>
    )
}
