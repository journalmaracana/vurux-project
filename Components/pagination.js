
import { useRouter } from 'next/router';
import React, { useState, useEffect ,FC } from 'react';
import Link  from "next/link";
import { connect } from 'react-redux'

 export const Pagination = (props) => {
    const langue=props.langue?props.langue:"fr"
    const router = useRouter();
    const query = useRouter().query;
    const perPage=props.perPage
    const total=props.total
    const [ currentPage, setCurrentPage ] = useState( 1 );
    const [ lastPage, setLastPage ] = useState( 1 );
    const [ pageNumbers, setPageNumbers ] = useState([])
    useEffect( () => {
      const page=query.page? query.page:""
      setCurrentPage( page ? parseInt( page ) : 1 );
      console.log("queeeerycuurent",currentPage)
  }, [ query ] )
  
  useEffect( () => {
      setLastPage( Math.floor( total / perPage ) + ( total % perPage ? 1 : 0 ) );
      const f=Math.floor( total / perPage ) + ( total % perPage ? 1 : 0 ) 
      console.log("queeeerycuurent last",currentPage ,f)
  
  }, [ total, perPage ,currentPage] )
  
  useEffect( () => {
      let tempArray = []; 
      let pageCount = Math.floor( total / perPage ) + ( 0 < total % perPage ? 1 : 0 )
      console.log("queeeerycuurent count",total ,"second",( 0 < total % perPage ? 1 : 0 ))
      for ( let i = -1; i < 2 && pageCount >= 3; i++ ) {
          if ( 1 < currentPage && currentPage < pageCount )
              tempArray.push( currentPage + i );
          if ( 1 === currentPage )
              tempArray.push( currentPage + i + 1 );
          if ( currentPage === pageCount )
              tempArray.push( currentPage + i - 1 );
      }
  
      for ( let i = 0; i < pageCount && pageCount < 3; i++ ) {
          tempArray.push( i + 1 );
      }
  
      setPageNumbers( tempArray );

  }, [ total, perPage, currentPage ] )
  return (
  
            <ul className="pagination vurox-horizontal-links boxed"  >
               
                 <li className={currentPage=== 1 ? 'disabled' : ''}>
                 <Link  href={ {pathname: router.pathname, query: { ...query, page: 1 } } } scroll={ false }>
                    <a href={ {pathname: router.pathname, query: { ...query, page: 1 } } }> <i className={langue==="ar"?"ti-angle-double-right":"ti-angle-double-left"}></i></a>
                    </Link>
                </li>
              
                   <li className={currentPage === 1 ? 'disabled' : ''}>
                          <Link  href={ {pathname: router.pathname, query: { ...query, page: currentPage - 1 } } } scroll={ false }>
                   <a  href={ {pathname: router.pathname, query: { ...query, page: currentPage - 1 } } }> <i className={langue==="ar"?"ti-angle-right":"ti-angle-left"}></i></a>
                   </Link>
               </li>
               <div className={langue === 'ar' ?"flex flex-row-reverse":"" }>
            
                        {
                    pageNumbers.length ?
                        pageNumbers.map( ( page, index ) => (
                            <li key={index} className={currentPage === page ? 'active' : ''}>
                                    <Link   scroll={ false } href={ {pathname: router.pathname, query: { ...query, page: page } } }>
                            <a href={ {pathname: router.pathname, query: { ...query, page: page } } }>{page}</a>
                            </Link>
                        </li>
                         
                                // <Link
                                //     href={ {pathname: router.pathname, query: { ...query, page: page } } }
                                //     scroll={ false }
                                // ><a className={ `inline-flex w-11 h-11 items-center justify-center rounded-full ${currentPage == page ? 'bg-primary-6000 text-white ' : 'bg-white hover:bg-neutral-100 border border-neutral-200 text-neutral-6000 dark:text-neutral-400 dark:bg-neutral-900 dark:hover:bg-neutral-800 dark:border-neutral-700 '}${twFocusClass()}` }>
                                //   { page }</a>
                                //   </Link>
                         
                        ) )
                        : ""
                }
                </div>
                
                     <li className={currentPage === lastPage || pageNumbers.length==1  ?  'disabled' : ''}>
                     <Link  href={ {pathname: router.pathname, query: { ...query, page: currentPage + 1 } } } scroll={ false }>
                         <a href={ {pathname: router.pathname, query: { ...query, page: currentPage + 1 } } }> <i className={langue==="ar"?"ti-angle-left":"ti-angle-right"}></i></a>
                         </Link>
                     </li>
         
                <li className={currentPage === lastPage || pageNumbers.length==1  ? 'disabled' : ''}>
                <Link  href={ {pathname: router.pathname, query: { ...query, page: lastPage } } } scroll={ false }>
                    <a href={ {pathname: router.pathname, query: { ...query, page: lastPage } } } > <i className={langue==="ar"?"ti-angle-double-left":"ti-angle-double-right"}></i></a>
                    </Link>
                </li>
            </ul>
        );
 
}
