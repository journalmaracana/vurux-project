
const initialState = {
    UserID: "",
    UserMail: "" ,
    UserType:"",
    UserToken:"",
    UserIsValid:false,
    userName:"",
    userLastName:''
}

export const UserAuth = ( state = initialState, action ) => {
	switch( action.type ){
		case 'LOGGED_IN' : {
			return{
                UserID: action.payload.UserID,
                UserMail: action.payload.UserMail,
                UserType:action.payload.UserType,
                UserToken:action.payload.UserToken,
                UserIsValid:action.payload.UserIsValid,
                userName:action.payload.userName,
                userLastName:action.payload.userLastName
			}
		}

		case 'USER_RESET' : {
			return{
                UserID: "",
                UserMail: "" ,
                UserType:"",
                UserToken:"",
                UserIsValid:false,
                userName:"",
                userLastName:''
			}
		}
	}
	return state
}