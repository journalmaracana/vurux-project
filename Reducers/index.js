import { combineReducers } from 'redux'
import { vuroxUsers } from './users'
import { vuroxCompanyInfo } from './company'
import { vuroxCompanyCalendar } from './calendar'
import { vuroxMail } from './mail'
import { vuroxChatMessages } from './message'
import  {UserAuth} from './auth'
import { i18nReducer } from "react-redux-i18n";
const rootReducer = combineReducers({
	user:UserAuth,
	users: vuroxUsers,
	i18n:i18nReducer,
	company: vuroxCompanyInfo,
	calendar: vuroxCompanyCalendar,
	mail: vuroxMail,
	message: vuroxChatMessages,
})
export default rootReducer;