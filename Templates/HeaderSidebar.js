import React, { useContext, useEffect, useState } from "react";
import { vuroxContext } from "../context/index";
import { VerticalNavHeading, Navitem } from 'Components/nav'
import * as Bsicon from 'react-bootstrap-icons'
import { Search, GridFill, Grid } from "react-bootstrap-icons";
import VuroxFormSearch from 'Components/search'
import { API_LINK } from "../utils/constantes";
import { connect } from "react-redux";
import axios from "axios";
const Sidebar = (props) => {
	console.log('sidebar',props)
	const { toggleMenu, menuState } = useContext(vuroxContext);
	const [type, setType] = useState("simple");
	const [simple, setSimple] = useState([]);
	const [navPermissions, setNavPermission] = useState([]);
	let navPermission = [];
	useEffect(() => {
		setType(props.user.UserType ? props.user.UserType : "")
		const userID = props.user.UserID ? props.user.UserID : ""
		getUser(userID);
		
	}, []);
	function getUser(id) {
		if (type === "simple") {
			axios
				.get(API_LINK + "v1/user/" + id + "/")
				.then((res) => {
					setSimple(res.data.roles.permissions);
				})
				.catch((err) => console.log(err + "dddd"));
		}

	}
	function getText(text) {
		let result = ""
		switch (text) {
			case "utilisateur":
				result = props.i18n.locale === "ar" ? "المستخدمين":"Utilisateurs"
				return result
			case "article":
				result = props.i18n.locale === "ar" ?  "المقالات": "Articles"
				return result
			case "journal":
				result = props.i18n.locale === "ar" ? "جريدة" : "Journal"
				return result
			case "top-club":
				result = props.i18n.locale === "ar" ?"أفضل النوادي" :  "Top Clubs"
				return result
			case "top-personality":
				result = props.i18n.locale === "ar" ?  "أفضل الشخصيات" :"Top Personalités"
				return result
			case "top-selection":
				result = props.i18n.locale === "ar" ?  "أفضل المنتخبات الوطنية" :"Top Sélections"
				return result
			case "top-tournoi":
				result = props.i18n.locale === "ar" ?"أفضل البطولات"  : "Top Tournois"
				return result
			case "profil":
				result = props.i18n.locale === "ar" ? "حسابي" : "Profil"
				return result
		
			case "publicity":
				result = props.i18n.locale === "ar" ? "الإعلانات" : "Publicité"
				return result
			case "Client":

			default:
				break;
		}
		return result
	}
	function switchPermission(typeUser) {
		let navSimple = [
			<Navitem link="/" text={props.i18n.locale === "ar" ?  "الصفحة الرئيسية" :"Accueil"} icon={<Bsicon.House />} />,
			<Navitem link="/profil" text={props.i18n.locale === "ar" ?"حسابي" : "Profil" } icon={<Bsicon.House />} />
		];
		switch (typeUser) {
			case "principal":
				return (
					<>
						<Navitem link="/" text={props.i18n.locale === "ar" ? "الصفحة الرئيسية" : "Accueil"} icon={<Bsicon.House />} />
						<Navitem link="/profil" text={props.i18n.locale === "ar" ?"حسابي" : "Profil" } icon={<Bsicon.House />} />
						<Navitem link='/article' text={props.i18n.locale === "ar" ? "المقالات":"Articles"  } icon={<Bsicon.CardText />} />
						<Navitem link='/users' icon={<Bsicon.PersonDash />} text={props.i18n.locale === "ar" ? "المستخدمين" :"Utilisateurs" } />
						<Navitem link='/journal' text={props.i18n.locale === "ar" ? "جريدة" :"Journal" } icon={<Bsicon.Bag />} />
						<Navitem link='/topClubs' icon={<Bsicon.FileImage />} text={props.i18n.locale === "ar" ? "أفضل النوادي" :"Top Clubs" } />
						<Navitem link='/topPersonality' icon={<Bsicon.Person />} text={props.i18n.locale === "ar" ? "أفضل الشخصيات" :"Top Personalités" } />
						<Navitem link='/topSelections' icon={<Bsicon.Flag />} text={props.i18n.locale === "ar" ? "أفضل المنتخبات الوطنية" :"Top Séléctions" } />
						<Navitem link='/topTournois' icon={<Bsicon.FilterLeft />} text={props.i18n.locale === "ar" ?  "أفضل البطولات":"Top Tournois" } />
						<Navitem link='/ads_manager' text={props.i18n.locale === "ar" ? "الإعلانات" : 'Publicité'} icon={<Bsicon.Bag />} />
					</>
				);

			case "simple":
				simple.length
					? simple.map((simple) => {
						console.log("simplesimple", simple);
						navSimple.push(
							<>
								<Navitem
									link={"/" + simple}
									text={getText(simple)}
									icon={<Bsicon.PieChart />}
								/>
							</>
						);
					})
					: null;
				return [...navSimple];
			case "Client":
				return (
					<Navitem link="/" text={props.i18n.locale === "ar" ?  "الصفحة الرئيسية": "Accueil"} icon={<Bsicon.House />} />
				);
			default:
				break;
		}
	}
	return (
		<div className={`${props.className} vurox-vertical-nav ${props.i18n.locale=="ar"?"text-right":"text-left"} `} style={{ width: props.width + 'px' }}>
			<ul>
				{/* {menuState ? (

					<Grid className="vurox-menu-toggler" onClick={toggleMenu} />

				) : (
					<GridFill className="vurox-menu-toggler" onClick={toggleMenu} />
				)} */}
				 <VerticalNavHeading>{props.i18n.locale === "ar" ? "لوحة القيادة" : "Dashboards"} </VerticalNavHeading>
        
                {type ? switchPermission(type) : null}
				<VuroxFormSearch border='rounded' className='ml-4 d-block d-sm-none bg-grey-6' />
				
				{/* 				
				<Navitem link='/' text='Acceuil' icon={<Bsicon.House />} />
				<Navitem link='/profil' text='Profil' icon={<Bsicon.Person />} />
				<Navitem link='/journal' text='Journal' icon={<Bsicon.Bag />} />
				<Navitem link='/article' text='Articles' icon={<Bsicon.CardText />} />
				<Navitem link='/users' icon={<Bsicon.PersonDash />} text='Utilisateurs' />
				<Navitem link='/topClubs' icon={<Bsicon.FileImage />} text='Top Clubs' />
				<Navitem link='/topPersonality' icon={<Bsicon.Person />} text='Top Personalités' />
				<Navitem link='/topSelections' icon={<Bsicon.Flag />} text='Top Séléctions' />
				<Navitem link='/topTournois' icon={<Bsicon.FilterLeft />} text='Top Tournois' />
				<Navitem link='/ads_manager' text='Publicité' icon={<Bsicon.Bag />} />
				<Navitem link='/webAnalytics' icon={<Bsicon.GraphUp />} text='Web Analytics' />
				<Navitem link='/webAnalytics2' icon={<Bsicon.GraphUp />} text='Web Analytics 2' />
				<Navitem link='/productManagement' icon={<Bsicon.BookmarkCheck />} text='Product Management' />
				<Navitem link='/productManagement2' icon={<Bsicon.BoundingBoxCircles />} text='Product Management 2' />
				<Navitem link='/salesMonitoring' text='Sales monitoring' icon={<Bsicon.PieChart />} />
				<VerticalNavHeading>Application</VerticalNavHeading>
				<Navitem link='/orders' text='Orders' icon={<Bsicon.Bag />} badge="badge badge-pill bg-green-5" badgeText='4' />
				<Navitem link='/mail' text='Email' icon={<Bsicon.Envelope />} badge="badge badge-pill bg-cyan-5" badgeText='14+' />
				<VerticalNavHeading>Components</VerticalNavHeading>
				<Navitem link='/alert' text='Alert' icon={<Bsicon.ExclamationCircle />} />
				<Navitem link='/badge' text='Badge' icon={<Bsicon.Fullscreen />} />
				<Navitem link='/breadcrumb' text='Breadcrumbs' icon={<Bsicon.FilterLeft />} badge="badge badge-pill bg-cyan-5" badgeText='14+' />
				<Navitem link='/buttons' text='Buttons' icon={<Bsicon.ViewList />} />
				<Navitem link='/card' text='Cards' icon={<Bsicon.FilesAlt />} />
				<Navitem link='/carousel' text='Carousel' icon={<Bsicon.Textarea />} />
				<Navitem link='/collapse' text='Collapse' icon={<Bsicon.Collection />} />
				<Navitem link='/comments' text='Comments' icon={<Bsicon.ChatDots />} />
				<Navitem link='/dropdown' text='Dropdown' icon={<Bsicon.Filter />} />
				<Navitem link='/forms' text='Forms' icon={<Bsicon.CardText />} badge="badge badge-pill bg-blue-5" badgeText='13+' />
				<Navitem link='/listitems' text='List Items' icon={<Bsicon.ListCheck />} />
				<Navitem link='/modal' text='Modals' icon={<Bsicon.Window />} />
				<Navitem link='/upload' text='Upload' icon={<Bsicon.CloudUpload />} />
				<Navitem link='/notification' text='Notification' icon={<Bsicon.InfoCircle />} /> */}
			
			</ul>
		</div>
	);
}
export default connect(state => state)(Sidebar);

